# **Essence** - An Operating System

## Discussion

Visit https://essence.handmade.network/forums.

## Building

Download this project's source. 

    git clone https://nakst@bitbucket.org/nakst/essence.git

Start the build system.

    ./start.sh

Follow the on-screen instructions to build a cross compiler.

You can then run the build system again to build the operating system, and test it in an emulator. If you have Qemu installed, try the command `t2`. If you have VirtualBox installed, make a 128MB drive called `vbox.vdi` in the `essence` folder, and try the command `v` for a much smoother experience than in Qemu.

**Warning: This software is still in development. Expect bugs!**

## Documentation

*Warning: This is a work in progress and may be out of date as the API is still evolving.*

[Essence Documentation](https://nakst.github.io/essence_documentation/)
