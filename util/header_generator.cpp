#define TARGET_LANGUAGE_C

#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <dirent.h>

FILE *output;

#ifdef TARGET_LANGUAGE_C
const char *prefix = R"Multiline(#ifndef IncludedEssenceAPIHeader
#define IncludedEssenceAPIHeader

#include <stdint.h>
#include <limits.h>
#include <stddef.h>
#include <stdbool.h>

#ifndef OS_NO_STDARG
#include <stdarg.h>
#endif

#ifdef __cplusplus
#define OS_EXTERN_C extern "C"
#define OS_CONSTRUCTOR(x) x

// Scoped defer: http://www.gingerbill.org/article/defer-in-cpp.html
template <typename F> struct OSprivDefer { F f; OSprivDefer(F f) : f(f) {} ~OSprivDefer() { f(); } };
template <typename F> OSprivDefer<F> OSdefer_func(F f) { return OSprivDefer<F>(f); }
#define OSDEFER_1(x, y) x##y
#define OSDEFER_2(x, y) OSDEFER_1(x, y)
#define OSDEFER_3(x)    OSDEFER_2(x, __COUNTER__)
#define OS_Defer(code)   auto OSDEFER_3(_defer_) = OSdefer_func([&](){code;})
#define OSDefer(code)   OS_Defer(code)

#else
#ifndef nullptr
#define nullptr (0)
#endif
#define OS_EXTERN_C extern
#define OS_CONSTRUCTOR(x)
#endif

#define OSLiteral(x) (char *) x, OSCStringLength((char *) x)

OS_EXTERN_C uintptr_t _OSSyscall(uintptr_t argument0, uintptr_t argument1, uintptr_t argument2, 
			        uintptr_t unused,    uintptr_t argument3, uintptr_t argument4);
#define OSSyscall(a, b, c, d, e) _OSSyscall((a), (b), (c), 0, (d), (e))

#define OS_CHECK_ERROR(x) (((intptr_t) (x)) < (OS_SUCCESS))

typedef intptr_t OSError;
typedef uintptr_t OSHandle;
typedef void *OSObject;

)Multiline";

const char *filename = "auto_os.h";

void DefineConstant(const char *name, const char *value) {
	fprintf(output, "#define %s (%s)\n", name, value);
}

void StartEnum(const char *name) {
	fprintf(output, "typedef enum %s {\n", name);
}

void EnumEntry(const char *name) {
	fprintf(output, "\t%s,\n", name);
}

void EnumEntryAt(const char *name, const char *value) {
	fprintf(output, "\t%s = %s,\n", name, value);
}

void EndEnum(const char *name) {
	fprintf(output, "} %s;\n", name);
}

void StartStruct(const char *name) {
	fprintf(output, "typedef struct %s {\n", name);
}

void EndStruct(const char *name) {
	fprintf(output, "} %s;\n", name);
}
#endif

int main(int argc, char **argv) {
	output = fopen(filename, "wb");

	{
		fprintf(output, prefix);

		{
			DefineConstant("OS_SCANCODE_A", "0x1C");
			DefineConstant("OS_SCANCODE_B", "0x32");
			DefineConstant("OS_SCANCODE_C", "0x21");
			DefineConstant("OS_SCANCODE_D", "0x23");
			DefineConstant("OS_SCANCODE_E", "0x24");
			DefineConstant("OS_SCANCODE_F", "0x2B");
			DefineConstant("OS_SCANCODE_G", "0x34");
			DefineConstant("OS_SCANCODE_H", "0x33");
			DefineConstant("OS_SCANCODE_I", "0x43");
			DefineConstant("OS_SCANCODE_J", "0x3B");
			DefineConstant("OS_SCANCODE_K", "0x42");
			DefineConstant("OS_SCANCODE_L", "0x4B");
			DefineConstant("OS_SCANCODE_M", "0x3A");
			DefineConstant("OS_SCANCODE_N", "0x31");
			DefineConstant("OS_SCANCODE_O", "0x44");
			DefineConstant("OS_SCANCODE_P", "0x4D");
			DefineConstant("OS_SCANCODE_Q", "0x15");
			DefineConstant("OS_SCANCODE_R", "0x2D");
			DefineConstant("OS_SCANCODE_S", "0x1B");
			DefineConstant("OS_SCANCODE_T", "0x2C");
			DefineConstant("OS_SCANCODE_U", "0x3C");
			DefineConstant("OS_SCANCODE_V", "0x2A");
			DefineConstant("OS_SCANCODE_W", "0x1D");
			DefineConstant("OS_SCANCODE_X", "0x22");
			DefineConstant("OS_SCANCODE_Y", "0x35");
			DefineConstant("OS_SCANCODE_Z", "0x1A");

			DefineConstant("OS_SCANCODE_0", "0x45");
			DefineConstant("OS_SCANCODE_1", "0x16");
			DefineConstant("OS_SCANCODE_2", "0x1E");
			DefineConstant("OS_SCANCODE_3", "0x26");
			DefineConstant("OS_SCANCODE_4", "0x25");
			DefineConstant("OS_SCANCODE_5", "0x2E");
			DefineConstant("OS_SCANCODE_6", "0x36");
			DefineConstant("OS_SCANCODE_7", "0x3D");
			DefineConstant("OS_SCANCODE_8", "0x3E");
			DefineConstant("OS_SCANCODE_9", "0x46");

			// NUM_LOCK is also sent by the pause key.
			DefineConstant("OS_SCANCODE_NUM_LOCK	", "0x77"); 
			DefineConstant("OS_SCANCODE_CAPS_LOCK	", "0x58");
			DefineConstant("OS_SCANCODE_SCROLL_LOCK	", "0x7E");
			DefineConstant("OS_SCANCODE_LEFT_SHIFT	", "0x12");
			DefineConstant("OS_SCANCODE_LEFT_CTRL	", "0x14");
			DefineConstant("OS_SCANCODE_LEFT_ALT	", "0x11");
			DefineConstant("OS_SCANCODE_LEFT_FLAG	", "0x11F");
			DefineConstant("OS_SCANCODE_RIGHT_SHIFT	", "0x59");
			DefineConstant("OS_SCANCODE_RIGHT_CTRL	", "0x114");
			DefineConstant("OS_SCANCODE_RIGHT_ALT	", "0x111");
			DefineConstant("OS_SCANCODE_PAUSE	", "0xE1");
			DefineConstant("OS_SCANCODE_CONTEXT_MENU", "0x127");

			DefineConstant("OS_SCANCODE_BACKSPACE	", "0x66");
			DefineConstant("OS_SCANCODE_ESCAPE	", "0x76");
			DefineConstant("OS_SCANCODE_INSERT	", "0x170");
			DefineConstant("OS_SCANCODE_HOME	", "0x16C");
			DefineConstant("OS_SCANCODE_PAGE_UP	", "0x17D");
			DefineConstant("OS_SCANCODE_DELETE	", "0x171");
			DefineConstant("OS_SCANCODE_END		", "0x169");
			DefineConstant("OS_SCANCODE_PAGE_DOWN	", "0x17A");
			DefineConstant("OS_SCANCODE_UP_ARROW	", "0x175");
			DefineConstant("OS_SCANCODE_LEFT_ARROW	", "0x16B");
			DefineConstant("OS_SCANCODE_DOWN_ARROW	", "0x172");
			DefineConstant("OS_SCANCODE_RIGHT_ARROW	", "0x174");

			DefineConstant("OS_SCANCODE_SPACE	", "0x29");
			DefineConstant("OS_SCANCODE_TAB		", "0x0D");
			DefineConstant("OS_SCANCODE_ENTER	", "0x5A");

			DefineConstant("OS_SCANCODE_SLASH	", "0x4A");
			DefineConstant("OS_SCANCODE_BACKSLASH	", "0x5D");
			DefineConstant("OS_SCANCODE_LEFT_BRACE	", "0x54");
			DefineConstant("OS_SCANCODE_RIGHT_BRACE	", "0x5B");
			DefineConstant("OS_SCANCODE_EQUALS	", "0x55");
			DefineConstant("OS_SCANCODE_BACKTICK	", "0x0E");
			DefineConstant("OS_SCANCODE_HYPHEN	", "0x4E");
			DefineConstant("OS_SCANCODE_SEMICOLON	", "0x4C");
			DefineConstant("OS_SCANCODE_QUOTE	", "0x52");
			DefineConstant("OS_SCANCODE_COMMA	", "0x41");
			DefineConstant("OS_SCANCODE_PERIOD	", "0x49");

			DefineConstant("OS_SCANCODE_NUM_DIVIDE 	 ", "0x14A");
			DefineConstant("OS_SCANCODE_NUM_MULTIPLY ", "0x7C");
			DefineConstant("OS_SCANCODE_NUM_SUBTRACT ", "0x7B");
			DefineConstant("OS_SCANCODE_NUM_ADD	 ", "0x79");
			DefineConstant("OS_SCANCODE_NUM_ENTER	 ", "0x15A");
			DefineConstant("OS_SCANCODE_NUM_POINT	 ", "0x71");
			DefineConstant("OS_SCANCODE_NUM_0	 ", "0x70");
			DefineConstant("OS_SCANCODE_NUM_1	 ", "0x69");
			DefineConstant("OS_SCANCODE_NUM_2	 ", "0x72");
			DefineConstant("OS_SCANCODE_NUM_3	 ", "0x7A");
			DefineConstant("OS_SCANCODE_NUM_4	 ", "0x6B");
			DefineConstant("OS_SCANCODE_NUM_5	 ", "0x73");
			DefineConstant("OS_SCANCODE_NUM_6	 ", "0x74");
			DefineConstant("OS_SCANCODE_NUM_7	 ", "0x6C");
			DefineConstant("OS_SCANCODE_NUM_8	 ", "0x75");
			DefineConstant("OS_SCANCODE_NUM_9	 ", "0x7D");

			// Both are sent when print screen is pressed.
			DefineConstant("OS_SCANCODE_PRINT_SCREEN_1 ", "0x112");
			DefineConstant("OS_SCANCODE_PRINT_SCREEN_2 ", "0x17C");

			DefineConstant("OS_SCANCODE_F1 ", "0x05");
			DefineConstant("OS_SCANCODE_F2 ", "0x06");
			DefineConstant("OS_SCANCODE_F3 ", "0x04");
			DefineConstant("OS_SCANCODE_F4 ", "0x0C");
			DefineConstant("OS_SCANCODE_F5 ", "0x03");
			DefineConstant("OS_SCANCODE_F6 ", "0x0B");
			DefineConstant("OS_SCANCODE_F7 ", "0x83");
			DefineConstant("OS_SCANCODE_F8 ", "0x0A");
			DefineConstant("OS_SCANCODE_F9 ", "0x01");
			DefineConstant("OS_SCANCODE_F10", "0x09");
			DefineConstant("OS_SCANCODE_F11", "0x78");
			DefineConstant("OS_SCANCODE_F12", "0x07");

			DefineConstant("OS_SCANCODE_ACPI_POWER 	", "0x137");
			DefineConstant("OS_SCANCODE_ACPI_SLEEP 	", "0x13F");
			DefineConstant("OS_SCANCODE_ACPI_WAKE  	", "0x15E");

			DefineConstant("OS_SCANCODE_MM_NEXT	", "0x14D");
			DefineConstant("OS_SCANCODE_MM_PREVIOUS	", "0x115");
			DefineConstant("OS_SCANCODE_MM_STOP	", "0x13B");
			DefineConstant("OS_SCANCODE_MM_PAUSE	", "0x134");
			DefineConstant("OS_SCANCODE_MM_MUTE	", "0x123");
			DefineConstant("OS_SCANCODE_MM_QUIETER	", "0x121");
			DefineConstant("OS_SCANCODE_MM_LOUDER	", "0x132");
			DefineConstant("OS_SCANCODE_MM_SELECT	", "0x150");
			DefineConstant("OS_SCANCODE_MM_EMAIL	", "0x148");
			DefineConstant("OS_SCANCODE_MM_CALC	", "0x12B");
			DefineConstant("OS_SCANCODE_MM_FILES	", "0x140");

			DefineConstant("OS_SCANCODE_WWW_SEARCH	", "0x110");
			DefineConstant("OS_SCANCODE_WWW_HOME	", "0x13A");
			DefineConstant("OS_SCANCODE_WWW_BACK	", "0x138");
			DefineConstant("OS_SCANCODE_WWW_FORWARD	", "0x130");
			DefineConstant("OS_SCANCODE_WWW_STOP	", "0x128");
			DefineConstant("OS_SCANCODE_WWW_REFRESH	", "0x120");
			DefineConstant("OS_SCANCODE_WWW_STARRED	", "0x118");

			DefineConstant("OS_ICON_NONE 			", "0");
			DefineConstant("OS_ICON_FILE 			", "1");
			DefineConstant("OS_ICON_FOLDER 			", "2");
			DefineConstant("OS_ICON_ERROR 			", "3");
			DefineConstant("OS_ICON_FORWARD			", "4");
			DefineConstant("OS_ICON_BACK 			", "5");
			DefineConstant("OS_ICON_PARENT			", "6");
			DefineConstant("OS_ICON_BOOKMARK		", "7");
			DefineConstant("OS_ICON_ROTATE_CLOCKWISE 	", "8");
			DefineConstant("OS_ICON_ROTATE_ANTI_CLOCKWISE 	", "9");
			DefineConstant("OS_ICON_MAGNIFYING_GLASS	", "10");
			DefineConstant("OS_ICON_SHUTDOWN		", "11");
			DefineConstant("OS_ICON_RUN			", "12");
			DefineConstant("OS_ICON_WARNING			", "13");
			DefineConstant("OS_ICON_RENAME			", "14");

			DefineConstant("OS_FLAGS_DEFAULT", "0");
			DefineConstant("OS_SUCCESS      ", "-1");

			// These must be negative.
			// See OSReadFileSync.
			DefineConstant("OS_ERROR_BUFFER_TOO_SMALL		", "-2");
			DefineConstant("OS_ERROR_UNKNOWN_OPERATION_FAILURE 	", "-7");
			DefineConstant("OS_ERROR_NO_MESSAGES_AVAILABLE		", "-9");
			DefineConstant("OS_ERROR_MESSAGE_QUEUE_FULL		", "-10");
			DefineConstant("OS_ERROR_MESSAGE_NOT_HANDLED_BY_GUI	", "-13");
			DefineConstant("OS_ERROR_PATH_NOT_WITHIN_MOUNTED_VOLUME	", "-14");
			DefineConstant("OS_ERROR_PATH_NOT_TRAVERSABLE		", "-15");
			DefineConstant("OS_ERROR_FILE_ALREADY_EXISTS		", "-19");
			DefineConstant("OS_ERROR_FILE_DOES_NOT_EXIST		", "-20");
			DefineConstant("OS_ERROR_DRIVE_ERROR_FILE_DAMAGED	", "-21");
			DefineConstant("OS_ERROR_ACCESS_NOT_WITHIN_FILE_BOUNDS	", "-22");
			DefineConstant("OS_ERROR_FILE_PERMISSION_NOT_GRANTED	", "-23");
			DefineConstant("OS_ERROR_FILE_IN_EXCLUSIVE_USE		", "-24");
			DefineConstant("OS_ERROR_FILE_CANNOT_GET_EXCLUSIVE_USE	", "-25");
			DefineConstant("OS_ERROR_INCORRECT_NODE_TYPE		", "-26");
			DefineConstant("OS_ERROR_EVENT_NOT_SET			", "-27");
			DefineConstant("OS_ERROR_TIMEOUT_REACHED		", "-29");
			DefineConstant("OS_ERROR_REQUEST_CLOSED_BEFORE_COMPLETE ", "-30");
			DefineConstant("OS_ERROR_NO_CHARACTER_AT_COORDINATE	", "-31");
			DefineConstant("OS_ERROR_FILE_ON_READ_ONLY_VOLUME	", "-32");
			DefineConstant("OS_ERROR_USER_CANCELED_IO		", "-33");
			DefineConstant("OS_ERROR_INVALID_DIMENSIONS		", "-34");
			DefineConstant("OS_ERROR_DRIVE_CONTROLLER_REPORTED	", "-35");
			DefineConstant("OS_ERROR_COULD_NOT_ISSUE_PACKET		", "-36");
			DefineConstant("OS_ERROR_HANDLE_TABLE_FULL		", "-37");
			DefineConstant("OS_ERROR_COULD_NOT_RESIZE_FILE		", "-38");
			DefineConstant("OS_ERROR_DIRECTORY_NOT_EMPTY		", "-39");
			DefineConstant("OS_ERROR_UNSUPPORTED_FILESYSTEM		", "-40");
			DefineConstant("OS_ERROR_NODE_ALREADY_DELETED		", "-41");
			DefineConstant("OS_ERROR_NODE_IS_ROOT			", "-42");
			DefineConstant("OS_ERROR_VOLUME_MISMATCH		", "-43");
			DefineConstant("OS_ERROR_TARGET_WITHIN_SOURCE		", "-44");
			DefineConstant("OS_ERROR_TARGET_INVALID_TYPE		", "-45");
			DefineConstant("OS_ERROR_NOTHING_TO_DRAW		", "-46");

			DefineConstant("OS_SYSTEM_CONSTANT_TIME_STAMP_UNITS_PER_MICROSECOND", "0");
			DefineConstant("OS_SYSTEM_CONSTANT_RIGHT_TO_LEFT_LAYOUT 	   ", "1");

			DefineConstant("OS_INVALID_HANDLE 		", "(OSHandle) (0)");
			DefineConstant("OS_CURRENT_THREAD	 	", "(OSHandle) (0x1000)");
			DefineConstant("OS_CURRENT_PROCESS	 	", "(OSHandle) (0x1001)");
			DefineConstant("OS_SURFACE_UI_SHEET		", "(OSHandle) (0x2000)");
			DefineConstant("OS_SURFACE_WALLPAPER		", "(OSHandle) (0x2001)");

			DefineConstant("OS_INVALID_OBJECT		", "nullptr");

			DefineConstant("OS_WAIT_NO_TIMEOUT", "-1");
			DefineConstant("OS_MAX_WAIT_COUNT ", "16");
		}

		{
			StartEnum("OSFatalError");
			EnumEntry("OS_FATAL_ERROR_INVALID_BUFFER");
			EnumEntry("OS_FATAL_ERROR_UNKNOWN_SYSCALL");
			EnumEntry("OS_FATAL_ERROR_INVALID_MEMORY_REGION");
			EnumEntry("OS_FATAL_ERROR_MEMORY_REGION_LOCKED_BY_KERNEL");
			EnumEntry("OS_FATAL_ERROR_PATH_LENGTH_EXCEEDS_LIMIT");
			EnumEntry("OS_FATAL_ERROR_INVALID_HANDLE"); // Note: this has to be fatal!! See the linear handle list
			EnumEntry("OS_FATAL_ERROR_MUTEX_NOT_ACQUIRED_BY_THREAD");
			EnumEntry("OS_FATAL_ERROR_MUTEX_ALREADY_ACQUIRED");
			EnumEntry("OS_FATAL_ERROR_BUFFER_NOT_ACCESSIBLE");
			EnumEntry("OS_FATAL_ERROR_SHARED_MEMORY_REGION_TOO_LARGE");
			EnumEntry("OS_FATAL_ERROR_SHARED_MEMORY_STILL_MAPPED");
			EnumEntry("OS_FATAL_ERROR_COULD_NOT_LOAD_FONT");
			EnumEntry("OS_FATAL_ERROR_COULD_NOT_DRAW_FONT");
			EnumEntry("OS_FATAL_ERROR_COULD_NOT_ALLOCATE_MEMORY");
			EnumEntry("OS_FATAL_ERROR_INCORRECT_FILE_ACCESS");
			EnumEntry("OS_FATAL_ERROR_TOO_MANY_WAIT_OBJECTS");
			EnumEntry("OS_FATAL_ERROR_INCORRECT_NODE_TYPE");
			EnumEntry("OS_FATAL_ERROR_PROCESSOR_EXCEPTION");
			EnumEntry("OS_FATAL_ERROR_INVALID_PANE_CHILD");
			EnumEntry("OS_FATAL_ERROR_INVALID_PANE_OBJECT");
			EnumEntry("OS_FATAL_ERROR_UNSUPPORTED_CALLBACK");
			EnumEntry("OS_FATAL_ERROR_MISSING_CALLBACK");
			EnumEntry("OS_FATAL_ERROR_UNKNOWN");
			EnumEntry("OS_FATAL_ERROR_RECURSIVE_BATCH");
			EnumEntry("OS_FATAL_ERROR_CORRUPT_HEAP");
			EnumEntry("OS_FATAL_ERROR_BAD_CALLBACK_OBJECT");
			EnumEntry("OS_FATAL_ERROR_RESIZE_GRID");
			EnumEntry("OS_FATAL_ERROR_OUT_OF_GRID_BOUNDS");
			EnumEntry("OS_FATAL_ERROR_OVERWRITE_GRID_OBJECT");
			EnumEntry("OS_FATAL_ERROR_CORRUPT_LINKED_LIST");
			EnumEntry("OS_FATAL_ERROR_NO_MENU_POSITION");
			EnumEntry("OS_FATAL_ERROR_BAD_OBJECT_TYPE");
			EnumEntry("OS_FATAL_ERROR_MESSAGE_SHOULD_BE_HANDLED");
			EnumEntry("OS_FATAL_ERROR_INDEX_OUT_OF_BOUNDS");
			EnumEntry("OS_FATAL_ERROR_INVALID_STRING_LENGTH");
			EnumEntry("OS_FATAL_ERROR_SPINLOCK_NOT_ACQUIRED");
			EnumEntry("OS_FATAL_ERROR_UNKNOWN_SNAPSHOT_TYPE");
			EnumEntry("OS_FATAL_ERROR_INVALID_INSTANCE");
			EnumEntry("OS_FATAL_ERROR_PROCESS_ALREADY_ATTACHED");
			EnumEntry("OS_FATAL_ERROR_INSTANCE_NOT_READY");
			EnumEntry("OS_FATAL_ERROR_PARENT_INSTANCE_PROCESS_MISMATCH");
			EnumEntry("OS_FATAL_ERROR_COUNT");
			EnumEntry("OS_FATAL_ERROR_NO_INSTANCE_PARENT");
			EnumEntry("OS_FATAL_ERROR_INCORRECT_OBJECT_PARENT");
			EnumEntry("OS_FATAL_ERROR_INTERNAL");
			EnumEntry("OS_FATAL_ERROR_SINGLE_LINE_TEXTBOX_CANNOT_WRAP");
			EndEnum("OSFatalError");
		}

		{
			StartEnum("OSSyscallType");
			EnumEntryAt("OS_SYSCALL_PRINT", "0x1000");
			EnumEntry("OS_SYSCALL_ALLOCATE");
			EnumEntry("OS_SYSCALL_FREE");
			EnumEntry("OS_SYSCALL_CREATE_PROCESS");
			EnumEntry("OS_SYSCALL_GET_CREATION_ARGUMENT");
			EnumEntry("OS_SYSCALL_CREATE_SURFACE");
			EnumEntry("OS_SYSCALL_GET_LINEAR_BUFFER");
			EnumEntry("OS_SYSCALL_INVALIDATE_RECTANGLE");
			EnumEntry("OS_SYSCALL_COPY_TO_SCREEN");
			EnumEntry("OS_SYSCALL_FORCE_SCREEN_UPDATE");
			EnumEntry("OS_SYSCALL_FILL_RECTANGLE");
			EnumEntry("OS_SYSCALL_COPY_SURFACE");
			EnumEntry("OS_SYSCALL_CLEAR_MODIFIED_REGION");
			EnumEntry("OS_SYSCALL_GET_MESSAGE");
			EnumEntry("OS_SYSCALL_POST_MESSAGE");
			EnumEntry("OS_SYSCALL_POST_MESSAGE_REMOTE");
			EnumEntry("OS_SYSCALL_WAIT_MESSAGE");
			EnumEntry("OS_SYSCALL_CREATE_WINDOW");
			EnumEntry("OS_SYSCALL_UPDATE_WINDOW");
			EnumEntry("OS_SYSCALL_DRAW_SURFACE");
			EnumEntry("OS_SYSCALL_CLOSE_HANDLE");
			EnumEntry("OS_SYSCALL_TERMINATE_THREAD");
			EnumEntry("OS_SYSCALL_CREATE_THREAD");
			EnumEntry("OS_SYSCALL_WAIT");
			EnumEntry("OS_SYSCALL_SHARE_MEMORY");
			EnumEntry("OS_SYSCALL_MAP_OBJECT");
			EnumEntry("OS_SYSCALL_OPEN_SHARED_MEMORY");
			EnumEntry("OS_SYSCALL_TERMINATE_PROCESS");
			EnumEntry("OS_SYSCALL_OPEN_NODE");
			EnumEntry("OS_SYSCALL_READ_FILE_SYNC");
			EnumEntry("OS_SYSCALL_WRITE_FILE_SYNC");
			EnumEntry("OS_SYSCALL_RESIZE_FILE");
			EnumEntry("OS_SYSCALL_CREATE_EVENT");
			EnumEntry("OS_SYSCALL_SET_EVENT");
			EnumEntry("OS_SYSCALL_RESET_EVENT");
			EnumEntry("OS_SYSCALL_POLL_EVENT");
			EnumEntry("OS_SYSCALL_REFRESH_NODE_INFORMATION");
			EnumEntry("OS_SYSCALL_SET_CURSOR_STYLE");
			EnumEntry("OS_SYSCALL_MOVE_WINDOW");
			EnumEntry("OS_SYSCALL_GET_WINDOW_BOUNDS");
			EnumEntry("OS_SYSCALL_REDRAW_ALL");
			EnumEntry("OS_SYSCALL_PAUSE_PROCESS");
			EnumEntry("OS_SYSCALL_CRASH_PROCESS");
			EnumEntry("OS_SYSCALL_GET_THREAD_ID");
			EnumEntry("OS_SYSCALL_ENUMERATE_DIRECTORY_CHILDREN");
			EnumEntry("OS_SYSCALL_READ_FILE_ASYNC");
			EnumEntry("OS_SYSCALL_WRITE_FILE_ASYNC");
			EnumEntry("OS_SYSCALL_GET_IO_REQUEST_PROGRESS");
			EnumEntry("OS_SYSCALL_CANCEL_IO_REQUEST");
			EnumEntry("OS_SYSCALL_BATCH");
			EnumEntry("OS_SYSCALL_NEED_WM_TIMER");
			EnumEntry("OS_SYSCALL_RESET_CLICK_CHAIN");
			EnumEntry("OS_SYSCALL_GET_CURSOR_POSITION");
			EnumEntry("OS_SYSCALL_COPY");
			EnumEntry("OS_SYSCALL_GET_CLIPBOARD_HEADER");
			EnumEntry("OS_SYSCALL_PASTE_TEXT");
			EnumEntry("OS_SYSCALL_DELETE_NODE");
			EnumEntry("OS_SYSCALL_MOVE_NODE");
			EnumEntry("OS_SYSCALL_READ_CONSTANT_BUFFER");
			EnumEntry("OS_SYSCALL_GET_PROCESS_STATE");
			EnumEntry("OS_SYSCALL_SHUTDOWN");
			EnumEntry("OS_SYSCALL_SET_FOCUSED_WINDOW");
			EnumEntry("OS_SYSCALL_YIELD_SCHEDULER");
			EnumEntry("OS_SYSCALL_GET_SYSTEM_CONSTANTS");
			EnumEntry("OS_SYSCALL_SLEEP");
			EnumEntry("OS_SYSCALL_TAKE_SYSTEM_SNAPSHOT");
			EnumEntry("OS_SYSCALL_OPEN_PROCESS");
			EnumEntry("OS_SYSCALL_SET_SYSTEM_CONSTANT");
			EnumEntry("OS_SYSCALL_OPEN_INSTANCE");
			EnumEntry("OS_SYSCALL_ATTACH_INSTANCE_TO_PROCESS");
			EnumEntry("OS_SYSCALL_SHARE_INSTANCE");
			EnumEntry("OS_SYSCALL_ISSUE_FOREIGN_COMMAND");
			EnumEntry("OS_SYSCALL_GET_REQUEST_RESPONSE");
			EnumEntry("OS_SYSCALL_SET_REQUEST_RESPONSE");
			EnumEntry("OS_SYSCALL_SET_TLS");
			EnumEntry("OS_SYSCALL_GET_SYSTEM_INFORMATION");
			EnumEntry("OS_SYSCALL_SHARE_CONSTANT_BUFFER");
			EndEnum("OSSyscallType");
		}
	}

	fclose(output);

	return 0;
}
