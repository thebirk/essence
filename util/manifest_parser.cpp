#ifndef MANIFEST_PARSER_LIBRARY
#define MANIFEST_PARSER_UTIL
#endif

#ifdef MANIFEST_PARSER_UTIL
#include <stdio.h>
#include <assert.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#define PrintStdout printf

#define OS_NO_CSTDLIB
#define OS_MANIFEST_HEADER
#include "../api/os.h"
#include "../api/utf8.h"

void OSCrashProcess(OSError x) {
	(void) x;
	assert(false);
}
#else
#define PrintStdout(...) do {} while(0)
#endif

/*
 * Manifest syntax:
 *
 * [section_type section_name]
 * numeric = 5; # Comment
 * string = "hello, world";
 * identifier = Something;
 */

struct Token {
	char *text;
	int bytes;

	enum {
		LEFT_BRACKET, RIGHT_BRACKET, EQUALS, SEMICOLON,
		IDENTIFIER, NUMERIC, STRING, PERCENT, BLOCK,
		LEFT_BRACE, RIGHT_BRACE, AT, DOUBLE_SEMICOLON,
		END,
	} type;
};

#define EVENT_END_SECTION (0)
#define EVENT_START_SECTION (1)
#define EVENT_ATTRIBUTE (2)
#define EVENT_LISTING (3)

typedef void (*ParseCallback)(Token attribute, Token section, Token variable, Token value, int event);

static void RemoveEscapeSequencesFromString(Token *token) {
	for (uintptr_t i = 0; i < (uintptr_t) token->bytes; i++) {
		if (token->text[i] == '\\') {
			if (token->text[i + 1] == '$') {
				token->text[i] = 0x11;
				token->text[i + 1] = 0x12;
			}
		}
	}
}

static bool NextToken(char *&buffer, Token *token, bool expect = false, bool parseBlocks = true) {
	Token original = *token;

	try_again:;
	int c = utf8_value(buffer);

	token->text = buffer;
	token->bytes = utf8_length_char(buffer);

	if (c == '[') {
		token->type = Token::LEFT_BRACKET;
	} else if (c == ']') {
		token->type = Token::RIGHT_BRACKET;
	} else if (c == '{') {
		if (parseBlocks) {
			uintptr_t level = 1;
			buffer = token->text + token->bytes;

			while (level) {
				Token token;

				if (!NextToken(buffer, &token, false, false)) {
					return false;
				}

				if (token.type == Token::LEFT_BRACE) level++;
				if (token.type == Token::RIGHT_BRACE) level--;
			}

			token->bytes = buffer - token->text;
			token->type = Token::BLOCK;
		} else {
			token->type = Token::LEFT_BRACE;
		}
	} else if (c == '}') {
		token->type = Token::RIGHT_BRACE;
	} else if (c == '=') {
		token->type = Token::EQUALS;
	} else if (c == ';') {
		token->type = Token::SEMICOLON;

		if (utf8_value(buffer + 1) == ';') {
			token->type = Token::DOUBLE_SEMICOLON;
			token->bytes++;
		}
	} else if (c == '%') {
		token->type = Token::PERCENT;
	} else if (c == '@') {
		token->type = Token::AT;
	} else if (c == 0) {
		token->type = Token::END;
		token->bytes = 0;
	} else if (c == ' ' || c == '\n' || c == '\t') {
		buffer++;
		goto try_again;
	} else if (c == '#') {
		while (*buffer != '\n') buffer++;
		goto try_again;
	} else if (isalpha(c) || c >= 0x80 || c == '_') {
		token->type = Token::IDENTIFIER;
		token->bytes = 0;

		while (isalpha(c) || c >= 0x80 || c == '_' || isdigit(c)) {
			token->bytes += utf8_length_char(buffer);
			buffer = token->text + token->bytes;
			c = utf8_value(buffer);
		}
	} else if (isdigit(c)) {
		token->type = Token::NUMERIC;
		token->bytes = 0;

		while (isdigit(c) || c == ',') {
			token->bytes += utf8_length_char(buffer);
			buffer = token->text + token->bytes;
			c = utf8_value(buffer);
		}
	} else if (c == '\'') {
		token->type = Token::STRING;
		buffer = token->text + token->bytes;
		c = utf8_value(buffer);

		while (c != '\'' && c) {
			token->bytes += utf8_length_char(buffer);
			buffer = token->text + token->bytes;
			c = utf8_value(buffer);
		}

		if (!c) {
			PrintStdout("Unexpected end of file in string\n");
			return false;
		}

		token->bytes++;
	} else if (c == '"') {
		token->type = Token::STRING;
		buffer = token->text + token->bytes;
		c = utf8_value(buffer);

		while (c != '"' && c) {
			token->bytes += utf8_length_char(buffer);
			buffer = token->text + token->bytes;
			c = utf8_value(buffer);
		}

		if (!c) {
			PrintStdout("Unexpected end of file in string\n");
			return false;
		}

		token->bytes++;
		RemoveEscapeSequencesFromString(token);
	} else {
		PrintStdout("Unexpected character in input: '%c'\n", c);
		return false;
	}

	buffer = token->text + token->bytes;

	if (expect && original.type != token->type) {
		PrintStdout("Unexpected token type. Found '%.*s'\n", token->bytes, token->text);
		return false;
	}

	return true;
}

bool CompareTokens(Token a, Token b) {
	if (a.bytes != b.bytes) return false;
	return 0 == memcmp(a.text, b.text, a.bytes);
}

bool CompareTokens(Token a, const char *string) {
	if (a.bytes != (int) strlen(string)) return false;
	return 0 == memcmp(a.text, string, a.bytes);
}

bool ParseManifest(char *text, ParseCallback callback) {
	Token token, section, attribute;
	attribute = (Token) {nullptr, 0, Token::IDENTIFIER};
	section = (Token) {nullptr, 0, Token::IDENTIFIER};

	while (true) {
		if (!NextToken(text, &token)) return false;

		if (token.type == Token::END) break;

		if (token.type == Token::LEFT_BRACKET) {
			callback(attribute, section, (Token) {}, (Token) {}, EVENT_END_SECTION);

			token.type = Token::IDENTIFIER;
			if (!NextToken(text, &token, true)) return false;
			Token token2;
			if (!NextToken(text, &token2)) return false;

			if (token2.type == Token::IDENTIFIER) {
				attribute = token;
				section = token2;
				token.type = Token::RIGHT_BRACKET;
				if (!NextToken(text, &token, true)) return false;
			} else if (token2.type == Token::RIGHT_BRACKET) {
				attribute = (Token) {nullptr, 0, Token::IDENTIFIER};
				section = token;
			} else {
				PrintStdout("Unexpected token type. Found '%.*s'\n", token2.bytes, token2.text);
				return false;
			}

			callback(attribute, section, (Token) {}, (Token) {}, EVENT_START_SECTION);
		} else if (token.type == Token::IDENTIFIER) {
			Token name = token;

			if (!NextToken(text, &token)) return false;

			if (token.type == Token::SEMICOLON) {
				callback(attribute, section, name, token, EVENT_LISTING);
			} else if (token.type == Token::EQUALS) {
				if (!NextToken(text, &token)) return false;

				if (token.type != Token::IDENTIFIER
						&& token.type != Token::STRING
						&& token.type != Token::NUMERIC
						&& token.type != Token::BLOCK) {
					PrintStdout("Unexpected token type. Found '%.*s'\n", token.bytes, token.text);
					return false;
				}

				callback(attribute, section, name, token, EVENT_ATTRIBUTE);

				token.type = Token::SEMICOLON;
				if (!NextToken(text, &token, true)) return false;
			} else {
				PrintStdout("Unexpected token type. Found '%.*s'\n", token.bytes, token.text);
				return false;
			}
		} else {
			PrintStdout("Unexpected token type. Found '%.*s'\n", token.bytes, token.text);
			return false;
		}
	}

	callback(attribute, section, (Token) {}, (Token) {}, EVENT_END_SECTION);

	return true;
}

Token RemoveQuotes(Token token) {
	if (*token.text == '"') {
		token.text++;
		token.bytes--;
	}

	if (token.text[token.bytes - 1] == '"') {
		token.bytes--;
	}

	return token;
}
