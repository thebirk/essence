void *OSCRTmemset(void *s, int c, size_t n) {
	uint8_t *s8 = (uint8_t *) s;
	for (uintptr_t i = 0; i < n; i++) {
		s8[i] = (uint8_t) c;
	}
	return s;
}

void *OSCRTmemcpy(void *dest, const void *src, size_t n) {
	uint8_t *dest8 = (uint8_t *) dest;
	const uint8_t *src8 = (const uint8_t *) src;
	for (uintptr_t i = 0; i < n; i++) {
		dest8[i] = src8[i];
	}
	return dest;
}

void *OSCRTmemmove(void *dest, const void *src, size_t n) {
	if ((uintptr_t) dest < (uintptr_t) src) {
		return OSCRTmemcpy(dest, src, n);
	} else {
		uint8_t *dest8 = (uint8_t *) dest;
		const uint8_t *src8 = (const uint8_t *) src;
		for (uintptr_t i = n; i; i--) {
			dest8[i - 1] = src8[i - 1];
		}
		return dest;
	}
}

size_t OSCRTstrlen(const char *s) {
	size_t n = 0;
	while (s[n]) n++;
	return n;
}

size_t OSCRTstrnlen(const char *s, size_t maxlen) {
	size_t n = 0;
	while (s[n] && maxlen--) n++;
	return n;
}

void *OSCRTmalloc(size_t size) {
	void *x = OSHeapAllocate(size, false);
	return x;
}

void *OSCRTcalloc(size_t num, size_t size) {
	return OSHeapAllocate(num * size, true);
}

void OSCRTfree(void *ptr) {
	OSHeapFree(ptr);
}

int OSCRTabs(int n) {
	if (n < 0)	return 0 - n;
	else		return n;
}

void *OSCRTrealloc(void *ptr, size_t size) {
	if (!ptr) return OSCRTmalloc(size);

	size_t oldSize = ((OSHeapRegion *) ((uint8_t *) ptr - 0x10))->size;
	
	if (!oldSize) {
		oldSize = ((OSHeapRegion *) ((uint8_t *) ptr - 0x10))->largeRegionSize;
	} else {
		oldSize -= 0x10;
	}

	void *newBlock = OSCRTmalloc(size);
	if (!newBlock) return nullptr;
	memcpy(newBlock, ptr, oldSize > size ? size : oldSize);
	OSCRTfree(ptr);
	return newBlock;
}

char *OSCRTgetenv(const char *name) {
	(void) name;
	return nullptr;
}

int OSCRTtoupper(int c) {
	if (c >= 'a' && c <= 'z') {
		return c - 'a' + 'z';
	} else {
		return c;
	}
}

int OSCRTtolower(int c) {
	if (c >= 'A' && c <= 'Z') {
		return c - 'A' + 'a';
	} else {
		return c;
	}
}

int OSCRTstrcasecmp(const char *s1, const char *s2) {
	while (true) {
		if (*s1 != *s2 && OSCRTtolower(*s1) != OSCRTtolower(*s2)) {
			if (*s1 == 0) return -1;
			else if (*s2 == 0) return 1;
			return *s1 - *s2;
		}

		if (*s1 == 0) {
			return 0;
		}

		s1++;
		s2++;
	}
}

int OSCRTstrncasecmp(const char *s1, const char *s2, size_t n) {
	while (n--) {
		if (*s1 != *s2 && OSCRTtolower(*s1) != OSCRTtolower(*s2)) {
			if (*s1 == 0) return -1;
			else if (*s2 == 0) return 1;
			return *s1 - *s2;
		}

		if (*s1 == 0) {
			return 0;
		}

		s1++;
		s2++;
	}

	return 0;
}

int OSCRTstrcmp(const char *s1, const char *s2) {
	while (true) {
		if (*s1 != *s2) {
			if (*s1 == 0) return -1;
			else if (*s2 == 0) return 1;
			return *s1 - *s2;
		}

		if (*s1 == 0) {
			return 0;
		}

		s1++;
		s2++;
	}
}

int OSCRTstrncmp(const char *s1, const char *s2, size_t n) {
	while (n--) {
		if (*s1 != *s2) {
			if (*s1 == 0) return -1;
			else if (*s2 == 0) return 1;
			return *s1 - *s2;
		}

		if (*s1 == 0) {
			return 0;
		}

		s1++;
		s2++;
	}

	return 0;
}

int OSCRTisspace(int c) {
	if (c == ' ')  return 1;
	if (c == '\f') return 1;
	if (c == '\n') return 1;
	if (c == '\r') return 1;
	if (c == '\t') return 1;
	if (c == '\v') return 1;

	return 0;
}

unsigned long int OSCRTstrtoul(const char *nptr, char **endptr, int base) {
	// TODO errno

	if (base > 36) return 0;

	while (OSCRTisspace(*nptr)) {
		nptr++;
	}

	if (*nptr == '+') {
		nptr++;
	} else if (*nptr == '-') {
		nptr++;
	}

	if (base == 0) {
		if (nptr[0] == '0' && (nptr[1] == 'x' || nptr[1] == 'X')) {
			base = 16;
			nptr += 2;
		} else if (nptr[0] == '0') {
			CF(Print)("WARNING: strtoul with base=0, detected octal\n");
			base = 8; // Why?!?
			nptr++;
		} else {
			base = 10;
		}
	}

	uint64_t value = 0;
	bool overflow = false;

	while (true) {
		int64_t digit = ConvertCharacterToDigit(*nptr, base);

		if (digit != -1) {
			nptr++;

			uint64_t x = value;
			value *= base;
			value += (uint64_t) digit;

			if (value / base != x) {
				overflow = true;
			}
		} else {
			break;
		}
	}

	if (overflow) {
		value = ULONG_MAX;
	}

	if (endptr) {
		*endptr = (char *) nptr;
	}

	return value;
}

size_t OSCRTstrcspn(const char *s, const char *reject) {
	size_t count = 0;

	while (true) {
		char character = *s;
		if (!character) return count;

		const char *search = reject;

		while (true) {
			char c = *search;

			if (!c) {
				goto match;
			} else if (character == c) {
				break;
			}

			search++;
		}

		return count;

		match:;
		count++;
		s++;
	}
}

char *OSCRTstrsep(char **stringp, const char *delim) {
	char *string = *stringp;

	if (!string) {
		return NULL;
	}

	size_t tokenLength = OSCRTstrcspn(string, delim);

	if (string[tokenLength] == 0) {
		*stringp = NULL;
	} else {
		string[tokenLength] = 0;
		*stringp = string + tokenLength + 1;
	}

	return string;
}

char *OSCRTstrcat(char *dest, const char *src) {
	char *o = dest;
	dest += OSCRTstrlen(dest);

	while (*src) {
		*dest = *src;
		src++;
		dest++;
	}

	*dest = 0;

	return o;
}

long int OSCRTstrtol(const char *nptr, char **endptr, int base) {
	// TODO errno

	if (base > 36) return 0;

	while (OSCRTisspace(*nptr)) {
		nptr++;
	}

	bool positive = true;

	if (*nptr == '+') {
		positive = true;
		nptr++;
	} else if (*nptr == '-') {
		positive = false;
		nptr++;
	}

	if (base == 0) {
		if (nptr[0] == '0' && (nptr[1] == 'x' || nptr[1] == 'X')) {
			base = 16;
			nptr += 2;
		} else if (nptr[0] == '0') {
			CF(Print)("WARNING: strtol with base=0, detected octal\n");
			base = 8; // Why?!?
			nptr++;
		} else {
			base = 10;
		}
	}

	int64_t value = 0;
	bool overflow = false;

	while (true) {
		int64_t digit = ConvertCharacterToDigit(*nptr, base);

		if (digit != -1) {
			nptr++;

			int64_t x = value;
			value *= base;
			value += digit;

			if (value / base != x) {
				overflow = true;
			}
		} else {
			break;
		}
	}

	if (!positive) {
		value = -value;
	}

	if (overflow) {
		value = positive ? LONG_MAX : LONG_MIN;
	}

	if (endptr) {
		*endptr = (char *) nptr;
	}

	return value;
}

int OSCRTatoi(const char *nptr) {
	return (int) OSCRTstrtol(nptr, NULL, 10);
}

char *OSCRTstrstr(const char *haystack, const char *needle) {
	size_t haystackLength = OSCRTstrlen(haystack);
	size_t needleLength = OSCRTstrlen(needle);

	if (haystackLength < needleLength) {
		return nullptr;
	}

	for (uintptr_t i = 0; i <= haystackLength - needleLength; i++) {
		for (uintptr_t j = 0; j < needleLength; j++) {
			if (haystack[i + j] != needle[j]) {
				goto tryNext;
			}
		}

		return (char *) haystack + i;

		tryNext:;
	}

	return nullptr;
}

void OSCRTqsort(void *_base, size_t nmemb, size_t size, int (*compar)(const void *, const void *)) {
	if (nmemb <= 1) return;

	uint8_t *base = (uint8_t *) _base;
	uint8_t swap[size];

	intptr_t i = -1, j = nmemb;

	while (true) {
		while (compar(base + ++i * size, base) < 0);
		while (compar(base + --j * size, base) > 0);

		if (i >= j) break;

		OSCRTmemcpy(swap, base + i * size, size);
		OSCRTmemcpy(base + i * size, base + j * size, size);
		OSCRTmemcpy(base + j * size, swap, size);
	}

	OSCRTqsort(base, ++j, size, compar);
	OSCRTqsort(base + j * size, nmemb - j, size, compar);
}

char *OSCRTstrcpy(char *dest, const char *src) {
	size_t stringLength = OSCRTstrlen(src);
	OSCRTmemcpy(dest, src, stringLength + 1);
	return dest;
}

char *OSCRTstpcpy(char *dest, const char *src) {
	size_t stringLength = OSCRTstrlen(src);
	OSCRTmemcpy(dest, src, stringLength + 1);
	return dest + stringLength;
}

size_t OSCRTstrspn(const char *s, const char *accept) {
	size_t count = 0;

	while (true) {
		char character = *s;

		const char *search = accept;

		while (true) {
			char c = *search;

			if (!c) {
				break;
			} else if (character == c) {
				goto match;
			}

			search++;
		}

		return count;

		match:;
		count++;
		s++;
	}
}

char *OSCRTstrrchr(const char *s, int c) {
	const char *start = s;
	if (!s[0]) return NULL;
	s += strlen(s) - 1;

	while (true) {
		if (*s == c) {
			return (char *) s;
		}

		if (s == start) {
			return NULL;
		}

		s--;
	}
}

char *OSCRTstrchr(const char *s, int c) {
	while (true) {
		if (*s == c) {
			return (char *) s;
		}

		if (*s == 0) {
			return NULL;
		}

		s++;
	}
}

char *OSCRTstrncpy(char *dest, const char *src, size_t n) {
	size_t i;

	for (i = 0; i < n && src[i]; i++) {
		dest[i] = src[i];
	}

	for (; i < n; i++) {
		dest[i] = 0;
	}

	return dest;
}

int OSCRTmemcmp(const void *s1, const void *s2, size_t n) {
	return CF(CompareBytes)((void *) s1, (void *) s2, n);
}

void *OSCRTmemchr(const void *_s, int _c, size_t n) {
	uint8_t *s = (uint8_t *) _s;
	uint8_t c = (uint8_t) _c;

	for (uintptr_t i = 0; i < n; i++) {
		if (s[i] == c) {
			return s + i;
		}
	}

	return nullptr;
}

int OSCRTisalpha(int c) {
	return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
}

int OSCRTisdigit(int c) {
	return (c >= '0' && c <= '9');
}

int OSCRTrand() {
	uint8_t a = OSGetRandomByte();
	uint8_t b = OSGetRandomByte();
	uint8_t c = OSGetRandomByte();
	return (a << 16) | (b << 8) | (c << 0);
}

int OSCRTisalnum(int c) {
	return isalpha(c) || isdigit(c);
}

int OSCRTiscntrl(int c) {
	return c < 0x20 || c == 0x7F;
}

int OSCRTisgraph(int c) {
	return c > ' ' && c < 0x7F;
}

int OSCRTislower(int c) {
	return c >= 'a' && c <= 'z';
}

int OSCRTisprint(int c) {
	return c >= ' ' && c < 127;
}

int OSCRTispunct(int c) {
	return c != ' ' && !isalnum(c);
}

int OSCRTisupper(int c) {
	return c >= 'A' && c <= 'Z';
}

int OSCRTisxdigit(int c) {
	return OSCRTisdigit(c) || (c >= 'A' && c <= 'F') || (c >= 'a' && c <= 'f');
}

char *OSCRTsetlocale(int category, const char *locale) {
	(void) category;
	(void) locale;
	return nullptr;
}

int OSCRTsystem(const char *command) {
	OSExecuteProgram(command, OSCStringLength(command));
	return 0;
}

void OSCRTsrand(unsigned int seed) {
	osRandomByteSeed = seed;
}

void OSCRTexit(int status) {
	(void) status;
	OSTerminateProcess(OS_CURRENT_PROCESS);
}

void OSCRTabort() {
	OSCrashProcess(OS_ERROR_UNKNOWN_OPERATION_FAILURE);
	while (true);
}

int OSCRTstrcoll(const char *s1, const char *s2) {
	return OSCompareStrings((char *) s1, (char *) s2, OSCStringLength(s1), OSCStringLength(s2));
}

char *OSCRTstrerror(int errnum) {
	(void) errnum;
	return (char*) "unknown operation failure";
}

char *OSCRTstrpbrk(const char *s, const char *accept) {
	size_t l1 = OSCStringLength(s), l2 = OSCStringLength(accept);

	for (uintptr_t i = 0; i <  l1; i++) {
		char c = s[i];

		for (uintptr_t j = 0; j < l2; j++) {
			if (accept[j] == c) {
				return (char *) (i + s);
			}
		}
	}

	return nullptr;
}

