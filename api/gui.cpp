#include <string.h>
#include <ctype.h>

static void EnterDebugger() {
	asm volatile ("xchg %bx,%bx");
}

// #define VISUALISE_REPAINTS

#define DIMENSION_PUSH (65535)

#define TAB_WIDTH (24)
#define TAB_LARGE_WIDTH (224)
#define TAB_BAND_LARGE_HEIGHT (32)
#define TAB_BAND_HEIGHT (26)

#define CARET_BLINK_HZ (1)
#define CARET_BLINK_PAUSE (2)

#define RESIZE_LEFT 		(1)
#define RESIZE_RIGHT 		(2)
#define RESIZE_TOP 		(4)
#define RESIZE_BOTTOM 		(8)
#define RESIZE_TOP_LEFT 	(5)
#define RESIZE_TOP_RIGHT 	(6)
#define RESIZE_BOTTOM_LEFT 	(9)
#define RESIZE_BOTTOM_RIGHT 	(10)
#define RESIZE_MOVE		(0)
#define RESIZE_NONE		(11)

#define SCROLLBAR_SIZE (17)
#define SCROLLBAR_MINIMUM (16)
#define SCROLLBAR_BUTTON_AMOUNT (64)

#define STANDARD_BORDER_SIZE (2)

#define ICON_TEXT_GAP (5)

#define SCROLLBAR_BUTTON_UP   ((void *) 1)
#define SCROLLBAR_BUTTON_DOWN ((void *) 2)
#define SCROLLBAR_NUDGE_UP    ((void *) 3)
#define SCROLLBAR_NUDGE_DOWN  ((void *) 4)

#define OS_MANIFEST_DEFINITIONS
#include "../bin/OS/standard.manifest.h"

uint32_t STANDARD_BACKGROUND_COLOR = 0xF5F6F9;

uint32_t TEXT_COLOR_DEFAULT = 0x000515;
uint32_t TEXT_COLOR_DISABLED = 0x777777;
uint32_t TEXT_COLOR_DISABLED_SHADOW = 0xEEEEEE;
uint32_t TEXT_COLOR_HEADING = 0x003296;
uint32_t TEXT_COLOR_TITLEBAR = 0xFFFFFF;
uint32_t TEXT_COLOR_TOOLBAR = 0xFFFFFF;

uint32_t TEXTBOX_SELECTED_COLOR_1 = 0xFFC4D9F9;
uint32_t TEXTBOX_SELECTED_COLOR_2 = 0xFFDDDDDD;

uint32_t DISABLE_TEXT_SHADOWS = 1;
uint32_t TEXT_SHADOWS_OFFSET = 0;

// TODO Instancing.
// 	- Introduce standard IPC interfaces, e.g. for scripting languages.
// 	- Switching the instance of the current window.
// 	- Multiple windows for one instance.
// 	- Single instance programs.
// 	- Destroying headless instances.
// 	- Instance request errors.
// TODO Loading GUI layouts from manifests.
// 	- GUI editor.
// TODO Keyboard controls.
// 	- Access keys.
// TODO Advanced layouts.
// 	- Multiple-cell positions.
// 	- Improve wrapping.
// 	- Right-to-left. (started)
// TODO New controls.
// 	- Comboboxes.
// 	- Tabs. (started)
// 	- Toggle buttons.
// 	- Splitter.
// 	- Spinner.
// 	- Graphs.
// 	- Playlists.
// TODO Standard dialogs (pick font/color/folder).
// TODO Tooltips.
// TODO Menus have a few unneeded pixels at the bottom.

struct UIImage {
	OSRectangle region;
	OSRectangle border;
	OSDrawMode drawMode;

	uint8_t boxStyle;
	uint32_t boxColor;

	UIImage Translate(int x, int y) {
		UIImage a = *this;
		a.region.left += x;
		a.region.right += x;
		a.border.left += x;
		a.border.right += x;
		a.region.top += y;
		a.region.bottom += y;
		a.border.top += y;
		a.border.bottom += y;
		return a;
	}
};

// Moved to a separate file because they make GVIM lag!!
#include "ui_images.cpp"

static const int totalBorderWidth = 6 + 6;
static const int totalBorderHeight = 6 + 24 + 6;

struct GUIObject : APIObject {
	OSNotificationCallback notificationCallback;
	struct Window *window;
	OSRectangle bounds, cellBounds, inputBounds;
	uint16_t descendentInvalidationFlags;
	uint16_t layout;
	uint16_t preferredWidth, preferredHeight;
	uint16_t horizontalMargin : 4, verticalMargin : 4,
		verbose : 1,
		suggestWidth : 1, // Prevent recalculation of the preferred[Width/Height]
		suggestHeight : 1, // on MEASURE messages with grids.
		relayout : 1, repaint : 1,
		tabStop : 1, disabled : 1,
		isMenubar : 1;
};

#define DESCENDENT_REPAINT  (1)
#define DESCENDENT_RELAYOUT (2)

struct OSInstance : GUIObject { 
	void *argument;
	OSCommand *builtinCommands, *customCommands;
	OSHandle handle;
	uint8_t foreign : 1, headless : 1, modified : 1;
	void *data; size_t dataBytes;
	OSString filename;
	OSObject fileDialog;
	Window *window;
};

struct Control : GUIObject {
	// Current size: ~290 bytes.
	// Is this too big?
		
#define UI_IMAGE_NORMAL (0)
#define UI_IMAGE_DISABLED (1)
#define UI_IMAGE_HOVER (2)
#define UI_IMAGE_DRAG (3)
	UIImage **backgrounds;
	UIImage *icon;

	void *context;

	OSCommand *command;
	LinkedItem<Control> commandItem;

	OSMenuTemplate *rightClickMenu;

	OSString text;
	OSRectangle textBounds;
	uint32_t textColor;
	uint8_t textSize, textAlign;
	uint8_t textShadow : 1, 
		textBold : 1,
		textShadowBlur : 1,
		customTextRendering : 1;

	// Configuration:

	uint32_t focusable : 1,
		checkable : 1,
		radioCheck : 1,
		useClickRepeat : 1,

		noAnimations : 1,
		noDisabledTextColorChange : 1,
		ignoreActivationClicks : 1,
		drawParentBackground : 1,

		additionalCheckedBackgrounds : 1,
		additionalCheckedIcons : 1,
		hasFocusedBackground : 1,
		centerIcons : 1,
		iconHasVariants : 1,
		iconHasFocusVariant : 1,

		keepCustomCursorWhenDisabled : 1,
		cursor : 5;

	// State:

	uint32_t isChecked : 1, 
		firstPaint : 1,
		repaintCustomOnly : 1,
		pressedByKeyboard : 1;

	LinkedItem<Control> timerControlItem;

	// Animation data:
	uint8_t timerHz, timerStep;
	uint8_t animationStep, finalAnimationStep;
	uint8_t from1, from2, from3, from4, from5;
	uint8_t current1, current2, current3, current4, current5;
};

struct Tab {
	OSString text;
	uint8_t hoverAnimation;
	int width, widthTarget;
	bool newTabButton;
};

struct TabBand : Control {
	int activeTab, hoverTab;
	bool newTabPressed;
	unsigned flags;

	Tab *tabs;
	size_t tabCount;
};

struct MenuItem : Control {
	OSMenuItem item;
	Window *child;
	bool menubar;
};

struct ProgressBar : Control {
	int minimum, maximum, value;
};

struct WindowResizeControl : Control {
	unsigned direction;
};

struct Grid : GUIObject {
	unsigned columns, rows;
	GUIObject **objects;
	int *widths, *heights;
	int *minimumWidths, *minimumHeights;
	OSGridStyle style;
	OSRectangle borderSize;
	uint16_t gapSize;
	UIImage *background;
	uint32_t backgroundColor;
	int xOffset, yOffset;
	bool treatPreferredDimensionsAsMinima; // Used with scroll panes for PUSH objects.
	unsigned defaultLayout;
	bool noRightToLeftLayout;
};

struct Slider : Grid {
	int minimum, maximum, value, minorTickSpacing, majorTickSpacing, mode, previousClickAdjustment;
	struct SliderHandle *handle;
};

struct SliderHandle : Control {
	Slider *slider;
};

struct Scrollbar : Grid {
	bool enabled;
	bool orientation;
	bool automaticallyUpdatePosition;

	int contentSize;
	int viewportSize;
	int height;

	int anchor;
	float position;
	int maxPosition;
	int size;

	OSCommand *commands;
};

struct Window : GUIObject {
	OSHandle window, surface;

	// For OS_CREATE_WINDOW_MENU: 1 x 1.
	// For OS_CREATE_WINDOW_NORMAL: 3 x 4.
	// 	- Content pane at 1, 2.
	// 	- Or if OS_CREATE_WINDOW_WITH_MENUBAR,
	// 		- This is a subgrid, 1 x 2
	// 		- Menubar at 0, 0.
	// 		- Content pane at 0, 1.
	Grid *root;

	unsigned flags;
	OSCursorStyle cursor, cursorOld;

	struct Control *pressed,      // Mouse is pressing the control.
		       *hover,        // Mouse is hovering over the control.
		       *focus, 	      // Control has strong focus.
		       *lastFocus,    // Control has weak focus.
		       *defaultFocus, // Control receives focus if no other control has focus.
		       *buttonFocus;  // The last button to receive focus; use with ENTER.
	struct Grid *hoverGrid;

	int width, height;
	int minimumWidth, minimumHeight;

	LinkedList<Control> timerControls;
	int timerHz, caretBlinkStep, caretBlinkPause;

	uint8_t destroyed : 1, 
		created : 1, 
		activated : 1, 
		temporaryInstance : 1, 
		hasMenuParent : 1, 
		willUpdateAfterMessageProcessing : 1;

	OSCommand *dialogCommands;
	LinkedItem<Window> windowItem, repaintItem;
	Window *modalParent;
	OSInstance *instance;
	OSString baseWindowTitle;
};

struct OpenMenu {
	Window *window;
	Control *source;
	bool fromMenubar;
};

static OpenMenu openMenus[8];
static unsigned openMenuCount;

static bool navigateMenuMode;
static bool navigateMenuUsedKey;
static MenuItem *navigateMenuItem;
static MenuItem *navigateMenuFirstItem;

struct GUIAllocationBlock {
	size_t allocationCount;
	size_t totalBytes;
	size_t allocatedBytes;
};

static GUIAllocationBlock *guiAllocationBlock;

static LinkedList<Window> allWindows;
static LinkedList<Window> repaintWindows;

#include "list_view.cpp"
#define IMPLEMENTATION

static void SetParentDescendentInvalidationFlags(GUIObject *object, uint16_t mask) {
	do {
		if (object->type == API_OBJECT_WINDOW) {
			Window *window = (Window *) object;

			if (!window->willUpdateAfterMessageProcessing) {
				window->willUpdateAfterMessageProcessing = true;

				OSMessage message;
				message.type = OS_MESSAGE_UPDATE_WINDOW;
				message.context = window;
				OSPostMessage(&message);
			}
		}

		object->descendentInvalidationFlags |= mask;
		object = (GUIObject *) object->parent;
	} while (object);
}

void OSStartGUIAllocationBlock(size_t bytes) {
	guiAllocationBlock = (GUIAllocationBlock *) OSHeapAllocate(bytes + sizeof(GUIAllocationBlock), false);
	guiAllocationBlock->allocationCount = 0;
	guiAllocationBlock->totalBytes = bytes;
	guiAllocationBlock->allocatedBytes = 0;
}

size_t OSEndGUIAllocationBlock() {
	size_t allocatedBytes = guiAllocationBlock->allocatedBytes;

	OSPrint("End GUI block %x, using %d/%d bytes (%d allocations).\n", guiAllocationBlock, allocatedBytes, guiAllocationBlock->totalBytes, guiAllocationBlock->allocationCount);

	if (!guiAllocationBlock->allocationCount) {
		OSHeapFree(guiAllocationBlock);
	}

	guiAllocationBlock = nullptr;
	return allocatedBytes;
}

static void *GUIAllocate(size_t bytes, bool clear) {
	bytes += 8;

	if (guiAllocationBlock) {
		guiAllocationBlock->allocatedBytes += bytes;

		if (guiAllocationBlock->allocatedBytes < guiAllocationBlock->totalBytes) {
			guiAllocationBlock->allocationCount++;
			uintptr_t *a = (uintptr_t *) (((uint8_t *) (guiAllocationBlock + 1)) + guiAllocationBlock->allocatedBytes - bytes);
			if (clear) OSZeroMemory(a, bytes);
			*a = (uintptr_t) guiAllocationBlock;
			return a + 1;
		}
	}

	uintptr_t *a = (uintptr_t *) OSHeapAllocate(bytes, clear);
	*a = (uintptr_t) -1;
	return a + 1;
}

static void GUIFree(void *address) {
	if (!address) {
		return;
	}

	uintptr_t *a = (uintptr_t *) address - 1;

	if (*a == (uintptr_t) -1) {
		OSHeapFree(a);
	} else {
		GUIAllocationBlock *block = (GUIAllocationBlock *) *a;

		if (!block) {
			OSCrashProcess(OS_FATAL_ERROR_INVALID_BUFFER);
		}

		block->allocationCount--;

		if (block->allocationCount == 0) {
			// OSPrint("Freeing GUI block %x\n", block);
			OSHeapFree(block);
		}
	}
}

static bool ClipRectangle(OSRectangle parent, OSRectangle rectangle, OSRectangle *output) {
	OSRectangle current = parent;
	OSRectangle intersection;

	if (!((current.left > rectangle.right && current.right > rectangle.left)
			|| (current.top > rectangle.bottom && current.bottom > rectangle.top))) {
		intersection.left = current.left > rectangle.left ? current.left : rectangle.left;
		intersection.top = current.top > rectangle.top ? current.top : rectangle.top;
		intersection.right = current.right < rectangle.right ? current.right : rectangle.right;
		intersection.bottom = current.bottom < rectangle.bottom ? current.bottom : rectangle.bottom;
	} else {
		intersection = OS_MAKE_RECTANGLE(0, 0, 0, 0);
	}

	if (output) {
		*output = intersection;
	}

	return intersection.left < intersection.right && intersection.top < intersection.bottom;
}

bool OSClipRectangle(OSRectangle parent, OSRectangle rectangle, OSRectangle *output) {
	return ClipRectangle(parent, rectangle, output);
}

void OSDebugGUIObject(OSObject _guiObject) {
	GUIObject *guiObject = (GUIObject *) _guiObject;
	guiObject->verbose = true;
}

static void CopyText(OSString buffer) {
	OSClipboardHeader header = {};
	header.format = OS_CLIPBOARD_FORMAT_TEXT;
	header.textBytes = buffer.bytes;
	OSSyscall(OS_SYSCALL_COPY, (uintptr_t) buffer.buffer, (uintptr_t) &header, 0, 0);
}

static size_t ClipboardTextBytes() {
	OSClipboardHeader clipboard;
	OSSyscall(OS_SYSCALL_GET_CLIPBOARD_HEADER, 0, (uintptr_t) &clipboard, 0, 0);
	return clipboard.textBytes;
}

static inline void RepaintControl(OSObject _control, bool customOnly = false) {
	Control *control = (Control *) _control;

	control->repaint = true;
	SetParentDescendentInvalidationFlags(control, DESCENDENT_REPAINT);

	if (!customOnly) {
		control->repaintCustomOnly = false;
	}
}

void OSRepaintControl(OSObject object) {
	RepaintControl(object, false);
}

static inline bool IsPointInRectangle(OSRectangle rectangle, int x, int y) {
	if (rectangle.left > x || rectangle.right <= x || rectangle.top > y || rectangle.bottom <= y) {
		return false;
	}
	
	return true;
}

void OSPackWindow(OSObject object) {
	OSMessage message;
	Window *window = (Window *) object;

	message.type = OS_MESSAGE_MEASURE;
	message.measure.parentWidth = window->width;
	message.measure.parentHeight = window->height;

	OSSendMessage(window->root, &message);

	int newWidth = message.measure.preferredWidth + 16;
	int newHeight = message.measure.preferredHeight + 16;

	if (!(window->flags & OS_CREATE_WINDOW_HEADLESS)) {
		OSRectangle bounds;
		OSSyscall(OS_SYSCALL_GET_WINDOW_BOUNDS, window->window, (uintptr_t) &bounds, 0, 0);

		bounds.right = bounds.left + newWidth;
		bounds.bottom = bounds.top + newHeight;

		OSSyscall(OS_SYSCALL_MOVE_WINDOW, window->window, (uintptr_t) &bounds, 0, 0);
	}

	window->width = newWidth;
	window->height = newHeight;

	message.type = OS_MESSAGE_LAYOUT;
	message.layout.left = 0;
	message.layout.top = 0;
	message.layout.right = window->width;
	message.layout.bottom = window->height;
	message.layout.force = true;
	message.layout.clip = OS_MAKE_RECTANGLE(0, window->width, 0, window->height);
	OSSendMessage(window->root, &message);
}

void OSSetFocusedWindow(OSObject object) {
	Window *window = (Window *) object;
	if (window->flags & OS_CREATE_WINDOW_HEADLESS) return;
	OSSyscall(OS_SYSCALL_SET_FOCUSED_WINDOW, window->window, 0, 0, 0);
}

void OSSetControlCommand(OSObject _control, OSCommand *command) {
	Control *control = (Control *) _control;
	OSCommandTemplate *_command = command->specification;
	
	if (_command->iconID) {
		control->icon = icons16 + _command->iconID;
		control->iconHasVariants = true;
	}

	control->command = command;
	control->commandItem.thisItem = control;
	control->checkable = _command->checkable;
	control->radioCheck = _command->radioCheck;

	control->commandItem.nextItem = (LinkedItem<Control> *) command->controls;
	command->controls = &control->commandItem;

	control->isChecked = command->checked;
	control->disabled = command->disabled;
	control->notificationCallback = command->notificationCallback;
}

static void ReceiveTimerMessages(Control *control) {
	if (!control->timerControlItem.list && control->window) {
		control->timerHz = 30; // TODO Make this 60?
		control->window->timerControls.InsertStart(&control->timerControlItem);
		control->timerControlItem.thisItem = control;
	} else {
		// TODO If there wasn't a window.
	}
}

void OSAnimateControl(OSObject _control, bool fast) {
	Control *control = (Control *) _control;

	// OSPrint("Animate control %x\n", control);
	
	if (control->firstPaint && !control->noAnimations && !osSystemConstants[OS_SYSTEM_CONSTANT_NO_FANCY_GRAPHICS]) {
		control->from1 = control->current1;
		control->from2 = control->current2;
		control->from3 = control->current3;
		control->from4 = control->current4;
		control->from5 = control->current5;

		control->animationStep = 0;
		control->finalAnimationStep = fast ? 4 : 16;

		ReceiveTimerMessages(control);
	} else {
		control->animationStep = 16;
		control->finalAnimationStep = 16;
	}

	RepaintControl(control);
}

static void StandardCellLayout(GUIObject *object) {
	uint16_t layout = object->layout;
	object->cellBounds = object->bounds;

	if (osSystemConstants[OS_SYSTEM_CONSTANT_RIGHT_TO_LEFT_LAYOUT]
			&& (layout & (OS_CELL_H_LEFT | OS_CELL_H_RIGHT))) {
		layout ^= OS_CELL_H_LEFT | OS_CELL_H_RIGHT;
	}

	int width = object->bounds.right - object->bounds.left;
	int height = object->bounds.bottom - object->bounds.top;

	int preferredWidth = (layout & OS_CELL_H_EXPAND) ? width : (object->preferredWidth + object->horizontalMargin * 2);
	int preferredHeight = (layout & OS_CELL_V_EXPAND) ? height : (object->preferredHeight + object->verticalMargin * 2);

	if (width >= preferredWidth) {
		if (layout & OS_CELL_H_INDENT_1) {
			object->bounds.left += 16;
		} else if (layout & OS_CELL_H_INDENT_2) {
			object->bounds.left += 32;
		} else if (layout & OS_CELL_H_LEFT) {
			object->bounds.right = object->bounds.left + preferredWidth;
		} else if (layout & OS_CELL_H_RIGHT) {
			object->bounds.left = object->bounds.right - preferredWidth;
		} else {
			object->bounds.left = object->bounds.left + width / 2 - preferredWidth / 2;
			object->bounds.right = object->bounds.left + preferredWidth;
		}
	}

	if (height > preferredHeight) {
		if (layout & OS_CELL_V_EXPAND) {
		} else if (layout & OS_CELL_V_TOP) {
			object->bounds.bottom = object->bounds.top + preferredHeight;
		} else if (layout & OS_CELL_V_BOTTOM) {
			object->bounds.top = object->bounds.bottom - preferredHeight;
		} else {
			object->bounds.top = object->bounds.top + height / 2 - preferredHeight / 2;
			object->bounds.bottom = object->bounds.top + preferredHeight;
		}
	}
}

static void SetGUIObjectProperty(GUIObject *object, OSMessage *message) {
	uintptr_t value = (uintptr_t) message->setProperty.value;

	switch (message->setProperty.index) {
		case OS_GUI_OBJECT_PROPERTY_SUGGESTED_WIDTH: {
			object->suggestWidth = true;
			object->preferredWidth = value;
			object->relayout = true;

			{
				OSMessage message;
				message.type = OS_MESSAGE_CHILD_UPDATED;
				OSSendMessage(object->parent, &message);
			}
		} break;

		case OS_GUI_OBJECT_PROPERTY_SUGGESTED_HEIGHT: {
			object->suggestHeight = true;
			object->preferredHeight = value;
			object->relayout = true;

			{
				OSMessage message;
				message.type = OS_MESSAGE_CHILD_UPDATED;
				OSSendMessage(object->parent, &message);
			}
		} break;
	}
}

OSObject OSGetFocusedControl(OSObject _window, bool ignoreWeakFocus) {
	Window *window = (Window *) _window;

	if (ignoreWeakFocus) {
		return window->focus;
	} else {
		return window->lastFocus;
	}
}

void OSRemoveFocusedControl(OSObject _window, bool removeWeakFocus) {
	Window *window = (Window *) _window;
	OSMessage message;

	// Remove any focus.
	if (window->focus) {
		message.type = OS_MESSAGE_END_FOCUS;
		OSSendMessage(window->focus, &message);
		window->focus = nullptr;
	}

	// Remove any last focus.
	if (window->lastFocus && removeWeakFocus) {
		message.type = OS_MESSAGE_END_LAST_FOCUS;
		OSSendMessage(window->lastFocus, &message);
		window->lastFocus = nullptr;

		if (window->defaultFocus) {
			OSSetFocusedControl(window->defaultFocus, false);
		}
	}
}

void OSSetFocusedControl(OSObject _control, bool asDefaultForWindow) {
	if (!_control) {
		return;
	}

	Control *control = (Control *) _control;
	Window *window = control->window;
	OSMessage message;

	if (control != window->focus) {
		OSRemoveFocusedControl(window, true);
	}

	// Give this control focus, if it doesn't already have it.
	if (control != window->focus) {
		window->focus = control;
		window->lastFocus = control;
		message.type = OS_MESSAGE_START_FOCUS;
		OSSendMessage(control, &message);
	}

	if (asDefaultForWindow) {
		window->defaultFocus = control;
	}
}

void DrawUIImage(OSMessage *message, OSRectangle rectangle, UIImage *image, uint8_t alpha, OSRectangle *_clip = nullptr, OSHandle _surface = OS_INVALID_HANDLE) {
	if (alpha >= 225) {
		alpha = 255;
	}

	OSRectangle clip = message ? message->paint.clip : *_clip;
	if (_clip) clip = *_clip;
	OSHandle surface = message ? message->paint.surface : _surface;
	if (_surface) surface = _surface;
	
	if (osSystemConstants[OS_SYSTEM_CONSTANT_NO_FANCY_GRAPHICS] && image->boxStyle) {
		OSDrawBox(surface, rectangle, image->boxStyle, image->boxColor, clip);
	} else {
		OSDrawSurfaceClipped(surface, OS_SURFACE_UI_SHEET, rectangle, image->region, 
				image->border, image->drawMode, alpha, clip);
	}
}

static OSCallbackResponse ProcessControlMessage(OSObject _object, OSMessage *message) {
	OSCallbackResponse response = OS_CALLBACK_HANDLED;
	Control *control = (Control *) _object;

	switch (message->type) {
		case OS_MESSAGE_LAYOUT: {
			if (control->cellBounds.left == message->layout.left
					&& control->cellBounds.right == message->layout.right
					&& control->cellBounds.top == message->layout.top
					&& control->cellBounds.bottom == message->layout.bottom
					&& !control->relayout) {
				break;
			}

			control->bounds = OS_MAKE_RECTANGLE(
					message->layout.left, message->layout.right,
					message->layout.top, message->layout.bottom);

			if (control->verbose) {
				OSPrint("Cell layout for control %x: %d->%d, %d->%d\n", control, control->bounds.left, control->bounds.right, control->bounds.top, control->bounds.bottom);
			}

			StandardCellLayout(control);
			ClipRectangle(message->layout.clip, control->bounds, &control->inputBounds);

			if (control->verbose) {
				OSPrint("Layout control %x: %d->%d, %d->%d; input %d->%d, %d->%d\n", control, control->bounds.left, control->bounds.right, control->bounds.top, control->bounds.bottom,
						control->inputBounds.left, control->inputBounds.right, control->inputBounds.top, control->inputBounds.bottom);
			}

			control->relayout = false;
			RepaintControl(control);
			SetParentDescendentInvalidationFlags(control, DESCENDENT_REPAINT);

			{
				OSMessage m = *message;
				m.type = OS_MESSAGE_LAYOUT_TEXT;
				OSSendMessage(control, &m);
			}
		} break;

		case OS_MESSAGE_LAYOUT_TEXT: {
			OSRectangle contentBounds = control->bounds;
			contentBounds.left += control->horizontalMargin;
			contentBounds.right -= control->horizontalMargin;
			contentBounds.top += control->verticalMargin;
			contentBounds.bottom -= control->verticalMargin;

			control->textBounds = contentBounds;

			if (control->icon) {
				control->textBounds.left += control->icon->region.right - control->icon->region.left + ICON_TEXT_GAP;
			}
		} break;

		case OS_MESSAGE_MEASURE: {
			message->measure.preferredWidth = control->preferredWidth + control->horizontalMargin * 2;
			message->measure.preferredHeight = control->preferredHeight + control->verticalMargin * 2;
		} break;

		case OS_MESSAGE_KEY_PRESSED: {
			if (control->tabStop && control->window->lastFocus != control) {
				OSSetFocusedControl(control, false);
			} else {
				response = OS_CALLBACK_NOT_HANDLED;
			}
		} break;

		case OS_MESSAGE_PAINT: {
			if (control->repaint || message->paint.force) {
				// if (!control->firstPaint) OSPrint("first paint %x\n", control);
				control->firstPaint = true;

#if 0
				if (control->window->focus == control) {
					OSFillRectangle(message->paint.surface, message->paint.clip, OSColor(255, 0, 255));
					break;
				}
#endif

				if (control->verbose) {
					OSPrint("Painting control %x\n", control);
				}

				bool menuSource;
				bool normal, hover, pressed, disabled, focused;
				uint32_t textShadowColor, textColor;

				OSRectangle contentBounds = control->bounds;
				contentBounds.left += control->horizontalMargin;
				contentBounds.right -= control->horizontalMargin;
				contentBounds.top += control->verticalMargin;
				contentBounds.bottom -= control->verticalMargin;
				if (control->repaintCustomOnly && !message->paint.force) {
					goto repaintCustom;
				} else {
					control->repaintCustomOnly = false;
				}
				
				if (control->drawParentBackground) {
					OSMessage m = *message;
					m.type = OS_MESSAGE_PAINT_BACKGROUND;
					m.paintBackground.surface = message->paint.surface;
					ClipRectangle(message->paint.clip, control->bounds, &m.paintBackground.clip);
					OSSendMessage(control->parent, &m);
				}

				menuSource = false;

				for (uintptr_t i = 0; i < openMenuCount; i++) {
					if (openMenus[i].source == control) {
						menuSource = true;
						break;
					}
				}

				disabled = control->disabled;
				pressed = ((control->window->pressed == control && (control->window->hover == control || control->pressedByKeyboard)) 
						|| (control->window->focus == control && !control->hasFocusedBackground) || menuSource) && !disabled;
				hover = (control->window->hover == control || control->window->pressed == control) && !pressed && !disabled;
				focused = ((control->window->focus == control || control->window->buttonFocus == control || (navigateMenuItem == control && navigateMenuMode)) 
						&& control->hasFocusedBackground) && !pressed && !hover && (!disabled || (navigateMenuItem == control && navigateMenuMode));
				normal = !hover && !pressed && !disabled && !focused;

				if (focused) disabled = false;

				control->current1 = ((normal   ? 15 : 0) - control->from1) * control->animationStep / control->finalAnimationStep + control->from1;
				control->current2 = ((hover    ? 15 : 0) - control->from2) * control->animationStep / control->finalAnimationStep + control->from2;
				control->current3 = ((pressed  ? 15 : 0) - control->from3) * control->animationStep / control->finalAnimationStep + control->from3;
				control->current4 = ((disabled ? 15 : 0) - control->from4) * control->animationStep / control->finalAnimationStep + control->from4;
				control->current5 = ((focused  ? 15 : 0) - control->from5) * control->animationStep / control->finalAnimationStep + control->from5;

				if (control->backgrounds) {
					uintptr_t offset = (control->isChecked && control->additionalCheckedBackgrounds) ? 4 : 0;

					if (control->backgrounds[offset + 0]) {
						DrawUIImage(message, control->bounds, control->backgrounds[offset + 0], 0xFF);
					}

					if (control->backgrounds[offset + 2] && control->current2) {
						DrawUIImage(message, control->bounds, control->backgrounds[offset + 2], 0xF * control->current2);
					}

					if (control->backgrounds[offset + 3] && control->current3) {
						DrawUIImage(message, control->bounds, control->backgrounds[offset + 3], 0xF * control->current3);
					}

					if (control->backgrounds[offset + 1] && control->current4) {
						DrawUIImage(message, control->bounds, control->backgrounds[offset + 1], 0xF * control->current4);
					}

					if (control->current5 && control->backgrounds[offset + 4]) {
						DrawUIImage(message, control->bounds, control->backgrounds[offset + 4], 0xF * control->current5);
					}
				}

				if (control->icon) {
					OSRectangle bounds = contentBounds;
					UIImage *icon = control->icon;
					
					if (control->centerIcons) {
						bounds.left += (bounds.right - bounds.left) / 2 - (icon->region.right - icon->region.left) / 2;
						bounds.right = bounds.left + icon->region.right - icon->region.left;
						bounds.top += (bounds.bottom - bounds.top) / 2 - (icon->region.bottom - icon->region.top) / 2;
						bounds.bottom = bounds.top + icon->region.bottom - icon->region.top;
					} else {
						bounds.right = bounds.left + icon->region.right - icon->region.left;
						bounds.top += (bounds.bottom - bounds.top) / 2 - (icon->region.bottom - icon->region.top) / 2;
						bounds.bottom = bounds.top + icon->region.bottom - icon->region.top;
					}

					uintptr_t offset = (control->isChecked && control->additionalCheckedIcons) ? (control->iconHasFocusVariant ? 5 : 4) : 0;

					if (control->current1 || !control->iconHasVariants) {
						UIImage icon = control->icon->Translate((control->icon->region.right - control->icon->region.left) * (0 + offset), 0);
						DrawUIImage(message, bounds, &icon, 0xFF);
					}

					if (control->current2 && control->iconHasVariants) {
						UIImage icon = control->icon->Translate((control->icon->region.right - control->icon->region.left) * (3 + offset), 0);
						DrawUIImage(message, bounds, &icon, 0xF * control->current2);
					}

					if (control->current3 && control->iconHasVariants) {
						UIImage icon = control->icon->Translate((control->icon->region.right - control->icon->region.left) * (2 + offset), 0);
						DrawUIImage(message, bounds, &icon, 0xF * control->current3);
					}

					if (control->current4 && control->iconHasVariants) {
						UIImage icon = control->icon->Translate((control->icon->region.right - control->icon->region.left) * (1 + offset), 0);
						DrawUIImage(message, bounds, &icon, 0xF * control->current4);
					}

					if (control->current5 && control->iconHasVariants) {
						UIImage icon = control->icon->Translate((control->icon->region.right - control->icon->region.left) * ((control->iconHasFocusVariant ? 4 : 3) + offset), 0);
						DrawUIImage(message, bounds, &icon, 0xF * control->current5);
					}
				}

				textColor = control->textColor ? control->textColor : TEXT_COLOR_DEFAULT;
				textShadowColor = 0xFFFFFFFF - textColor;

				if (control->disabled && !control->noDisabledTextColorChange) {
#if 0
					textColor ^= TEXT_COLOR_DISABLED;
					textShadowColor = TEXT_COLOR_DISABLED_SHADOW;
#endif
					textColor &= 0xFFFFFF;
					textColor |= 0x80000000;
					textShadowColor &= 0xFFFFFF;
					textShadowColor |= 0x80000000;
				}

				if (!control->customTextRendering) {
					OSRectangle textBounds = control->textBounds;

					if (control->textShadow && !DISABLE_TEXT_SHADOWS) {
						OSRectangle bounds = textBounds;
						bounds.top++; bounds.bottom++; 
						if (TEXT_SHADOWS_OFFSET) { bounds.left++; bounds.right++; }

						OSDrawString(message->paint.surface, bounds, &control->text, control->textSize,
								control->textAlign, textShadowColor, -1, control->textBold ? OS_STANDARD_FONT_BOLD : OS_STANDARD_FONT_REGULAR, message->paint.clip, 
								!TEXT_SHADOWS_OFFSET && control->textShadowBlur ? 3 : 0);
					}

					OSDrawString(message->paint.surface, textBounds, &control->text, control->textSize,
							control->textAlign, textColor, -1, control->textBold ? OS_STANDARD_FONT_BOLD : OS_STANDARD_FONT_REGULAR, message->paint.clip, 0);
				}

				repaintCustom:;

				{
					OSMessage m = *message;
					m.type = OS_MESSAGE_CUSTOM_PAINT;
					OSSendMessage(control, &m);
				}

				control->repaint = false;
				control->repaintCustomOnly = true;
			}
		} break;

		case OS_MESSAGE_PARENT_UPDATED: {
			control->window = (Window *) message->parentUpdated.window;
			control->animationStep = 16;
			control->finalAnimationStep = 16;
			RepaintControl(control);
		} break;

		case OS_MESSAGE_DESTROY: {
			if (control->timerControlItem.list) {
				control->window->timerControls.Remove(&control->timerControlItem);
			}

			if (control->command) {
				OSCommand *command = control->command;

				LinkedItem<Control> **previous = (LinkedItem<Control> **) &command->controls;
				LinkedItem<Control> *item = (LinkedItem<Control> *) command->controls;

				while (item) {
					if (item->thisItem == control) {
						*previous = item->nextItem;
					}

					previous = &item->nextItem;
					item = item->nextItem;
				}
			}

			if (control->window->hover == control) control->window->hover = nullptr;
			if (control->window->pressed == control) control->window->pressed = nullptr;
			if (control->window->defaultFocus == control) control->window->defaultFocus = nullptr;
			if (control->window->focus == control) OSRemoveFocusedControl(control->window, true);
			if (control->window->buttonFocus == control) control->window->buttonFocus = nullptr;

			GUIFree(control->text.buffer);
			GUIFree(control);
		} break;

		case OS_MESSAGE_HIT_TEST: {
			message->hitTest.result = IsPointInRectangle(control->inputBounds, message->hitTest.positionX, message->hitTest.positionY);
		} break;

		case OS_MESSAGE_MOUSE_RIGHT_RELEASED: {
			if (control->rightClickMenu) {
				OSCreateMenu(control->rightClickMenu, control, OS_CREATE_MENU_AT_CURSOR, OS_FLAGS_DEFAULT, control->window->instance);
			}
		} break;

		case OS_MESSAGE_START_HOVER:
		case OS_MESSAGE_END_HOVER:
		case OS_MESSAGE_START_FOCUS:
		case OS_MESSAGE_END_FOCUS:
		case OS_MESSAGE_END_LAST_FOCUS:
		case OS_MESSAGE_START_PRESS:
		case OS_MESSAGE_END_PRESS: {
			OSAnimateControl(control, message->type == OS_MESSAGE_START_PRESS || message->type == OS_MESSAGE_START_FOCUS);
		} break;

		case OS_MESSAGE_MOUSE_MOVED: {
			if (!IsPointInRectangle(control->inputBounds, message->mouseMoved.newPositionX, message->mouseMoved.newPositionY)) {
				break;
			}

			if (control->window->hover != control) {
				control->window->hover = control;

				OSMessage message;
				message.type = OS_MESSAGE_START_HOVER;
				OSSendMessage(control, &message);
			}
		} break;

		case OS_MESSAGE_WM_TIMER: {
			if (control->animationStep == control->finalAnimationStep) {
				control->window->timerControls.Remove(&control->timerControlItem);
			} else {
				control->animationStep++;
				RepaintControl(control);
			}
		} break;

		case OS_MESSAGE_SET_PROPERTY: {
			if (message->setProperty.index == OS_CONTROL_PROPERTY_CURSOR) {
				control->cursor = (uintptr_t) message->setProperty.value;
			}

			SetGUIObjectProperty(control, message);
		} break;

		case OS_MESSAGE_DISABLE: {
			if (control->disabled == message->disable.disabled) break;
			control->disabled = message->disable.disabled;
			
			OSAnimateControl(control, false);
			
			if (control->window->focus == control) {
				OSRemoveFocusedControl(control->window, true);
			}
		} break;

		default: {
			response = OS_CALLBACK_NOT_HANDLED;
		} break;
	}

	return response;
}

static void CreateString(const char *text, size_t textBytes, OSString *string, size_t characterCount = 0) {
	GUIFree(string->buffer);
	string->buffer = (char *) GUIAllocate(textBytes, false);
	string->bytes = textBytes;
	string->characters = characterCount;
	OSCopyMemory(string->buffer, text, textBytes);

	char *m = string->buffer;

	if (!string->characters) {
		while (m < string->buffer + textBytes) {
			m = utf8_advance(m);
			string->characters++;
		}
	}
}

static OSCallbackResponse ProcessWindowResizeHandleMessage(OSObject _object, OSMessage *message) {
	OSCallbackResponse response = OS_CALLBACK_HANDLED;
	WindowResizeControl *control = (WindowResizeControl *) _object;

	if (message->type == OS_MESSAGE_MOUSE_DRAGGED && control->direction != RESIZE_NONE) {
		Window *window = control->window;

		if (window->flags & OS_CREATE_WINDOW_HEADLESS) return OS_CALLBACK_REJECTED;

		OSRectangle bounds, bounds2;
		OSSyscall(OS_SYSCALL_GET_WINDOW_BOUNDS, window->window, (uintptr_t) &bounds, 0, 0);

		bounds2 = bounds;

		int oldWidth = bounds.right - bounds.left;
		int oldHeight = bounds.bottom - bounds.top;

		if (control->direction & RESIZE_LEFT) bounds.left = message->mouseMoved.newPositionXScreen;
		if (control->direction & RESIZE_RIGHT) bounds.right = message->mouseMoved.newPositionXScreen;
		if (control->direction & RESIZE_TOP) bounds.top = message->mouseMoved.newPositionYScreen;
		if (control->direction & RESIZE_BOTTOM) bounds.bottom = message->mouseMoved.newPositionYScreen;

		int newWidth = bounds.right - bounds.left;
		int newHeight = bounds.bottom - bounds.top;

		if (newWidth < window->minimumWidth && control->direction & RESIZE_LEFT) bounds.left = bounds.right - window->minimumWidth;
		if (newWidth < window->minimumWidth && control->direction & RESIZE_RIGHT) bounds.right = bounds.left + window->minimumWidth;
		if (newHeight < window->minimumHeight && control->direction & RESIZE_TOP) bounds.top = bounds.bottom - window->minimumHeight;
		if (newHeight < window->minimumHeight && control->direction & RESIZE_BOTTOM) bounds.bottom = bounds.top + window->minimumHeight;

		bool relayout = true;

		if (control->direction == RESIZE_MOVE) {
			bounds.left = message->mouseDragged.newPositionXScreen - message->mouseDragged.originalPositionX;
			bounds.top = message->mouseDragged.newPositionYScreen - message->mouseDragged.originalPositionY;
			bounds.right = bounds.left + oldWidth;
			bounds.bottom = bounds.top + oldHeight;
			relayout = false;
		}
		
		OSSyscall(OS_SYSCALL_MOVE_WINDOW, window->window, (uintptr_t) &bounds, 0, 0);

		if (relayout) {
			window->width = bounds.right - bounds.left;
			window->height = bounds.bottom - bounds.top;
		
			OSMessage layout;
			layout.type = OS_MESSAGE_LAYOUT;
			layout.layout.left = 0;
			layout.layout.top = 0;
			layout.layout.right = window->width;
			layout.layout.bottom = window->height;
			layout.layout.force = true;
			layout.layout.clip = OS_MAKE_RECTANGLE(0, window->width, 0, window->height);
			OSSendMessage(window->root, &layout);
		}
	} else if (message->type == OS_MESSAGE_START_DRAG) {
		draggingWindowResizeHandle = true;
	} else if (message->type == OS_MESSAGE_END_PRESS) {
		draggingWindowResizeHandle = false;
		lastIdleTimeStamp = OSProcessorReadTimeStamp();
	} else if (message->type == OS_MESSAGE_LAYOUT_TEXT) {
		control->textBounds = control->bounds;
		control->textBounds.top -= 3;
		control->textBounds.bottom -= 3;
	} else {
		response = OSForwardMessage(_object, OS_MAKE_MESSAGE_CALLBACK(ProcessControlMessage, nullptr), message);
	}

	return response;
}

static OSObject CreateWindowResizeHandle(UIImage **images, unsigned direction) {
	WindowResizeControl *control = (WindowResizeControl *) GUIAllocate(sizeof(WindowResizeControl), true);
	control->type = API_OBJECT_CONTROL;
	control->backgrounds = images;
	control->preferredWidth = images[0]->region.right - images[0]->region.left;
	control->preferredHeight = images[0]->region.bottom - images[0]->region.top;
	control->direction = direction;
	control->noAnimations = true;
	control->noDisabledTextColorChange = true;
	control->keepCustomCursorWhenDisabled = true;
	OSSetMessageCallback(control, OS_MAKE_MESSAGE_CALLBACK(ProcessWindowResizeHandleMessage, nullptr));

	switch (direction) {
		case RESIZE_LEFT:
		case RESIZE_RIGHT:
			control->cursor = OS_CURSOR_RESIZE_HORIZONTAL;
			break;

		case RESIZE_TOP:
		case RESIZE_BOTTOM:
			control->cursor = OS_CURSOR_RESIZE_VERTICAL;
			break;

		case RESIZE_TOP_RIGHT:
		case RESIZE_BOTTOM_LEFT:
			control->cursor = OS_CURSOR_RESIZE_DIAGONAL_1;
			break;

		case RESIZE_TOP_LEFT:
		case RESIZE_BOTTOM_RIGHT:
			control->cursor = OS_CURSOR_RESIZE_DIAGONAL_2;
			break;

		case RESIZE_MOVE: {
			control->textColor = TEXT_COLOR_TITLEBAR;
			control->textShadow = true;
			control->textBold = true;
			control->textSize = 10;
			control->textShadowBlur = true;
		} break;
	}

	return control;
}

#include "textbox.cpp"

static void IssueCommand(Control *control, OSCommand *command = nullptr, OSInstance *instance = nullptr) {
	if (!instance) instance = control->window->instance;
	if (!command) command = control->command;

	if (control ? (control->disabled) : (command->disabled)) {
		return;
	}

	bool checkable = control ? control->checkable : command->specification->checkable;
	bool radioCheck = control ? control->radioCheck : command->specification->radioCheck;
	bool isChecked = control ? control->isChecked : command->checked;

	if (radioCheck) {
		isChecked = true;

		if (command) {
			OSCheckCommand(command, isChecked);
		}
	} else if (checkable) {
		// Update the checked state.
		isChecked = !isChecked;

		if (command) {
			// Update the command.
			OSCheckCommand(command, isChecked);
		} else {
			control->isChecked = isChecked;
		}
	}

	OSNotification n = {};
	n.type = OS_NOTIFICATION_COMMAND;
	n.command.checked = isChecked;
	n.command.command = command;

	if (control) {
		OSSendNotification(control, control->notificationCallback, &n, instance);
	} else {
		OSSendNotification(nullptr, command->notificationCallback, &n, instance);
	}

#if 0
	if (window->flags & OS_CREATE_WINDOW_MENU) {
		OSMessage m;
		m.type = OS_MESSAGE_DESTROY;
		OSSendMessage(openMenus[0].window, &m);
	}
#endif
}

#if 0
void IssueCommand(OSMessage *message) {
	OSInstance *instance = (OSInstance *) message->context;
	char *data = (char *) OSHeapAllocate(message->issueCommand.nameBytes, false);
	OSReadConstantBuffer(message->issueCommand.nameBuffer, data);

	{
		size_t nameBytes = message->issueCommand.nameBytes;

		for (uintptr_t i = 0; i < _commandCount; i++) {
			if (0 == OSCompareStrings(_commands[i]->name, data, _commands[i]->nameBytes, nameBytes)) {
				OSIssueCommand(instance, _commands[i]);
				break;
			}
		}
	}

	OSHeapFree(data);
	OSCloseHandle(message->issueCommand.nameBuffer);
}
#endif

void OSIssueCommand(OSObject instance, OSCommand *command) {
	IssueCommand(nullptr, command, (OSInstance *) instance);
}

OSObject OSGetWindow(OSObject object) {
	return ((GUIObject *) object)->window;
}

OSCallbackResponse ProcessButtonMessage(OSObject object, OSMessage *message) {
	Control *control = (Control *) object;
	OSCallbackResponse result = OS_CALLBACK_NOT_HANDLED;
	
	if (message->type == OS_MESSAGE_CLICKED) {
		IssueCommand(control);

		if (control->window->flags & OS_CREATE_WINDOW_MENU) {
			OSMessage m;
			m.type = OS_MESSAGE_DESTROY;
			OSSendMessage(openMenus[0].window, &m);
		}
	} else if (message->type == OS_MESSAGE_START_FOCUS) {
		if (!control->checkable && (control->window->flags & OS_CREATE_WINDOW_DIALOG)) {
			if (control->window->buttonFocus) OSRepaintControl(control->window->buttonFocus);
			control->window->buttonFocus = control;
			control->backgrounds = (control->command && control->command->specification->dangerous) ? buttonDangerousDefaultBackgrounds : buttonDefaultBackgrounds;
		}

#if 0
		if (control->radioCheck) {
			IssueCommand(control);
		}
#endif
	} else if (message->type == OS_MESSAGE_KEY_PRESSED) {
		if (message->keyboard.scancode == OS_SCANCODE_SPACE) {
			control->window->pressed = control;
			control->pressedByKeyboard = true;
			OSAnimateControl(control, true);
			result = OS_CALLBACK_HANDLED;
		}
	} else if (message->type == OS_MESSAGE_KEY_RELEASED) {
		if (message->keyboard.scancode == OS_SCANCODE_SPACE) {
			control->window->pressed = nullptr;
			control->pressedByKeyboard = false;
			OSAnimateControl(control, false);
			result = OS_CALLBACK_HANDLED;
			IssueCommand(control);
		}
	} 

	if (result == OS_CALLBACK_NOT_HANDLED) {
		result = OSForwardMessage(object, OS_MAKE_MESSAGE_CALLBACK(ProcessControlMessage, nullptr), message);
	}

	return result;
}

OSObject OSCreateBlankControl(int width, int height, OSCursorStyle cursor, unsigned flags) {
	Control *control = (Control *) GUIAllocate(sizeof(Control), true);
	control->type = API_OBJECT_CONTROL;
	control->preferredWidth = width;
	control->preferredHeight = height;
	control->drawParentBackground = flags & OS_BLANK_CONTROL_DRAW_PARENT_BACKGROUND;
	control->ignoreActivationClicks = flags & OS_BLANK_CONTROL_IGNORE_ACTIVATION_CLICKS;
	control->focusable = flags & OS_BLANK_CONTROL_FOCUSABLE;
	control->tabStop = flags & OS_BLANK_CONTROL_TAB_STOP;
	control->noAnimations = true;
	control->customTextRendering = true;
	control->cursor = cursor;
	OSSetMessageCallback(control, OS_MAKE_MESSAGE_CALLBACK(ProcessControlMessage, nullptr));
	return control;
}

void OSRemoveGUIObjectFromParent(OSObject guiObject) {
	GUIObject *object = (GUIObject *) guiObject;
	OSMessage m;

	if (object->parent) {
		m.type = OS_MESSAGE_REMOVE_CHILD;
		m.removeChild.child = object;
		OSSendMessage(object->parent, &m);
		object->parent = nullptr;
	}
}

void OSDestroyGUIObject(OSObject guiObject) {
	GUIObject *object = (GUIObject *) guiObject;
	OSMessage m;
	OSRemoveGUIObjectFromParent(object);
	m.type = OS_MESSAGE_DESTROY;
	m.context = object;
	OSPostMessage(&m);
}

static int RoundUpToNearestInteger(float x) {
	int truncate = (int) x;
	return truncate + 1;
}

#define GET_TAB_BAND_FROM_TAB_PANE(tabPane) ((TabBand *) (((Grid *) tabPane)->objects[0]))
#define SEND_TAB_PANE_NOTIFICATION(tabPane, n) OSSendNotification(tabPane, ((Grid *) tabPane)->notificationCallback, n, OSGetInstance(tabPane))

OSCallbackResponse ProcessTabBandMessage(OSObject object, OSMessage *message) {
	TabBand *control = (TabBand *) object;
	OSCallbackResponse result = OS_CALLBACK_NOT_HANDLED;

	if (message->type == OS_MESSAGE_CUSTOM_PAINT) {
		OSRectangle bounds = control->bounds;

		int position = 0;

		for (intptr_t i = 0; i < (intptr_t) control->tabCount; i++) {
			Tab *tab = control->tabs + i;

			if (control->tabs[i].newTabButton) position += 2;
			bounds.left = control->bounds.left + position;
			bounds.right = bounds.left + tab->width;
			position += control->tabs[i].width - 1;

			if (control->activeTab == i) {
				DrawUIImage(message, bounds, &tabActiveNormal, 0xFF);
			} else if (tab->newTabButton && control->newTabPressed && control->hoverTab == i) {
				if (control->hoverTab == i) {
					DrawUIImage(message, bounds, &tabPressed, 0xFF);
				} else {
					DrawUIImage(message, bounds, &tabOtherHover, 0xFF);
				}
			} else {
				DrawUIImage(message, bounds, &tabOtherNormal, 0xFF);
				DrawUIImage(message, bounds, &tabOtherHover, 0xFF * tab->hoverAnimation / 16);
			}

			{
				OSRectangle textBounds = bounds;
				textBounds.left += control->flags & OS_CREATE_TAB_PANE_LARGE ? 12 : 6;
				textBounds.right -= control->flags & OS_CREATE_TAB_PANE_LARGE ? 12 : 6;
				if (i != control->activeTab) textBounds.top += 2;

				OSDrawString(message->paint.surface, textBounds, &tab->text, 0, 
						OS_DRAW_STRING_VALIGN_CENTER 
						| (control->flags & OS_CREATE_TAB_PANE_LARGE ? OS_DRAW_STRING_HALIGN_LEFT : OS_DRAW_STRING_HALIGN_CENTER), 
						TEXT_COLOR_DEFAULT, -1, OS_STANDARD_FONT_REGULAR, message->paint.clip, 0);
			}

			if (tab->newTabButton) {
				bounds.left += 7;
				bounds.top += 9;
				bounds.right -= 7;
				bounds.bottom -= 7;

				DrawUIImage(message, bounds, &tabNew, 0xFF);
			}
		}

		result = OS_CALLBACK_HANDLED;
	} else if (message->type == OS_MESSAGE_END_HOVER) {
		ReceiveTimerMessages(control);
		control->hoverTab = -1;
	} else if (message->type == OS_MESSAGE_END_PRESS) {
	} else if (message->type == OS_MESSAGE_MOUSE_MOVED || message->type == OS_MESSAGE_MOUSE_DRAGGED) {
		OSRectangle bounds = control->bounds;
		bool found = false;

		int position = 0;

		for (intptr_t i = 0; i < (intptr_t) control->tabCount; i++) {
			Tab *tab = control->tabs + i;

			if (control->tabs[i].newTabButton) position += 2;
			bounds.left = control->bounds.left + position;
			bounds.right = bounds.left + tab->width;
			position += control->tabs[i].width - 1;

			if (IsPointInRectangle(bounds, message->mouseMoved.newPositionX, message->mouseMoved.newPositionY)) {
				if (control->hoverTab != i) {
					ReceiveTimerMessages(control);
				}
					
				control->hoverTab = i;
				found = true;
				break;
			}
		}

		if (!found && control->hoverTab != -1) {
			ReceiveTimerMessages(control);
			control->hoverTab = -1;
		}

		RepaintControl(control);
	} else if (message->type == OS_MESSAGE_WM_TIMER) {
		bool stillNeeded = false;
		bool preventSnap = false;

		for (uintptr_t i = 0; i < control->tabCount; i++) {
			Tab *tab = control->tabs + i;
			int animationTarget = ((int) i == control->hoverTab) ? 16 : 0;

			if (animationTarget != tab->hoverAnimation) {
				stillNeeded = true;
				if (tab->hoverAnimation < animationTarget) tab->hoverAnimation++;
				else tab->hoverAnimation--;
			}

			if (tab->widthTarget != tab->width) {
				stillNeeded = true;
				int move = (float) (tab->widthTarget - tab->width) / 4;
				tab->width += move;

				if (!move && !preventSnap) {
					tab->width = tab->widthTarget;
					preventSnap = true;
				}
			}
		}

		if (!stillNeeded && control->timerControlItem.list) {
			control->window->timerControls.Remove(&control->timerControlItem);
		}

		RepaintControl(control);

		result = OS_CALLBACK_HANDLED;
	} else if (message->type == OS_MESSAGE_MOUSE_LEFT_PRESSED) {
		if (control->hoverTab != -1) {
			OSSetActiveTab(control->parent, control->hoverTab, false, true);
		}
	} else if (message->type == OS_MESSAGE_MOUSE_LEFT_RELEASED) {
		RepaintControl(control);

		if (control->hoverTab != -1 && control->tabs[control->hoverTab].newTabButton && control->newTabPressed) {
			OSNotification n;
			n.type = OS_NOTIFICATION_NEW_TAB;
			SEND_TAB_PANE_NOTIFICATION(control->parent, &n);
		}

		control->newTabPressed = false;
	} else if (message->type == OS_MESSAGE_DESTROY) {
		for (uintptr_t i = 0; i < control->tabCount; i++) {
			GUIFree(control->tabs[i].text.buffer);
		}

		GUIFree(control->tabs);
	} 
	
	if (result == OS_CALLBACK_NOT_HANDLED) {
		result = OSForwardMessage(object, OS_MAKE_MESSAGE_CALLBACK(ProcessControlMessage, nullptr), message);
	}

	if (message->type == OS_MESSAGE_LAYOUT && (control->flags & OS_CREATE_TAB_PANE_LARGE)) {
		int totalWidth = 0;

		for (uintptr_t i = 0; i < control->tabCount; i++) {
			int width;

			if (!control->tabs[i].newTabButton) {
				width = TAB_LARGE_WIDTH;
			} else {
				width = TAB_WIDTH;
			}

			control->tabs[i].widthTarget = width;
			totalWidth += width;
		}

		if (totalWidth > control->bounds.right - control->bounds.left) {
			// The tabs don't fit in the band.
			// Evenly spread out their width.

			int widthAvailable = control->bounds.right - control->bounds.left;
			int spreadCount = 0;

			for (uintptr_t i = 0; i < control->tabCount; i++) {
				if (control->tabs[i].newTabButton) {
					widthAvailable -= control->tabs[i].widthTarget + 5;
				} else {
					spreadCount++;
				}
			}

			int widthPerTab = widthAvailable / spreadCount;
			int remaining = widthAvailable % spreadCount;

			for (uintptr_t i = 0; i < control->tabCount; i++) {
				if (!control->tabs[i].newTabButton) {
					int additional = remaining ? 2 : 1;
					control->tabs[i].widthTarget = widthPerTab + additional;
					if (remaining) remaining--;
				}
			}
		}

		RepaintControl(control);
		ReceiveTimerMessages(control);
	}

	return result;
}

static OSObject CreateTabBand(unsigned flags) {
	TabBand *control = (TabBand *) GUIAllocate(sizeof(TabBand), true);

	control->type = API_OBJECT_CONTROL;
	control->preferredHeight = TAB_BAND_HEIGHT;
	control->drawParentBackground = true;
	control->ignoreActivationClicks = false;
	control->focusable = true;
	control->tabStop = true;
	control->noAnimations = true;
	control->customTextRendering = true;
	control->cursor = OS_CURSOR_NORMAL;
	control->backgrounds = tabBandBackgrounds;
	control->flags = flags;
	control->animationStep = 0;
	control->finalAnimationStep = 16;

	OSSetMessageCallback(control, OS_MAKE_MESSAGE_CALLBACK(ProcessTabBandMessage, nullptr));

	return control;
}

void OSInsertTab(OSObject tabPane, bool end, const char *text, size_t textBytes) {
	TabBand *band = GET_TAB_BAND_FROM_TAB_PANE(tabPane);
	OSRepaintControl(tabPane);

	int before = end ? band->tabCount - (band->flags & OS_CREATE_TAB_PANE_NEW_BUTTON ? 1 : 0) : 0;

	Tab *newTabs = (Tab *) GUIAllocate((band->tabCount + 1) * sizeof(Tab), false);
	OSCopyMemory(newTabs, band->tabs, band->tabCount * sizeof(Tab));
	GUIFree(band->tabs);
	band->tabs = newTabs;
	OSMoveMemory(newTabs + before, newTabs + band->tabCount, sizeof(Tab), true);
	band->tabCount++;

	Tab *newTab = newTabs + before;
	CreateString(text, textBytes, &newTab->text);

	if (band->flags & OS_CREATE_TAB_PANE_LARGE) {
		newTab->widthTarget = TAB_LARGE_WIDTH;

		OSMessage m;
		m.type = OS_MESSAGE_CHILD_UPDATED;
		OSSendMessage(tabPane, &m);
		band->relayout = true;
	} else {
		newTab->widthTarget = MeasureStringWidth(text, textBytes, FONT_SIZE, fontRegular) + 24;

		if (newTab->widthTarget < TAB_WIDTH) {
			newTab->widthTarget = TAB_WIDTH;
		}
	}

	if (!(band->flags & OS_CREATE_TAB_PANE_ANIMATIONS)) {
		newTab->width = newTab->widthTarget;
	} else {
		ReceiveTimerMessages(band);
	}
}

void OSRemoveTab(OSObject tabPane, int index) {
	TabBand *band = GET_TAB_BAND_FROM_TAB_PANE(tabPane);
	OSRepaintControl(tabPane);
	(void) index;
	(void) band;
	// TODO.
}

void OSSetActiveTab(OSObject tabPane, int index, bool relative, bool sendNotification) {
	TabBand *band = GET_TAB_BAND_FROM_TAB_PANE(tabPane);

	if (band->tabs[index].newTabButton) {
		band->newTabPressed = true;
		return;
	}

	// TODO Skip new tab button and removed tabs.

	band->activeTab = (relative ? band->activeTab : 0) + index;
	OSRepaintControl(tabPane);

	if (sendNotification) {
		OSNotification n;
		n.type = OS_NOTIFICATION_ACTIVE_TAB_CHANGED;
		n.activeTabChanged.newIndex = band->activeTab;
		SEND_TAB_PANE_NOTIFICATION(tabPane, &n);
	}
}

void OSChangeTabText(OSObject tabPane, int index, const char *text, size_t textBytes) {
	TabBand *band = GET_TAB_BAND_FROM_TAB_PANE(tabPane);
	OSRepaintControl(band);
	CreateString(text, textBytes, &band->tabs[index].text);
}

OSObject OSCreateTabPane(unsigned flags) {
	OSObject grid = OSCreateGrid(1, 2, OS_GRID_STYLE_LAYOUT);
	OSObject tabBand = CreateTabBand(flags);
	OSAddControl(grid, 0, 0, tabBand, OS_CELL_H_FILL);

	if (flags & OS_CREATE_TAB_PANE_NEW_BUTTON) {
		OSInsertTab(grid, 0, OSLiteral(""));

		Tab *tab = ((TabBand *) tabBand)->tabs;
		tab->newTabButton = true;
		tab->widthTarget = tab->width = TAB_WIDTH;
	}

	return grid;
}

void OSSetTabPaneContent(OSObject tabPane, OSObject content) {
	Grid *grid = (Grid *) tabPane;
	if (grid->objects[1]) OSRemoveGUIObjectFromParent(grid->objects[1]);
	OSAddControl(grid, 0, 1, content, OS_CELL_FILL);
}

OSObject OSCreateButton(OSCommand *command, OSButtonStyle style) {
	Control *control = (Control *) GUIAllocate(sizeof(Control), true);
	control->type = API_OBJECT_CONTROL;
	control->tabStop = true;

	control->preferredWidth = 80;
	control->preferredHeight = 21;
	control->textColor = TEXT_COLOR_DEFAULT;

	control->drawParentBackground = true;
	control->ignoreActivationClicks = false;

	OSSetControlCommand(control, command);

	if (style == OS_BUTTON_STYLE_TOOLBAR) {
		control->textColor = TEXT_COLOR_TOOLBAR;
		control->horizontalMargin = 12;
		control->preferredWidth = 0;
		control->preferredHeight = 31;
		control->textShadowBlur = true;
		control->textShadow = true;
		control->backgrounds = toolbarItemBackgrounds;
		control->additionalCheckedBackgrounds = true;
	} else if (style == OS_BUTTON_STYLE_TOOLBAR_ICON_ONLY) {
		control->horizontalMargin = 6;
		control->preferredWidth = 32;
		control->preferredHeight = 31;
		control->centerIcons = true;
		control->backgrounds = toolbarItemBackgrounds;
		control->additionalCheckedBackgrounds = true;
	} else {
		control->focusable = true;
		control->hasFocusedBackground = true;

		if (control->checkable) {
			control->textAlign = OS_DRAW_STRING_VALIGN_CENTER | OS_DRAW_STRING_HALIGN_LEFT;
			control->icon = control->radioCheck ? &radioboxHover : &checkboxHover;
			control->iconHasVariants = true;
			control->iconHasFocusVariant = true;
			control->additionalCheckedIcons = true;
			control->preferredHeight = 18;
		} else {
			control->backgrounds = command->specification->dangerous ? buttonDangerousBackgrounds : buttonBackgrounds;
		}

		if (style == OS_BUTTON_STYLE_REPEAT) {
			control->useClickRepeat = true;
		}
	}

	OSSetMessageCallback(control, OS_MAKE_MESSAGE_CALLBACK(ProcessButtonMessage, nullptr));

	if (style != OS_BUTTON_STYLE_TOOLBAR_ICON_ONLY) {
		OSSetText(control, command->specification->label, command->specification->labelBytes, OS_RESIZE_MODE_GROW_ONLY | OS_RESIZE_MODE_ADDITIONAL_WIDTH_PADDING);
	} 

	return control;
}

static int FindObjectInGrid(Grid *grid, OSObject object) {
	if (grid->type != API_OBJECT_GRID) {
		OSCrashProcess(OS_FATAL_ERROR_INVALID_PANE_OBJECT);
	}

	for (uintptr_t i = 0; i < grid->columns * grid->rows; i++) {
		if (grid->objects[i] == object && object) {
			return i;
		}
	}

	return grid->columns * grid->rows;
}

OSCallbackResponse ProcessMenuItemMessage(OSObject object, OSMessage *message) {
	MenuItem *control = (MenuItem *) object;
	OSCallbackResponse result = OS_CALLBACK_NOT_HANDLED;

	if (message->type == OS_MESSAGE_LAYOUT_TEXT) {
		if (!control->menubar) {
			control->textBounds = control->bounds;
			// Leave room for the icons.
			control->textBounds.left += 24;
			control->textBounds.right -= 4;
			result = OS_CALLBACK_HANDLED;
		}
	} else if (message->type == OS_MESSAGE_END_HOVER) {
		if (navigateMenuItem == control) {
			navigateMenuItem = nullptr;
		}
	} else if (message->type == OS_MESSAGE_CLICKED 
			|| message->type == OS_MESSAGE_START_HOVER || message->type == OS_MESSAGE_START_FOCUS) {
		if (control->window->hover != control) {
			OSMessage m;
			m.type = OS_MESSAGE_END_HOVER;
			OSSendMessage(control->window->hover, &m);
			control->window->hover = control;
		}

		if (navigateMenuItem) {
			OSAnimateControl(navigateMenuItem, false);
		}

		bool openMenu = control->item.type == OSMenuItem_SUBMENU;

		if (message->type == OS_MESSAGE_START_HOVER && !openMenuCount) {
			openMenu = false;
		} else {
			navigateMenuMode = true;
			navigateMenuItem = control;
		}

		if (openMenuCount && openMenus[openMenuCount - 1].source == control) {
			openMenu = false;
		}

		if (message->type == OS_MESSAGE_START_FOCUS && !control->menubar) {
			openMenu = false;
		}

		if (openMenu) {
			OSCreateMenu((OSMenuTemplate *) control->item.value, control, OS_CREATE_MENU_AT_SOURCE, 
					control->menubar ? OS_CREATE_MENU_FROM_MENUBAR : OS_CREATE_SUBMENU, control->window->instance);

			OSAnimateControl(control, true);

			if (message->type == OS_MESSAGE_CLICKED) {
				result = OS_CALLBACK_HANDLED;
			}
		}
	} else if (message->type == OS_MESSAGE_CUSTOM_PAINT && !control->menubar) {
		int xOffset = 4, yOffset = 2;
		OSRectangle iconRegion = OS_MAKE_RECTANGLE(control->bounds.left + xOffset, control->bounds.left + xOffset + 16, control->bounds.top + yOffset, control->bounds.top + yOffset + 16);
		OSRectangle clip;
		ClipRectangle(iconRegion, message->paint.clip, &clip);

		if (control->item.type == OSMenuItem_SUBMENU) {
			DrawUIImage(message, iconRegion, &menuIconSub, 0xFF, &clip);
		} else if (control->isChecked && control->radioCheck) {
			DrawUIImage(message, iconRegion, &menuIconRadio, 0xFF, &clip);
		} else if (control->isChecked) {
			DrawUIImage(message, iconRegion, &menuIconCheck, 0xFF, &clip);
		}
	}

	if (result == OS_CALLBACK_NOT_HANDLED) {
		result = OSForwardMessage(object, OS_MAKE_MESSAGE_CALLBACK(ProcessButtonMessage, nullptr), message);
	}

	return result;
}

static OSObject CreateMenuItem(OSMenuItem item, bool menubar, OSInstance *instance) {
	MenuItem *control = (MenuItem *) GUIAllocate(sizeof(MenuItem), true);
	control->type = API_OBJECT_CONTROL;

	control->preferredWidth = !menubar ? 70 : 21;
	control->preferredHeight = 21;
	control->drawParentBackground = true;
	control->textAlign = menubar ? OS_FLAGS_DEFAULT : (OS_DRAW_STRING_VALIGN_CENTER | OS_DRAW_STRING_HALIGN_LEFT);
	control->backgrounds = menuItemBackgrounds;
	control->ignoreActivationClicks = menubar;
	control->noAnimations = true;
	control->hasFocusedBackground = true;
	control->tabStop = true;

	control->item = item;
	control->menubar = menubar;
	// control->horizontalMargin = menubar ? 0 : 4;

	if (item.type == OSMenuItem_COMMAND) {
		uintptr_t index = (uintptr_t) item.value;
		OSCommand *commands = instance->customCommands;

		if (index & (1 << 31)) {
			commands = instance->builtinCommands;
			index ^= (1 << 31);
		}

		OSCommand *command = index + commands;
		OSSetControlCommand(control, command);
		OSSetText(control, command->specification->label, command->specification->labelBytes, OS_RESIZE_MODE_GROW_ONLY);
	} else if (item.type == OSMenuItem_SUBMENU) {
		OSMenuTemplate *menu = (OSMenuTemplate *) item.value;
		OSSetText(control, menu->name, menu->nameBytes, OS_RESIZE_MODE_GROW_ONLY);
	}

	if (menubar) {
		control->preferredWidth += 4;
	} else {
		control->preferredWidth += 32;
	}

	OSSetMessageCallback(control, OS_MAKE_MESSAGE_CALLBACK(ProcessMenuItemMessage, nullptr));

	return control;
}

OSCallbackResponse ProcessMenuSeparatorMessage(OSObject object, OSMessage *message) {
	Control *control = (Control *) object;
	OSCallbackResponse response = OS_CALLBACK_NOT_HANDLED;

	if (message->type == OS_MESSAGE_CUSTOM_PAINT) {
		OSDrawSurface(message->paint.surface, OS_SURFACE_UI_SHEET, 
				OS_MAKE_RECTANGLE(control->bounds.left, control->bounds.right + 1, control->bounds.top + 1, control->bounds.bottom - 1),
				lineHorizontal.region, lineHorizontal.border, lineHorizontal.drawMode, 0xFF);
		response = OS_CALLBACK_HANDLED;
	}

	if (response == OS_CALLBACK_NOT_HANDLED) {
		response = OSForwardMessage(object, OS_MAKE_MESSAGE_CALLBACK(ProcessControlMessage, nullptr), message);
	}

	return response;
}

OSObject CreateMenuSeparator() {
	Control *control = (Control *) GUIAllocate(sizeof(Control), true);
	control->type = API_OBJECT_CONTROL;
	control->drawParentBackground = true;
	control->preferredWidth = 1;
	control->preferredHeight = 3;
	OSSetMessageCallback(control, OS_MAKE_MESSAGE_CALLBACK(ProcessMenuSeparatorMessage, nullptr));
	return control;
}

OSObject OSCreateLine(bool orientation) {
	Control *control = (Control *) GUIAllocate(sizeof(Control), true);
	control->type = API_OBJECT_CONTROL;
	control->backgrounds = orientation ? lineVerticalBackgrounds : lineHorizontalBackgrounds;
	control->drawParentBackground = true;

	control->preferredWidth = 1;
	control->preferredHeight = 1;

	OSSetMessageCallback(control, OS_MAKE_MESSAGE_CALLBACK(ProcessControlMessage, nullptr));

	return control;
}

static OSCallbackResponse ProcessIconDisplayMessage(OSObject _object, OSMessage *message) {
	uint16_t iconID = (uintptr_t) message->context;
	Control *control = (Control *) _object;

	if (message->type == OS_MESSAGE_CUSTOM_PAINT) {
		OSDrawSurfaceClipped(message->paint.surface, OS_SURFACE_UI_SHEET, control->bounds, 
				icons32[iconID].region, icons32[iconID].border,
				OS_DRAW_MODE_REPEAT_FIRST, 0xFF, message->paint.clip);
		return OS_CALLBACK_HANDLED;
	} else {
		return OSForwardMessage(_object, OS_MAKE_MESSAGE_CALLBACK(ProcessControlMessage, nullptr), message);
	}
}

OSObject OSCreateIconDisplay(uint16_t iconID) {
	Control *control = (Control *) GUIAllocate(sizeof(Control), true);
	control->type = API_OBJECT_CONTROL;
	control->drawParentBackground = true;
	control->preferredWidth = 32;
	control->preferredHeight = 32;

	OSSetMessageCallback(control, OS_MAKE_MESSAGE_CALLBACK(ProcessIconDisplayMessage, (void *) (uintptr_t) iconID));
	return control;
}


static OSCallbackResponse ProcessLabelMessage(OSObject _object, OSMessage *message) {
	Control *control = (Control *) _object;
	OSCallbackResponse response = OS_CALLBACK_NOT_HANDLED;

	if (message->type == OS_MESSAGE_MEASURE && (control->textAlign & OS_DRAW_STRING_WORD_WRAP)) {
		message->measure.preferredWidth = message->measure.parentWidth;
		message->measure.preferredHeight = MeasureString(control->text.buffer, control->text.bytes, control->textSize ? control->textSize : FONT_SIZE, fontRegular, message->measure.parentWidth);
		response = OS_CALLBACK_HANDLED;
		// OSPrint("Measure label: %d by %d (given width of %d)\n", message->measure.preferredWidth, message->measure.preferredHeight, message->measure.parentWidth);
	}

#if 0
	if (message->type == OS_MESSAGE_PAINT) {
		OSRectangle r;
		ClipRectangle(control->bounds, message->paint.clip, &r);
		OSFillRectangle(message->paint.surface, r, OSColor(255, 0, 255));
	}
#endif
	
	if (response == OS_CALLBACK_NOT_HANDLED) {
		return OSForwardMessage(_object, OS_MAKE_MESSAGE_CALLBACK(ProcessControlMessage, nullptr), message);
	}

	return response;
}

OSObject OSCreateLabel(char *text, size_t textBytes, bool wordWrap, bool useAdditionalWidthPadding) {
	Control *control = (Control *) GUIAllocate(sizeof(Control), true);
	control->type = API_OBJECT_CONTROL;
	control->drawParentBackground = true;
	control->textAlign = OS_DRAW_STRING_HALIGN_LEFT | (wordWrap ? (OS_DRAW_STRING_WORD_WRAP | OS_DRAW_STRING_VALIGN_TOP) : OS_DRAW_STRING_VALIGN_CENTER);

	OSSetText(control, text, textBytes, (!useAdditionalWidthPadding ? OS_RESIZE_MODE_NO_WIDTH_PADDING : 0) | OS_RESIZE_MODE_EXACT);
	OSSetMessageCallback(control, OS_MAKE_MESSAGE_CALLBACK(ProcessLabelMessage, nullptr));

	return control;
}

OSRectangle OSGetControlBounds(OSObject _control) {
	Control *control = (Control *) _control;
	return control->bounds;
}

void OSDrawProgressBar(OSHandle surface, OSRectangle bounds, float progress, OSRectangle clip, bool blue) {
	DrawUIImage(nullptr, bounds, &progressBarBackground, 0xFF, &clip, surface);

	if (progress > 0.99) progress = 1;

	OSRectangle filled = OS_MAKE_RECTANGLE(
			bounds.left + 1, bounds.left + 1 + (int) (progress * (bounds.right - bounds.left - 2)),
			bounds.top + 1, bounds.bottom - 1);

	UIImage image = blue ? progressBarFilledBlue : progressBarFilled;

	DrawUIImage(nullptr, filled, &image, 0xFF, &clip, surface);
}

static OSCallbackResponse ProcessProgressBarMessage(OSObject _object, OSMessage *message) {
	OSCallbackResponse response = OS_CALLBACK_NOT_HANDLED;
	ProgressBar *control = (ProgressBar *) _object;

	if (message->type == OS_MESSAGE_PAINT) {
		response = OS_CALLBACK_HANDLED;

		if (control->repaint || message->paint.force) {
			{
				OSMessage m = *message;
				m.type = OS_MESSAGE_PAINT_BACKGROUND;
				m.paintBackground.surface = message->paint.surface;
				ClipRectangle(message->paint.clip, control->bounds, &m.paintBackground.clip);
				OSSendMessage(control->parent, &m);
			}

			if (control->disabled) {
				DrawUIImage(message, control->bounds, &progressBarDisabled, 0xFF);
			} else {
				DrawUIImage(message, control->bounds, &progressBarBackground, 0xFF);

				if (control->maximum) {
					float progress = (float) (control->value - control->minimum) / (float) (control->maximum - control->minimum);
					if (progress > 0.99) progress = 1;
					OSRectangle filled = OS_MAKE_RECTANGLE(
							control->bounds.left + 1, control->bounds.left + 1 + (int) (progress * (control->bounds.right - control->bounds.left - 2)),
							control->bounds.top + 1, control->bounds.bottom - 1);

					DrawUIImage(message, filled, &progressBarFilled, 0xFF);
				} else {
					OSRectangle clip;
					ClipRectangle(message->paint.clip, OS_MAKE_RECTANGLE(control->bounds.left + 1, control->bounds.right - 1, control->bounds.top + 1, control->bounds.bottom - 1), &clip);
					if (control->value >= control->bounds.right - control->bounds.left) control->value = -100;
					OSRectangle marquee = OS_MAKE_RECTANGLE(
							control->bounds.left + 1 + control->value, control->bounds.left + 101 + control->value,
							control->bounds.top + 1, control->bounds.bottom - 1);
					DrawUIImage(message, marquee, &progressBarMarquee, 0xFF, &clip);
				}
			}

			control->repaint = false;
			control->repaintCustomOnly = true;
		}
	} else if (message->type == OS_MESSAGE_PARENT_UPDATED) {
		if (!control->maximum) {
			OSForwardMessage(_object, OS_MAKE_MESSAGE_CALLBACK(ProcessControlMessage, nullptr), message);
			control->timerHz = 30;
			control->window->timerControls.InsertStart(&control->timerControlItem);
		}
	} else if (message->type == OS_MESSAGE_WM_TIMER) {
		response = OS_CALLBACK_HANDLED;
		OSSetProgressBarValue(control, control->value + 2);
	}

	if (response == OS_CALLBACK_NOT_HANDLED) {
		response = OSForwardMessage(_object, OS_MAKE_MESSAGE_CALLBACK(ProcessControlMessage, nullptr), message);
	}

	return response;
}

void OSSetProgressBarValue(OSObject _control, int newValue) {
	ProgressBar *control = (ProgressBar *) _control;
	control->value = newValue;
	RepaintControl(control);
}

OSObject OSCreateProgressBar(int minimum, int maximum, int initialValue, bool small) {
	ProgressBar *control = (ProgressBar *) GUIAllocate(sizeof(ProgressBar), true);

	control->type = API_OBJECT_CONTROL;

	control->minimum = minimum;
	control->maximum = maximum;
	control->value = initialValue;

	control->preferredWidth = 168;
	control->preferredHeight = small ? 16 : 21;

	if (!control->maximum) {
		// Indeterminate progress bar.
		control->timerControlItem.thisItem = control;
		control->value = -50;
	}

	OSSetMessageCallback(control, OS_MAKE_MESSAGE_CALLBACK(ProcessProgressBarMessage, nullptr));

	return control;
}

void *OSGetInstanceContext(OSObject _instance) {
	OSInstance *instance = (OSInstance *) _instance;
	return instance->argument;
}

OSObject OSGetInstance(OSObject _object) {
	APIObject *object = (APIObject *) _object;

	if (object->type == API_OBJECT_WINDOW) {
		return ((Window *) object)->instance;
	} else if (object->type == API_OBJECT_CONTROL) {
		return ((Control *) object)->window->instance;
	} else if (object->type == API_OBJECT_GRID) {
		return ((Grid *) object)->window->instance;
	} else {
		OSCrashProcess(OS_FATAL_ERROR_INVALID_PANE_OBJECT);
		return nullptr;
	}
}

void OSGetText(OSObject _control, OSString *string) {
	Control *control = (Control *) _control;
	*string = control->text;
}

void OSSetText(OSObject _control, char *text, size_t textBytes, unsigned resizeMode) {
	Control *control = (Control *) _control;
	CreateString(text, textBytes, &control->text);

	int suggestedWidth = MeasureStringWidth(text, textBytes, FONT_SIZE, fontRegular) + 8;
	int suggestedHeight = GetLineHeight(fontRegular, FONT_SIZE);

	if (resizeMode & OS_RESIZE_MODE_NO_WIDTH_PADDING) {
		resizeMode ^= OS_RESIZE_MODE_NO_WIDTH_PADDING;
		suggestedWidth -= 8;
	}

	if (resizeMode & OS_RESIZE_MODE_ADDITIONAL_WIDTH_PADDING) {
		resizeMode ^= OS_RESIZE_MODE_ADDITIONAL_WIDTH_PADDING;
		suggestedWidth += 8;
	}

	if (control->icon) {
		suggestedWidth += control->icon->region.right - control->icon->region.left + ICON_TEXT_GAP;
	}

	OSMessage message;

	if (resizeMode) {
		if (resizeMode == OS_RESIZE_MODE_EXACT || suggestedWidth > control->preferredWidth) {
			control->preferredWidth = suggestedWidth;
		}

		if (resizeMode == OS_RESIZE_MODE_EXACT || suggestedHeight > control->preferredHeight) {
			control->preferredHeight = suggestedHeight;
		}

		control->relayout = true;
		message.type = OS_MESSAGE_CHILD_UPDATED;
		OSSendMessage(control->parent, &message);
	}

	RepaintControl(control);
	message.type = OS_MESSAGE_TEXT_UPDATED;
	OSSendMessage(control, &message);
}

void OSAddGUIObjectToControl(OSObject _parent, OSObject _child) {
	Control *parent = (Control *) _parent;
	GUIObject *child = (GUIObject *) _child;
	child->parent = parent;
	child->layout = OS_CELL_FILL;
}

OSCallbackResponse OSRelayMessageToChild(OSObject parent, OSObject child, OSMessage *message) {
	(void) parent;

	if (message->type == OS_MESSAGE_PARENT_UPDATED) OSSendMessage(child, message);
	if (message->type == OS_MESSAGE_DESTROY) OSSendMessage(child, message);

	if (message->type == OS_MESSAGE_MOUSE_MOVED) {
		OSMessage m;
		m.type = OS_MESSAGE_HIT_TEST;
		m.hitTest.positionX = message->mouseMoved.newPositionX;
		m.hitTest.positionY = message->mouseMoved.newPositionY;
		OSSendMessage(child, &m);
		if (m.hitTest.result) return OSSendMessage(child, message);
	}

	return OS_CALLBACK_NOT_HANDLED;
}

void OSAddControl(OSObject _grid, unsigned column, unsigned row, OSObject _control, unsigned layout) {
	GUIObject *_object = (GUIObject *) _grid;

	if (_object->type == API_OBJECT_WINDOW) {
		Window *window = (Window *) _grid;
		_grid = window->root;

		if (window->flags & OS_CREATE_WINDOW_MENU) {
			column = 0;
			row = 0;
		} else if (window->flags & OS_CREATE_WINDOW_WITH_MENUBAR) {
			_grid = window->root->objects[7];
			column = 0;
			row = 1;
		} else if (window->flags & OS_CREATE_WINDOW_NORMAL) {
			column = 1;
			row = 2;
		}
	}

	Grid *grid = (Grid *) _grid;

	if ((layout & ~(OS_CELL_H_INDENT_1 | OS_CELL_H_INDENT_2)) == OS_FLAGS_DEFAULT) {
		layout = grid->defaultLayout;
	}

	grid->relayout = true;
	SetParentDescendentInvalidationFlags(grid, DESCENDENT_RELAYOUT);

	if (column >= grid->columns || row >= grid->rows) {
		OSCrashProcess(OS_FATAL_ERROR_OUT_OF_GRID_BOUNDS);
	}

	GUIObject *control = (GUIObject *) _control;
	if (control->type != API_OBJECT_CONTROL && control->type != API_OBJECT_GRID) OSCrashProcess(OS_FATAL_ERROR_INVALID_PANE_OBJECT);
	control->layout = layout;

	if (control->parent) {
		OSCrashProcess(OS_FATAL_ERROR_OBJECT_ALREADY_HAS_PARENT);
	}

	control->parent = grid;

	GUIObject **object = grid->objects + (row * grid->columns + column);
	if (*object) OSCrashProcess(OS_FATAL_ERROR_OVERWRITE_GRID_OBJECT);
	*object = control;

	{
		OSMessage message;
		message.parentUpdated.window = grid->window;
		message.type = OS_MESSAGE_PARENT_UPDATED;
		OSSendMessage(control, &message);
	}
}

static OSCallbackResponse ProcessGridMessage(OSObject _object, OSMessage *message) {
	OSCallbackResponse response = OS_CALLBACK_HANDLED;
	Grid *grid = (Grid *) _object;

	switch (message->type) {
		case OS_MESSAGE_HIT_TEST: {
			message->hitTest.result = IsPointInRectangle(grid->inputBounds, message->hitTest.positionX, message->hitTest.positionY)
					       && IsPointInRectangle(grid->bounds,      message->hitTest.positionX, message->hitTest.positionY);
		} break;

		case OS_MESSAGE_SET_PROPERTY: {
			void *value = message->setProperty.value;
			int valueInt = (int) (uintptr_t) value;
			bool repaint = false;

			switch (message->setProperty.index) {
				case OS_GRID_PROPERTY_BORDER_SIZE: {
					grid->borderSize = OS_MAKE_RECTANGLE_ALL(valueInt);
					repaint = true;
				} break;

				case OS_GRID_PROPERTY_GAP_SIZE: {
					grid->gapSize = valueInt;
					repaint = true;
				} break;

				default: {
					SetGUIObjectProperty(grid, message);
				} break;
			}

			if (repaint) {
				grid->repaint = true;
				SetParentDescendentInvalidationFlags(grid, DESCENDENT_REPAINT);
			}
		} break;

		case OS_MESSAGE_REMOVE_CHILD: {
			uintptr_t index = FindObjectInGrid(grid, message->removeChild.child);

			if (index == grid->columns * grid->rows) {
				OSCrashProcess(OS_FATAL_ERROR_INCORRECT_OBJECT_PARENT);
			}

			grid->objects[index] = nullptr;

			grid->relayout = true;
			SetParentDescendentInvalidationFlags(grid, DESCENDENT_RELAYOUT);
		} break;

		case OS_MESSAGE_LAYOUT: {
			if (grid->relayout || message->layout.force) {
				grid->descendentInvalidationFlags &= ~DESCENDENT_RELAYOUT;
				grid->relayout = false;

				grid->bounds = OS_MAKE_RECTANGLE(
						message->layout.left, message->layout.right,
						message->layout.top, message->layout.bottom);

				if (!grid->layout) grid->layout = OS_CELL_H_EXPAND | OS_CELL_V_EXPAND;
				StandardCellLayout(grid);

				tryAgain:;

				OSZeroMemory(grid->widths, sizeof(int) * grid->columns);
				OSZeroMemory(grid->heights, sizeof(int) * grid->rows);
				OSZeroMemory(grid->minimumWidths, sizeof(int) * grid->columns);
				OSZeroMemory(grid->minimumHeights, sizeof(int) * grid->rows);

				int pushH = 0, pushV = 0;

				if (grid->verbose) {
					OSPrint("->Laying out grid %x (%d by %d) into %d->%d, %d->%d (given %d->%d, %d->%d), layout = %X%X\n", 
							grid, grid->columns, grid->rows, grid->bounds.left, grid->bounds.right, grid->bounds.top, grid->bounds.bottom,
							message->layout.left, message->layout.right, message->layout.top, message->layout.bottom, grid->layout);
				}

				for (uintptr_t i = 0; i < grid->columns; i++) {
					for (uintptr_t j = 0; j < grid->rows; j++) {
						GUIObject **object = grid->objects + (j * grid->columns + i);
						if (*object && (*object)->layout == OS_CELL_HIDDEN) continue;

						OSMessage message;
						message.type = OS_MESSAGE_MEASURE;
						message.measure.parentWidth = grid->bounds.right - grid->bounds.left - (grid->borderSize.left + grid->borderSize.right + grid->gapSize * (grid->columns - 1)); 
						message.measure.parentHeight = grid->bounds.bottom - grid->bounds.top - (grid->borderSize.top + grid->borderSize.bottom + grid->gapSize * (grid->rows - 1)); 
						if (OSSendMessage(*object, &message) == OS_CALLBACK_NOT_HANDLED) continue;

						int width = message.measure.preferredWidth;
						int height = message.measure.preferredHeight;

						// OSPrint("Measuring %d, %d: %d, %d, %d, %d\n", i, j, width, height, message.measure.minimumWidth, message.measure.minimumHeight);

						if ((*object)->layout & OS_CELL_H_PUSH) { bool a = grid->widths[i] == DIMENSION_PUSH; grid->widths[i] = DIMENSION_PUSH; if (!a) pushH++; }
						else if (grid->widths[i] < width && grid->widths[i] != DIMENSION_PUSH) grid->widths[i] = width;
						if ((*object)->layout & OS_CELL_V_PUSH) { bool a = grid->heights[j] == DIMENSION_PUSH; grid->heights[j] = DIMENSION_PUSH; if (!a) pushV++; }
						else if (grid->heights[j] < height && grid->heights[j] != DIMENSION_PUSH) grid->heights[j] = height;

						if (grid->minimumWidths[i] < message.measure.preferredWidth) grid->minimumWidths[i] = message.measure.preferredWidth;
						if (grid->minimumHeights[j] < message.measure.preferredHeight) grid->minimumHeights[j] = message.measure.preferredHeight;
					}
				}

				if (grid->verbose) {
					OSPrint("->Results for grid %x (%d by %d)\n", grid, grid->columns, grid->rows);

					for (uintptr_t i = 0; i < grid->columns; i++) {
						OSPrint("Column %d is pref: %dpx, min: %dpx\n", i, grid->widths[i], grid->minimumWidths[i]);
					}

					for (uintptr_t j = 0; j < grid->rows; j++) {
						OSPrint("Row %d is pref: %dpx, min: %dpx\n", j, grid->heights[j], grid->minimumHeights[j]);
					}
				}

				if (pushH) {
					int usedWidth = grid->borderSize.left + grid->borderSize.right + grid->gapSize * (grid->columns - 1); 
					for (uintptr_t i = 0; i < grid->columns; i++) if (grid->widths[i] != DIMENSION_PUSH) usedWidth += grid->widths[i];
					int widthPerPush = (grid->bounds.right - grid->bounds.left - usedWidth) / pushH;

					for (uintptr_t i = 0; i < grid->columns; i++) {
						if (grid->widths[i] == DIMENSION_PUSH) {
							if (widthPerPush < grid->minimumWidths[i] && grid->treatPreferredDimensionsAsMinima) {
								grid->widths[i] = grid->minimumWidths[i];
							} else {
								grid->widths[i] = widthPerPush;
							}
						}
					}
				}

				if (pushV) {
					int usedHeight = grid->borderSize.top + grid->borderSize.bottom + grid->gapSize * (grid->rows - 1); 
					for (uintptr_t j = 0; j < grid->rows; j++) if (grid->heights[j] != DIMENSION_PUSH) usedHeight += grid->heights[j];
					int heightPerPush = (grid->bounds.bottom - grid->bounds.top - usedHeight) / pushV; 

					for (uintptr_t j = 0; j < grid->rows; j++) {
						if (grid->heights[j] == DIMENSION_PUSH) {
							if (heightPerPush >= grid->minimumHeights[j] || !grid->treatPreferredDimensionsAsMinima) {
								grid->heights[j] = heightPerPush;
							} else {
								grid->heights[j] = grid->minimumHeights[j];
							}
						}
					}
				}

				{
					OSMessage message;
					message.type = OS_MESSAGE_CHECK_LAYOUT;
					message.checkLayout.widths = grid->widths;
					message.checkLayout.heights = grid->heights;

					OSCallbackResponse response = OSSendMessage(grid, &message);

					if (response == OS_CALLBACK_REJECTED) {
						goto tryAgain;
					}
				}

				OSRectangle clip;
				ClipRectangle(message->layout.clip, grid->bounds, &clip);

				OSMessage message2;

				bool rightToLeft = !grid->noRightToLeftLayout 
					&& osSystemConstants[OS_SYSTEM_CONSTANT_RIGHT_TO_LEFT_LAYOUT];

				int posX;

				if (rightToLeft) {
					posX = grid->bounds.right - grid->borderSize.right + grid->xOffset;
				} else {
					posX = grid->bounds.left + grid->borderSize.left - grid->xOffset;
				}

				for (uintptr_t i = 0; i < grid->columns; i++) {
					int posY = grid->bounds.top + grid->borderSize.top - grid->yOffset;
					if (rightToLeft) posX -= grid->widths[i];

					for (uintptr_t j = 0; j < grid->rows; j++) {
						GUIObject **object = grid->objects + (j * grid->columns + i);
						if (*object && (*object)->layout == OS_CELL_HIDDEN) continue;

						message2.type = OS_MESSAGE_LAYOUT;
						message2.layout.clip = clip;
						message2.layout.force = true;
						message2.layout.left = posX;
						message2.layout.right = posX + grid->widths[i];
						message2.layout.top = posY;
						message2.layout.bottom = posY + grid->heights[j];

						OSSendMessage(*object, &message2);

						posY += grid->heights[j] + grid->gapSize;
					}

					if (!rightToLeft) posX += grid->widths[i] + grid->gapSize;
					if (rightToLeft) posX -= grid->gapSize;
				}

				grid->repaint = true;
				SetParentDescendentInvalidationFlags(grid, DESCENDENT_REPAINT);
			} else if (grid->descendentInvalidationFlags & DESCENDENT_RELAYOUT) {
				grid->descendentInvalidationFlags &= ~DESCENDENT_RELAYOUT;

				for (uintptr_t i = 0; i < grid->columns * grid->rows; i++) {
					if (grid->objects[i]) {
						message->layout.left   = ((GUIObject *) grid->objects[i])->cellBounds.left;
						message->layout.right  = ((GUIObject *) grid->objects[i])->cellBounds.right;
						message->layout.top    = ((GUIObject *) grid->objects[i])->cellBounds.top;
						message->layout.bottom = ((GUIObject *) grid->objects[i])->cellBounds.bottom;

						OSSendMessage(grid->objects[i], message);
					}
				}
			}
		} break;

		case OS_MESSAGE_MEASURE: {
			OSZeroMemory(grid->widths, sizeof(int) * grid->columns);
			OSZeroMemory(grid->heights, sizeof(int) * grid->rows);

			if (grid->verbose) {
				OSPrint("measuring grid...\n");
			}

			for (uintptr_t i = 0; i < grid->columns; i++) {
				for (uintptr_t j = 0; j < grid->rows; j++) {
					GUIObject **object = grid->objects + (j * grid->columns + i);
					if (!(*object)) continue;

					OSRectangle oldBounds = (*object)->bounds;
					message->measure.parentWidth = oldBounds.right - oldBounds.left;
					message->measure.parentHeight = oldBounds.bottom - oldBounds.top;
					if (OSSendMessage(*object, message) == OS_CALLBACK_NOT_HANDLED) continue;

					int width = message->measure.preferredWidth;
					int height = message->measure.preferredHeight;

					if (grid->widths[i] < width) grid->widths[i] = width;
					if (grid->heights[j] < height) grid->heights[j] = height;
				}
			}

			int width = grid->borderSize.left, height = grid->borderSize.top;

			for (uintptr_t i = 0; i < grid->columns; i++) width += grid->widths[i] + (i == grid->columns - 1 ? grid->borderSize.left : grid->gapSize);
			for (uintptr_t j = 0; j < grid->rows; j++) height += grid->heights[j] + (j == grid->rows - 1 ? grid->borderSize.top : grid->gapSize);

			if (!grid->suggestWidth) grid->preferredWidth = message->measure.preferredWidth = width;
			else message->measure.preferredWidth = grid->preferredWidth;
			if (!grid->suggestHeight) grid->preferredHeight = message->measure.preferredHeight = height;
			else message->measure.preferredHeight = grid->preferredHeight;

			if (grid->verbose) {
				OSPrint("results: %d by %d\n", message->measure.preferredWidth, message->measure.preferredHeight);
			}
		} break;

		case OS_MESSAGE_PAINT: {
			if (grid->descendentInvalidationFlags & DESCENDENT_REPAINT || grid->repaint || message->paint.force) {
				grid->descendentInvalidationFlags &= ~DESCENDENT_REPAINT;

				OSMessage m = *message;
				m.paint.force = message->paint.force || grid->repaint;
				grid->repaint = false;

				OSRectangle clip;

				if (ClipRectangle(message->paint.clip, grid->bounds, &clip)) {
					if (m.paint.force) {
						OSMessage m;
						m.type = OS_MESSAGE_PAINT_BACKGROUND;
						m.paintBackground.surface = message->paint.surface;
						ClipRectangle(message->paint.clip, grid->bounds, &m.paintBackground.clip);
						OSSendMessage(grid, &m);
					}

					for (uintptr_t i = 0; i < grid->columns * grid->rows; i++) {
						GUIObject *object = grid->objects[i];
						if (object && (object->repaint || m.paint.force || (object->descendentInvalidationFlags & DESCENDENT_REPAINT)) 
								&& ClipRectangle(clip, object->bounds, &m.paint.clip)) {
							OSSendMessage(object, &m);
						}
					}
				}
			}
		} break;

		case OS_MESSAGE_PAINT_BACKGROUND: {
			// What happens with overalapping children?

			if (grid->background) {
				DrawUIImage(nullptr, grid->bounds, grid->background, 0xFF, &message->paintBackground.clip, message->paintBackground.surface);
			} else if (grid->backgroundColor) {
				OSFillRectangle(message->paint.surface, message->paintBackground.clip, OSColor(grid->backgroundColor));
			} else {
				// We don't have a background.
				// Propagate the message to our parent.
				OSSendMessage(grid->parent, message);
			}
		} break;

		case OS_MESSAGE_PARENT_UPDATED: {
			grid->window = (Window *) message->parentUpdated.window;

			for (uintptr_t i = 0; i < grid->columns * grid->rows; i++) {
				OSSendMessage(grid->objects[i], message);
			}
		} break;

		case OS_MESSAGE_CHILD_UPDATED: {
			grid->relayout = true;
			SetParentDescendentInvalidationFlags(grid, DESCENDENT_RELAYOUT);
		} break;

		case OS_MESSAGE_DESTROY: {
			for (uintptr_t i = 0; i < grid->columns * grid->rows; i++) {
				OSSendMessage(grid->objects[i], message);
			}

			if (grid->window->hoverGrid == grid) {
				grid->window->hoverGrid = nullptr;
			}

			GUIFree(grid);
		} break;

		case OS_MESSAGE_DISABLE: {
			for (uintptr_t i = 0; i < grid->columns * grid->rows; i++) {
				OSSendMessage(grid->objects[i], message);
			}
		} break;

		case OS_MESSAGE_MOUSE_MOVED: {
			if (!IsPointInRectangle(grid->bounds, message->mouseMoved.newPositionX, message->mouseMoved.newPositionY)) {
				if (grid->window->hoverGrid == grid) grid->window->hoverGrid = nullptr;
				break;
			}

			grid->window->hoverGrid = grid;

			// Process mouse events in reverse for grids with overlapping children.
			for (intptr_t i = grid->columns * grid->rows - 1; i >= 0; i--) {
				OSSendMessage(grid->objects[i], message);
			}
		} break;

		case OS_MESSAGE_KEY_PRESSED: {
			response = OS_CALLBACK_NOT_HANDLED;

			if (message->keyboard.ctrl || message->keyboard.alt) {
				break;
			}

			OSObject previousFocus = message->keyboard.notHandledBy;
			int delta, start, end, i = FindObjectInGrid(grid, previousFocus);
			bool loopAround, foundFirstTime = i != (int) (grid->columns * grid->rows);

			if (message->keyboard.scancode == OS_SCANCODE_TAB) {
				delta = message->keyboard.shift ? -1 : 1;
				start = message->keyboard.shift ? grid->columns * grid->rows - 1 : 0;
				end = message->keyboard.shift ? -1 : grid->columns * grid->rows;
				loopAround = false;
			} else if (message->keyboard.scancode == OS_SCANCODE_LEFT_ARROW && foundFirstTime) {
				delta = -1;
				end = i - (i % grid->columns) - 1;
				start = end + grid->columns;
				loopAround = true;

				if (grid->columns == 1) {
					break;
				}
			} else if (message->keyboard.scancode == OS_SCANCODE_UP_ARROW && foundFirstTime) {
				delta = -grid->columns;
				end = (i % grid->columns) - grid->columns;
				start = end + grid->columns * grid->rows;
				loopAround = true;

				if (grid->rows == 1) {
					break;
				}
			} else if (message->keyboard.scancode == OS_SCANCODE_DOWN_ARROW && foundFirstTime) {
				delta = grid->columns;
				start = (i % grid->columns);
				end = start + grid->columns * grid->rows;
				loopAround = true;

				if (grid->rows == 1) {
					break;
				}
			} else if (message->keyboard.scancode == OS_SCANCODE_RIGHT_ARROW && foundFirstTime) {
				delta = 1;
				start = i - (i % grid->columns);
				end = start + grid->columns;
				loopAround = true;

				if (grid->columns == 1) {
					break;
				}
			} else {
				break;
			}

			retryTab:;
			i = FindObjectInGrid(grid, previousFocus);

			if (i == (int) (grid->columns * grid->rows)) {
				i = start;
			} else {
				i += delta;
			}

			while (i != end) {
				if (grid->objects[i] && grid->objects[i]->tabStop && !grid->objects[i]->disabled) {
					break;
				} else {
					i += delta;
				}
			}

			if (i == end && !loopAround) {
				// We're out of tab-stops in this grid.
				response = OS_CALLBACK_NOT_HANDLED;
			} else {
				if (loopAround && i == end) {
					loopAround = false;
					i = start;
				}

				OSObject focus = grid->objects[i];
				response = OSSendMessage(focus, message);

				if (response == OS_CALLBACK_NOT_HANDLED) {
					previousFocus = focus;
					goto retryTab;
				}
			}
		} break;

		default: {
			response = OS_CALLBACK_NOT_HANDLED;
		} break;
	}

	return response;
}

OSObject OSCreateGrid(unsigned columns, unsigned rows, OSGridStyle style) {
	uint8_t *memory = (uint8_t *) GUIAllocate(sizeof(Grid) + sizeof(OSObject) * columns * rows + 2 * sizeof(int) * (columns + rows), true);

	Grid *grid = (Grid *) memory;
	grid->type = API_OBJECT_GRID;
	grid->tabStop = true;

	grid->backgroundColor = STANDARD_BACKGROUND_COLOR;
	grid->columns = columns;
	grid->rows = rows;
	grid->objects = (GUIObject **) (memory + sizeof(Grid));
	grid->widths = (int *) (memory + sizeof(Grid) + sizeof(OSObject) * columns * rows);
	grid->heights = (int *) (memory + sizeof(Grid) + sizeof(OSObject) * columns * rows + sizeof(int) * columns);
	grid->minimumWidths = (int *) (memory + sizeof(Grid) + sizeof(OSObject) * columns * rows + sizeof(int) * columns + sizeof(int) * rows);
	grid->minimumHeights = (int *) (memory + sizeof(Grid) + sizeof(OSObject) * columns * rows + sizeof(int) * columns + sizeof(int) * rows + sizeof(int) * columns);
	grid->style = style;

	switch (style) {
		case OS_GRID_STYLE_GROUP_BOX: {
			grid->borderSize = OS_MAKE_RECTANGLE(8, 8, 6, 6);
			grid->gapSize = 4;
			grid->background = &gridBox;
			grid->defaultLayout = OS_CELL_H_FILL;
		} break;

		case OS_GRID_STYLE_TAB_PANE_CONTENT: {
			grid->borderSize = OS_MAKE_RECTANGLE(8, 8, 8, 8);
			grid->gapSize = 4;
			grid->background = &tabContent;
			grid->defaultLayout = OS_CELL_H_FILL;
		} break;

		case OS_GRID_STYLE_MENU: {
			grid->borderSize = OS_MAKE_RECTANGLE(4, 4, 3, 1);
			grid->gapSize = 0;
			grid->background = &menuBox;
		} break;

		case OS_GRID_STYLE_BLANK_MENU: {
			grid->borderSize = OS_MAKE_RECTANGLE(2, 2, 2, 2);
			grid->gapSize = 0;
			grid->background = &menuBoxBlank;
		} break;

		case OS_GRID_STYLE_MENUBAR: {
			grid->borderSize = OS_MAKE_RECTANGLE(0, 0, -1, 2);
			grid->gapSize = 0;
			grid->background = &menubarBackground;
		} break;

		case OS_GRID_STYLE_LAYOUT: {
			grid->borderSize = OS_MAKE_RECTANGLE_ALL(0);
			grid->gapSize = 0;
			grid->backgroundColor = 0;
		} break;

		case OS_GRID_STYLE_CONTAINER: {
			grid->borderSize = OS_MAKE_RECTANGLE_ALL(8);
			grid->gapSize = 6;
		} break;

		case OS_GRID_STYLE_CONTAINER_WITHOUT_BORDER: {
			grid->borderSize = OS_MAKE_RECTANGLE_ALL(0);
			grid->gapSize = 6;
		} break;

		case OS_GRID_STYLE_CONTAINER_ALT: {
			grid->borderSize = OS_MAKE_RECTANGLE_ALL(8);
			grid->gapSize = 6;
			grid->background = &dialogAltAreaBox;
		} break;

		case OS_GRID_STYLE_STATUS_BAR: {
			grid->borderSize = OS_MAKE_RECTANGLE_ALL(4);
			grid->gapSize = 6;
			grid->background = &dialogAltAreaBox;
		} break;

		case OS_GRID_STYLE_TOOLBAR: {
			grid->borderSize = OS_MAKE_RECTANGLE(5, 5, 0, 0);
			grid->gapSize = 4;
			grid->preferredHeight = 31;
			grid->suggestHeight = true;
			grid->background = &toolbarBackground;
			grid->defaultLayout = OS_CELL_V_CENTER | OS_CELL_V_PUSH;
		} break;

		case OS_GRID_STYLE_TOOLBAR_ALT: {
			grid->borderSize = OS_MAKE_RECTANGLE(5, 5, 0, 0);
			grid->gapSize = 4;
			grid->preferredHeight = 31;
			grid->suggestHeight = true;
			grid->background = &toolbarBackgroundAlt;
			grid->defaultLayout = OS_CELL_V_CENTER | OS_CELL_V_PUSH;
		} break;
	}

	OSSetMessageCallback(grid, OS_MAKE_MESSAGE_CALLBACK(ProcessGridMessage, nullptr));

	return grid;
}

int OSGetSliderPosition(OSObject _slider) {
	Slider *slider = (Slider *) _slider;

	if (slider->mode & OS_SLIDER_MODE_OPPOSITE_VALUE) {
		return slider->maximum - slider->value + slider->minimum;
	} else {
		return slider->value;
	}
}

static void SliderValueModified(Slider *grid, bool sendNotification = true) {
	if (grid->value < grid->minimum) grid->value = grid->minimum;
	if (grid->value > grid->maximum) grid->value = grid->maximum;
	if (grid->mode & OS_SLIDER_MODE_SNAP_TO_TICKS) grid->value -= grid->value % grid->minorTickSpacing;

	{
		OSMessage message;
		message.type = OS_MESSAGE_CHILD_UPDATED;
		OSSendMessage(grid, &message);

		if (sendNotification) {
			OSNotification n;
			n.type = OS_NOTIFICATION_VALUE_CHANGED;
			n.valueChanged.newValue = OSGetSliderPosition(grid);
			OSSendNotification(grid, grid->notificationCallback, &n, OSGetInstance(grid));
		}
	}
}

void OSSetSliderPosition(Slider *slider, int position, bool sendValueChangedNotification) {
	if (slider->mode & OS_SLIDER_MODE_OPPOSITE_VALUE) {
		slider->value = slider->maximum - position + slider->minimum;
	} else {
		slider->value = position;
	}

	SliderValueModified(slider, sendValueChangedNotification);
}

OSCallbackResponse ProcessSliderMessage(OSObject object, OSMessage *message) {
	Slider *grid = (Slider *) object;
	OSCallbackResponse response = OS_CALLBACK_NOT_HANDLED;

	if (message->type == OS_MESSAGE_LAYOUT) {
		if (grid->relayout || message->layout.force) {
			grid->relayout = false;
			grid->bounds = OS_MAKE_RECTANGLE(
					message->layout.left, message->layout.right,
					message->layout.top, message->layout.bottom);
			grid->cellBounds = grid->bounds;
			grid->repaint = true;
			SetParentDescendentInvalidationFlags(grid, DESCENDENT_REPAINT);

			int start, end;

			if (grid->mode & OS_SLIDER_MODE_HORIZONTAL) {
				start = grid->bounds.left + 14;
				end = grid->bounds.right - 14;
			} else {
				start = grid->bounds.top + 14;
				end = grid->bounds.bottom - 14;
			}

			int length = end - start;
			int position = length * (grid->value - grid->minimum) / (grid->maximum - grid->minimum) + start;

			OSMessage m;
			m.type = OS_MESSAGE_LAYOUT;
			m.layout.force = true;
			m.layout.clip = message->layout.clip;

			int *dStart, *dEnd, sStart, sEnd;

			if (grid->mode & OS_SLIDER_MODE_HORIZONTAL) {
				dStart = &m.layout.top;
				dEnd = &m.layout.bottom;
				sStart = message->layout.top;
				sEnd = message->layout.bottom;

				m.layout.left = position - 6;
				m.layout.right = position - 6 + 13;
			} else {
				dStart = &m.layout.left;
				dEnd = &m.layout.right;
				sStart = message->layout.left;
				sEnd = message->layout.right;

				m.layout.top = position - 6;
				m.layout.bottom = position - 6 + 13;
			}

			if ((grid->mode & OS_SLIDER_MODE_TICKS_BENEATH) && (grid->mode & OS_SLIDER_MODE_TICKS_ABOVE)) {
				*dStart = sStart + 4;
				*dEnd = sEnd - 4;
			} else if (grid->mode & OS_SLIDER_MODE_TICKS_ABOVE) {
				*dStart = sEnd - 2 - 20;
				*dEnd = sEnd - 2;
			} else if (grid->mode & OS_SLIDER_MODE_TICKS_BENEATH) {
				*dStart = sStart + 2;
				*dEnd = sStart + 2 + 20;
			} else {
				*dStart = sStart + 4;
				*dEnd = sEnd - 4;
			}

			OSSendMessage(grid->objects[0], &m);

			response = OS_CALLBACK_HANDLED;
		}
	} else if (message->type == OS_MESSAGE_MEASURE) {
		message->measure.preferredWidth = grid->preferredWidth;
		message->measure.preferredHeight = grid->preferredHeight;
		response = OS_CALLBACK_HANDLED;
	} else if (message->type == OS_MESSAGE_PAINT_BACKGROUND) {
		DrawUIImage(nullptr, grid->bounds, &sliderBox, 0xFF, &message->paintBackground.clip, message->paintBackground.surface);

		int start, end;

		if (grid->mode & OS_SLIDER_MODE_HORIZONTAL) {
			start = grid->bounds.left + 14;
			end = grid->bounds.right - 14;
		} else {
			start = grid->bounds.top + 14;
			end = grid->bounds.bottom - 14;
		}

		int length = end - start;
		float lengthPerTick = (float) length / (float) (grid->maximum - grid->minimum);
		float lengthPerMinorTick = lengthPerTick * grid->minorTickSpacing;

		{
			float i = start;
			int j = 0;

			while (i - 0.5f <= end) {
				if (grid->mode & OS_SLIDER_MODE_HORIZONTAL) {
					if (grid->mode & OS_SLIDER_MODE_TICKS_ABOVE) {
						OSDrawSurfaceClipped(message->paint.surface, OS_SURFACE_UI_SHEET, 
								OS_MAKE_RECTANGLE((int) i, (int) i + 2, grid->bounds.top + 11 - (j ? 5 : 7), grid->bounds.top + 11), 
								tickHorizontal.region, tickHorizontal.border, tickHorizontal.drawMode, 0xFF, message->paintBackground.clip);
					}

					if (grid->mode & OS_SLIDER_MODE_TICKS_BENEATH) {
						OSDrawSurfaceClipped(message->paint.surface, OS_SURFACE_UI_SHEET, 
								OS_MAKE_RECTANGLE((int) i, (int) i + 2, grid->bounds.top + 17, grid->bounds.top + 17 + (j ? 5 : 7)), 
								tickHorizontal.region, tickHorizontal.border, tickHorizontal.drawMode, 0xFF, message->paintBackground.clip);
					}
				} else {
					if (grid->mode & OS_SLIDER_MODE_TICKS_ABOVE) {
						OSDrawSurfaceClipped(message->paint.surface, OS_SURFACE_UI_SHEET, 
								OS_MAKE_RECTANGLE(grid->bounds.left + 11 - (j ? 5 : 7), grid->bounds.left + 11, (int) i, (int) i + 2), 
								tickVertical.region, tickVertical.border, tickVertical.drawMode, 0xFF, message->paintBackground.clip);
					}

					if (grid->mode & OS_SLIDER_MODE_TICKS_BENEATH) {
						OSDrawSurfaceClipped(message->paint.surface, OS_SURFACE_UI_SHEET, 
								OS_MAKE_RECTANGLE(grid->bounds.left + 17, grid->bounds.left + 17 + (j ? 5 : 7), (int) i, (int) i + 2), 
								tickVertical.region, tickVertical.border, tickVertical.drawMode, 0xFF, message->paintBackground.clip);
					}
				}

				i += lengthPerMinorTick;
				j++;

				if (j == grid->majorTickSpacing) {
					j = 0;
				}
			}
		}
	} else if (message->type == OS_MESSAGE_KEY_PRESSED) {
		if (!message->keyboard.ctrl && !message->keyboard.alt && !message->keyboard.shift) {
			response = OS_CALLBACK_HANDLED;

			if (message->keyboard.scancode == OS_SCANCODE_LEFT_ARROW || message->keyboard.scancode == OS_SCANCODE_UP_ARROW) {
				grid->value -= grid->minorTickSpacing;
			} else if (message->keyboard.scancode == OS_SCANCODE_RIGHT_ARROW || message->keyboard.scancode == OS_SCANCODE_DOWN_ARROW) {
				grid->value += grid->minorTickSpacing;
			} else {
				response = OS_CALLBACK_NOT_HANDLED;
			}

			SliderValueModified(grid);
		}
	} else if (message->type == OS_MESSAGE_CLICK_REPEAT) {
		grid->value += grid->previousClickAdjustment;
		SliderValueModified(grid);
	} else if (message->type == OS_MESSAGE_MOUSE_LEFT_PRESSED) {
		response = OS_CALLBACK_HANDLED;

		if (grid->mode & OS_SLIDER_MODE_HORIZONTAL) {
			if (message->mousePressed.positionX < grid->handle->bounds.left) {
				grid->previousClickAdjustment = -grid->minorTickSpacing;
			} else {
				grid->previousClickAdjustment = grid->minorTickSpacing;
			}
		} else {
			if (message->mousePressed.positionY < grid->handle->bounds.top) {
				grid->previousClickAdjustment = -grid->minorTickSpacing;
			} else {
				grid->previousClickAdjustment = grid->minorTickSpacing;
			}
		}


		grid->value += grid->previousClickAdjustment;
		SliderValueModified(grid);
	}

	if (response == OS_CALLBACK_NOT_HANDLED) {
		response = OSForwardMessage(object, OS_MAKE_MESSAGE_CALLBACK(ProcessGridMessage, nullptr), message);
	}

	return response;
}

OSCallbackResponse ProcessSliderHandleMessage(OSObject object, OSMessage *message) {
	SliderHandle *control = (SliderHandle *) object;
	Slider *slider = control->slider;
	OSCallbackResponse response = OS_CALLBACK_NOT_HANDLED;

	if (message->type == OS_MESSAGE_MOUSE_DRAGGED) {
		int start, end, mouse;

		if (slider->mode & OS_SLIDER_MODE_HORIZONTAL) {
			start = slider->bounds.left + 14;
			end = slider->bounds.right - 14;
			mouse = message->mouseDragged.newPositionX;
		} else {
			start = slider->bounds.top + 14;
			end = slider->bounds.bottom - 14;
			mouse = message->mouseDragged.newPositionY;
		}

		float position = (float) (mouse + 3 - start) / (float) (end - start);
		slider->value = (int) (position * (slider->maximum - slider->minimum) + slider->minimum);

		SliderValueModified(slider);
		response = OS_CALLBACK_HANDLED;
	}

	if (response == OS_CALLBACK_NOT_HANDLED) {
		response = OSForwardMessage(object, OS_MAKE_MESSAGE_CALLBACK(ProcessControlMessage, nullptr), message);
	}

	return response;
}

OSObject OSCreateSlider(int minimum, int maximum, int initialValue, 
		int mode, int minorTickSpacing, int majorTickSpacing) {
	uint8_t *memory = (uint8_t *) GUIAllocate(sizeof(Slider) + sizeof(OSObject) * 1, true);

	Slider *slider = (Slider *) memory;
	slider->type = API_OBJECT_GRID;

	slider->tabStop = true;
	slider->columns = 1;
	slider->rows = 1;
	slider->objects = (GUIObject **) (memory + sizeof(Slider));

	slider->minimum = minimum;
	slider->maximum = maximum;
	slider->mode = mode;
	slider->minorTickSpacing = minorTickSpacing;
	slider->majorTickSpacing = majorTickSpacing;

	OSSetSliderPosition(slider, initialValue, false);

	if (mode & OS_SLIDER_MODE_HORIZONTAL) {
		slider->preferredWidth = 168;
		slider->preferredHeight = 28;
	} else {
		slider->preferredHeight = 168;
		slider->preferredWidth = 28;
	}

	OSSetMessageCallback(slider, OS_MAKE_MESSAGE_CALLBACK(ProcessSliderMessage, nullptr));

	{
		SliderHandle *handle = (SliderHandle *) GUIAllocate(sizeof(SliderHandle), true);
		slider->handle = handle;
		handle->type = API_OBJECT_CONTROL;
		handle->slider = slider;
		handle->focusable = true;
		handle->tabStop = true;
		handle->drawParentBackground = true;
		handle->hasFocusedBackground = true;
		OSAddControl(slider, 0, 0, handle, 0);
		OSSetMessageCallback(handle, OS_MAKE_MESSAGE_CALLBACK(ProcessSliderHandleMessage, nullptr));

		if (mode & OS_SLIDER_MODE_HORIZONTAL) {
			if ((mode & OS_SLIDER_MODE_TICKS_BENEATH) && (mode & OS_SLIDER_MODE_TICKS_ABOVE)) {
				handle->backgrounds = sliderVertical;
			} else if (mode & OS_SLIDER_MODE_TICKS_BENEATH) {
				handle->backgrounds = sliderDown;
			} else if (mode & OS_SLIDER_MODE_TICKS_ABOVE) {
				handle->backgrounds = sliderUp;
			} else {
				handle->backgrounds = sliderVertical;
			}

			handle->preferredWidth = 13;
			handle->preferredHeight = 20;
		} else {
			if ((mode & OS_SLIDER_MODE_TICKS_BENEATH) && (mode & OS_SLIDER_MODE_TICKS_ABOVE)) {
				handle->backgrounds = sliderHorizontal;
			} else if (mode & OS_SLIDER_MODE_TICKS_BENEATH) {
				handle->backgrounds = sliderRight;
			} else if (mode & OS_SLIDER_MODE_TICKS_ABOVE) {
				handle->backgrounds = sliderLeft;
			} else {
				handle->backgrounds = sliderHorizontal;
			}

			handle->preferredWidth = 20;
			handle->preferredHeight = 13;
		}
	}

	return slider;
}

static OSCallbackResponse ProcessScrollPaneMessage(OSObject _object, OSMessage *message) {
	OSCallbackResponse response = OS_CALLBACK_NOT_HANDLED;
	Grid *grid = (Grid *) _object;

	if (message->type == OS_MESSAGE_LAYOUT) {
		if (grid->relayout || message->layout.force) {
			grid->relayout = false;
			grid->bounds = OS_MAKE_RECTANGLE(
					message->layout.left, message->layout.right,
					message->layout.top, message->layout.bottom);
			grid->repaint = true;
			SetParentDescendentInvalidationFlags(grid, DESCENDENT_REPAINT);

#if 0
			OSPrint("->Laying out grid %x (%d by %d) into %d->%d, %d->%d (given %d->%d, %d->%d), layout = %X\n", 
					grid, grid->columns, grid->rows, grid->bounds.left, grid->bounds.right, grid->bounds.top, grid->bounds.bottom,
					message->layout.left, message->layout.right, message->layout.top, message->layout.bottom, grid->layout);
#endif

			OSMessage m = {};

			int contentWidth = message->layout.right - message->layout.left - (grid->objects[1] ? SCROLLBAR_SIZE : 0);
			int contentHeight = message->layout.bottom - message->layout.top - (grid->objects[2] ? SCROLLBAR_SIZE : 0);

			m.type = OS_MESSAGE_LAYOUT;
			m.layout.force = true;
			m.layout.clip = message->layout.clip;

			m.layout.top = message->layout.top;
			m.layout.bottom = message->layout.top + contentHeight;
			m.layout.left = message->layout.left;
			m.layout.right = message->layout.left + contentWidth;
			OSSendMessage(grid->objects[0], &m);

			m.type = OS_MESSAGE_MEASURE;
			m.measure.parentWidth = grid->bounds.right - grid->bounds.left - (grid->objects[1] ? SCROLLBAR_SIZE : 0);
			m.measure.parentHeight = grid->bounds.bottom - grid->bounds.top - (grid->objects[2] ? SCROLLBAR_SIZE : 0);;
			OSCallbackResponse r = OSSendMessage(grid->objects[0], &m);
			if (r != OS_CALLBACK_HANDLED) OSCrashProcess(OS_FATAL_ERROR_MESSAGE_SHOULD_BE_HANDLED);

			int minimumWidth = m.measure.preferredWidth;
			int minimumHeight = m.measure.preferredHeight;

			m.type = OS_MESSAGE_LAYOUT;
			m.layout.force = true;
			m.layout.clip = message->layout.clip;

			if (grid->objects[1]) {
				m.layout.top = message->layout.top;
				m.layout.bottom = message->layout.top + contentHeight;
				m.layout.left = message->layout.right - SCROLLBAR_SIZE;
				m.layout.right = message->layout.right;
				OSSendMessage(grid->objects[1], &m);
				OSSetScrollbarMeasurements(grid->objects[1], minimumHeight, contentHeight);
			}

			if (grid->objects[2]) {
				m.layout.top = message->layout.bottom - SCROLLBAR_SIZE;
				m.layout.bottom = message->layout.bottom;
				m.layout.left = message->layout.left;
				m.layout.right = message->layout.left + contentWidth;
				OSSendMessage(grid->objects[2], &m);
				OSSetScrollbarMeasurements(grid->objects[2], minimumWidth, contentWidth);
			}

			{
				m.layout.top = message->layout.bottom - SCROLLBAR_SIZE;
				m.layout.bottom = message->layout.bottom;
				m.layout.left = message->layout.right - SCROLLBAR_SIZE;
				m.layout.right = message->layout.right;
				OSSendMessage(grid->objects[3], &m);
			}

			response = OS_CALLBACK_HANDLED;
		}
	} else if (message->type == OS_MESSAGE_MEASURE) {
		message->measure.preferredWidth = SCROLLBAR_SIZE * 3;
		message->measure.preferredHeight = SCROLLBAR_SIZE * 3;
		response = OS_CALLBACK_HANDLED;
	}

	if (response == OS_CALLBACK_NOT_HANDLED) {
		response = OSForwardMessage(_object, OS_MAKE_MESSAGE_CALLBACK(ProcessGridMessage, nullptr), message);
	}

	return response;
}

OSCallbackResponse ScrollPaneBarMoved(OSNotification *notification) {
	Scrollbar *scrollbar = (Scrollbar *) notification->generator;
	Grid *grid = (Grid *) notification->context;

	if (scrollbar->orientation) {
		// Vertical scrollbar.
		grid->yOffset = notification->valueChanged.newValue;
	} else {
		// Horizontal scrollbar.
		grid->xOffset = notification->valueChanged.newValue;
	}

	SetParentDescendentInvalidationFlags(grid, DESCENDENT_RELAYOUT);
	grid->relayout = true;

	return OS_CALLBACK_HANDLED;
}

OSObject OSCreateScrollPane(OSObject content, unsigned flags) {
	OSObject grid = OSCreateGrid(2, 2, OS_GRID_STYLE_LAYOUT);
	OSSetMessageCallback(grid, OS_MAKE_MESSAGE_CALLBACK(ProcessScrollPaneMessage, nullptr));

	OSAddGrid(grid, 0, 0, content, OS_CELL_FILL);
	((Grid *) content)->treatPreferredDimensionsAsMinima = true;

	if (flags & OS_CREATE_SCROLL_PANE_VERTICAL) {
		OSObject scrollbar = OSCreateScrollbar(OS_ORIENTATION_VERTICAL, true);
		OSAddGrid(grid, 1, 0, scrollbar, OS_CELL_V_PUSH | OS_CELL_V_EXPAND);
		OSSetObjectNotificationCallback(scrollbar, OS_MAKE_NOTIFICATION_CALLBACK(ScrollPaneBarMoved, content));
		// OSPrint("vertical %x\n", scrollbar);
	}

	if (flags & OS_CREATE_SCROLL_PANE_HORIZONTAL) {
		OSObject scrollbar = OSCreateScrollbar(OS_ORIENTATION_HORIZONTAL, true);
		OSAddGrid(grid, 0, 1, scrollbar, OS_CELL_H_PUSH | OS_CELL_H_EXPAND);
		OSSetObjectNotificationCallback(scrollbar, OS_MAKE_NOTIFICATION_CALLBACK(ScrollPaneBarMoved, content));
		// OSPrint("horizontal %x\n", scrollbar);
	}

	if ((flags & OS_CREATE_SCROLL_PANE_VERTICAL) && (flags & OS_CREATE_SCROLL_PANE_HORIZONTAL)) {
		OSObject corner = OSCreateGrid(1, 1, OS_GRID_STYLE_CONTAINER);
		OSAddGrid(grid, 1, 1, corner, OS_CELL_H_EXPAND | OS_CELL_V_EXPAND);
	}

	return grid;
}

int OSGetScrollbarPosition(OSObject object) {
	Scrollbar *scrollbar = (Scrollbar *) object;

	int position = 0;

	if (scrollbar->enabled) {
		float fraction = (float) scrollbar->position / (float) scrollbar->maxPosition;
		float range = (float) (scrollbar->contentSize - scrollbar->viewportSize);
		position = (int) (fraction * range);
	}

	return position;
}

static void ScrollbarPositionChanged(Scrollbar *scrollbar) {
	OSNotification n;
	n.type = OS_NOTIFICATION_VALUE_CHANGED;
	n.valueChanged.newValue = OSGetScrollbarPosition(scrollbar);
	OSSendNotification(scrollbar, scrollbar->notificationCallback, &n, OSGetInstance(scrollbar));
}

static OSCallbackResponse ScrollbarButtonPressed(OSNotification *notification) {
	Control *button = (Control *) notification->generator;
	Scrollbar *scrollbar = (Scrollbar *) button->context;

	int position = OSGetScrollbarPosition(scrollbar);
	int amount = SCROLLBAR_BUTTON_AMOUNT;

	if (notification->context == SCROLLBAR_BUTTON_UP) {
		position -= amount;
	} else if (notification->context == SCROLLBAR_BUTTON_DOWN) {
		position += amount;
	} else if (notification->context == SCROLLBAR_NUDGE_UP) {
		position -= scrollbar->viewportSize;
	} else if (notification->context == SCROLLBAR_NUDGE_DOWN) {
		position += scrollbar->viewportSize;
	}

	OSSetScrollbarPosition(scrollbar, position, true);

	return OS_CALLBACK_HANDLED;
}

static void RelayoutScrollbar(Scrollbar *grid) {
	OSMessage message;
	message.type = OS_MESSAGE_LAYOUT;
	message.layout.force = true;
	message.layout.clip = grid->inputBounds;

	if (grid->orientation) {
		message.layout.left = grid->bounds.left;
		message.layout.right = grid->bounds.right;

		message.layout.top = grid->bounds.top;
		message.layout.bottom = message.layout.top + SCROLLBAR_SIZE;
		OSSendMessage(grid->objects[0], &message);

		if (!grid->enabled) {
			message.layout.top = 0;
			message.layout.bottom = 0;
			OSSendMessage(grid->objects[1], &message);
			OSSendMessage(grid->objects[3], &message);

			message.layout.top = grid->bounds.top + SCROLLBAR_SIZE;
			message.layout.bottom = grid->bounds.bottom - SCROLLBAR_SIZE;
			OSSendMessage(grid->objects[4], &message);
		} else {
			int x = grid->bounds.top + SCROLLBAR_SIZE + grid->position + grid->size;
			message.layout.top = grid->bounds.top + SCROLLBAR_SIZE + grid->position;
			message.layout.bottom = x;
			OSSendMessage(grid->objects[1], &message);
			message.layout.bottom = message.layout.top;
			message.layout.top = grid->bounds.top + SCROLLBAR_SIZE;
			OSSendMessage(grid->objects[3], &message);
			message.layout.top = x;
			message.layout.bottom = grid->bounds.bottom - SCROLLBAR_SIZE;
			OSSendMessage(grid->objects[4], &message);
		}

		message.layout.bottom = grid->bounds.bottom;
		message.layout.top = message.layout.bottom - SCROLLBAR_SIZE;
		OSSendMessage(grid->objects[2], &message);
	} else {
		message.layout.top = grid->bounds.top;
		message.layout.bottom = grid->bounds.bottom;

		message.layout.left = grid->bounds.left;
		message.layout.right = message.layout.left + SCROLLBAR_SIZE;
		OSSendMessage(grid->objects[0], &message);

		if (!grid->enabled) {
			message.layout.left = 0;
			message.layout.right = 0;
			OSSendMessage(grid->objects[1], &message);
			OSSendMessage(grid->objects[3], &message);

			message.layout.left = grid->bounds.left + SCROLLBAR_SIZE;
			message.layout.right = grid->bounds.right - SCROLLBAR_SIZE;
			OSSendMessage(grid->objects[4], &message);
		} else {
			int x = grid->bounds.left + SCROLLBAR_SIZE + grid->position + grid->size;
			message.layout.left = grid->bounds.left + SCROLLBAR_SIZE + grid->position;
			message.layout.right = x;
			OSSendMessage(grid->objects[1], &message);
			message.layout.right = message.layout.left;
			message.layout.left = grid->bounds.left + SCROLLBAR_SIZE;
			OSSendMessage(grid->objects[3], &message);
			message.layout.left = x;
			message.layout.right = grid->bounds.right - SCROLLBAR_SIZE;
			OSSendMessage(grid->objects[4], &message);
		}

		message.layout.right = grid->bounds.right;
		message.layout.left = message.layout.right - SCROLLBAR_SIZE;
		OSSendMessage(grid->objects[2], &message);
	}
}

void OSSetScrollbarPosition(OSObject object, int newPosition, bool sendValueChangedNotification) {
	Scrollbar *scrollbar = (Scrollbar *) object;

	if (scrollbar->enabled) {
		// OSPrint("Set scrollbar position to %d\n", newPosition);

		float range = (float) (scrollbar->contentSize - scrollbar->viewportSize);
		float fraction = (float) newPosition / range;
		scrollbar->position = (fraction * (float) scrollbar->maxPosition);

		if (scrollbar->position < 0) scrollbar->position = 0;
		else if (scrollbar->position >= scrollbar->maxPosition) scrollbar->position = scrollbar->maxPosition;

		if (sendValueChangedNotification) {
			ScrollbarPositionChanged(scrollbar);
		}

		RelayoutScrollbar(scrollbar);
	}
}

static OSCallbackResponse ProcessScrollbarGripMessage(OSObject object, OSMessage *message) {
	Control *grip = (Control *) object;
	Scrollbar *scrollbar = (Scrollbar *) grip->context;

	if (scrollbar->enabled) {
		if (message->type == OS_MESSAGE_START_DRAG) {
			scrollbar->anchor = scrollbar->orientation ? message->mouseMoved.newPositionY : message->mouseMoved.newPositionX;
		} else if (message->type == OS_MESSAGE_MOUSE_DRAGGED) {
			{
				OSMessage message;
				message.type = OS_MESSAGE_CHILD_UPDATED;
				OSSendMessage(scrollbar, &message);
			}

			int mouse = scrollbar->orientation ? message->mouseDragged.newPositionY : message->mouseDragged.newPositionX;
			scrollbar->position += mouse - scrollbar->anchor;

			if (scrollbar->position < 0) scrollbar->position = 0;
			else if (scrollbar->position >= scrollbar->maxPosition) scrollbar->position = scrollbar->maxPosition;
			else scrollbar->anchor = mouse;

			ScrollbarPositionChanged(scrollbar);
		} else if (message->type == OS_MESSAGE_CUSTOM_PAINT) {
			OSRectangle bounds;

			if (scrollbar->orientation) {
				bounds.left = grip->bounds.left + 6;
				bounds.right = bounds.left + 5;
				bounds.top = (grip->bounds.top + grip->bounds.bottom) / 2 - 4;
				bounds.bottom = bounds.top + 8;
			} else {
				bounds.top = grip->bounds.top + 6;
				bounds.bottom = bounds.top + 5;
				bounds.left = (grip->bounds.left + grip->bounds.right) / 2 - 4;
				bounds.right = bounds.left + 8;
			}

			OSDrawSurfaceClipped(message->paint.surface, OS_SURFACE_UI_SHEET, bounds, 
					scrollbar->orientation ? scrollbarNotchesHorizontal.region : scrollbarNotchesVertical.region,
					scrollbar->orientation ? scrollbarNotchesHorizontal.border : scrollbarNotchesVertical.border,
					OS_DRAW_MODE_REPEAT_FIRST, 0xFF, message->paint.clip);
		} else if (message->type == OS_MESSAGE_LAYOUT) {
			// OSPrint("Layout scrollbar grip, %d, %d, %d, %d\n",
					// message->layout.left, message->layout.right, message->layout.top, message->layout.bottom);
		}
	}

	return OSForwardMessage(object, OS_MAKE_MESSAGE_CALLBACK(ProcessControlMessage, nullptr), message);
}

static OSCallbackResponse ProcessScrollbarMessage(OSObject object, OSMessage *message) {
	Scrollbar *grid = (Scrollbar *) object;
	OSCallbackResponse result = OS_CALLBACK_NOT_HANDLED;

	switch (message->type) {
		case OS_MESSAGE_MEASURE: {
			message->measure.preferredWidth = grid->preferredWidth;
			message->measure.preferredHeight = grid->preferredHeight;
			result = OS_CALLBACK_HANDLED;
		} break;

		case OS_MESSAGE_LAYOUT: {
			if (grid->relayout || message->layout.force) {
				grid->relayout = false;

				grid->bounds = OS_MAKE_RECTANGLE(
						message->layout.left, message->layout.right,
						message->layout.top, message->layout.bottom);

				StandardCellLayout(grid);

				ClipRectangle(message->layout.clip, grid->bounds, &grid->inputBounds);

				if (grid->enabled) {
					OSSetScrollbarMeasurements(grid, grid->contentSize, grid->viewportSize);
				}

				grid->repaint = true;
				SetParentDescendentInvalidationFlags(grid, DESCENDENT_REPAINT);

				// OSPrint("layout scrollbar %x\n", object);

				RelayoutScrollbar(grid);

				result = OS_CALLBACK_HANDLED;
			}
		} break;

		case OS_MESSAGE_DESTROY: {
			OSDestroyCommands(grid->commands);
		} break;

		default: {} break;
	}

	if (result == OS_CALLBACK_NOT_HANDLED) {
		result = OSForwardMessage(object, OS_MAKE_MESSAGE_CALLBACK(ProcessGridMessage, nullptr), message);
	}

	return result;
}

void OSSetScrollbarMeasurements(OSObject _scrollbar, int contentSize, int viewportSize) {
	Scrollbar *scrollbar = (Scrollbar *) _scrollbar;

	// OSPrint("Set scrollbar %x to %dpx in %dpx viewport\n", scrollbar, contentSize, viewportSize);

	if (contentSize <= viewportSize) {
		scrollbar->enabled = false;
		scrollbar->height = 0;
		scrollbar->position = 0;

		OSDisableControl(scrollbar->objects[0], true);
		OSDisableControl(scrollbar->objects[2], true);
		OSDisableControl(scrollbar->objects[3], true);
		OSDisableControl(scrollbar->objects[4], true);
	} else {
		int height = scrollbar->orientation ? (scrollbar->bounds.bottom - scrollbar->bounds.top) : (scrollbar->bounds.right - scrollbar->bounds.left);
		height -= SCROLLBAR_SIZE * 2;

		float fraction = -1;

		if (height != scrollbar->height && scrollbar->height > 0 && scrollbar->automaticallyUpdatePosition) {
			fraction = (float) scrollbar->position / (float) scrollbar->maxPosition;
		}

		scrollbar->enabled = true;
		scrollbar->height = height;
		scrollbar->contentSize = contentSize;
		scrollbar->viewportSize = viewportSize;

		float screens = (float) scrollbar->contentSize / (float) scrollbar->viewportSize;
		scrollbar->size = height / screens;

		if (scrollbar->size < SCROLLBAR_MINIMUM) scrollbar->size = SCROLLBAR_MINIMUM;

		scrollbar->maxPosition = height - scrollbar->size;
		if (fraction != -1) scrollbar->position = fraction * scrollbar->maxPosition;

		if (scrollbar->position < 0) scrollbar->position = 0;
		else if (scrollbar->position >= scrollbar->maxPosition) scrollbar->position = scrollbar->maxPosition;
		else if (scrollbar->position >= 0 && scrollbar->position < scrollbar->maxPosition) {}
		else scrollbar->position = 0;

		OSEnableControl(scrollbar->objects[0], true);
		OSEnableControl(scrollbar->objects[2], true);
		OSEnableControl(scrollbar->objects[3], true);
		OSEnableControl(scrollbar->objects[4], true);
	}

	RepaintControl(scrollbar->objects[0]);
	RepaintControl(scrollbar->objects[1]);
	RepaintControl(scrollbar->objects[2]);
	RepaintControl(scrollbar->objects[3]);
	RepaintControl(scrollbar->objects[4]);

	if (scrollbar->automaticallyUpdatePosition) {
		ScrollbarPositionChanged(scrollbar);
	}
	
	RelayoutScrollbar(scrollbar);
}

OSObject OSCreateScrollbar(bool orientation, bool automaticallyUpdatePosition) {
	uint8_t *memory = (uint8_t *) GUIAllocate(sizeof(Scrollbar) + sizeof(OSObject) * 5, true);

	Scrollbar *scrollbar = (Scrollbar *) memory;
	scrollbar->type = API_OBJECT_GRID;
	OSSetMessageCallback(scrollbar, OS_MAKE_MESSAGE_CALLBACK(ProcessScrollbarMessage, nullptr));

	scrollbar->commands = OSCreateCommands(_osScrollbarCommands);
	OSSetCommandNotificationCallback(scrollbar->commands + _osScrollbarButtonUp, OS_MAKE_NOTIFICATION_CALLBACK(ScrollbarButtonPressed, SCROLLBAR_BUTTON_UP));
	OSSetCommandNotificationCallback(scrollbar->commands + _osScrollbarButtonDown, OS_MAKE_NOTIFICATION_CALLBACK(ScrollbarButtonPressed, SCROLLBAR_BUTTON_DOWN));

	scrollbar->orientation = orientation;
	scrollbar->automaticallyUpdatePosition = automaticallyUpdatePosition;

	scrollbar->columns = 1;
	scrollbar->rows = 5;
	scrollbar->objects = (GUIObject **) (memory + sizeof(Scrollbar));
	scrollbar->preferredWidth = !orientation ? 64 : SCROLLBAR_SIZE;
	scrollbar->preferredHeight = !orientation ? SCROLLBAR_SIZE : 64;

	Control *nudgeUp = (Control *) GUIAllocate(sizeof(Control), true);
	nudgeUp->type = API_OBJECT_CONTROL;
	nudgeUp->context = scrollbar;
	nudgeUp->notificationCallback = OS_MAKE_NOTIFICATION_CALLBACK(ScrollbarButtonPressed, SCROLLBAR_NUDGE_UP);
	nudgeUp->backgrounds = orientation ? scrollbarTrackVerticalBackgrounds : scrollbarTrackHorizontalBackgrounds;
	nudgeUp->useClickRepeat = true;
	OSSetMessageCallback(nudgeUp, OS_MAKE_MESSAGE_CALLBACK(ProcessButtonMessage, nullptr));

	Control *nudgeDown = (Control *) GUIAllocate(sizeof(Control), true);
	nudgeDown->type = API_OBJECT_CONTROL;
	nudgeDown->context = scrollbar;
	nudgeDown->notificationCallback = OS_MAKE_NOTIFICATION_CALLBACK(ScrollbarButtonPressed, SCROLLBAR_NUDGE_DOWN);
	nudgeDown->backgrounds = orientation ? scrollbarTrackVerticalBackgrounds : scrollbarTrackHorizontalBackgrounds;
	nudgeDown->useClickRepeat = true;
	OSSetMessageCallback(nudgeDown, OS_MAKE_MESSAGE_CALLBACK(ProcessButtonMessage, nullptr));

	Control *up = (Control *) OSCreateButton(scrollbar->commands + _osScrollbarButtonUp, OS_BUTTON_STYLE_REPEAT);
	up->backgrounds = scrollbarButtonHorizontalBackgrounds;
	up->context = scrollbar;
	up->icon = orientation ? &smallArrowUpNormal : &smallArrowLeftNormal;
	up->iconHasVariants = true;
	up->centerIcons = true;
	up->focusable = false;

	Control *grip = (Control *) GUIAllocate(sizeof(Control), true);
	grip->type = API_OBJECT_CONTROL;
	grip->context = scrollbar;
	grip->backgrounds = orientation ? scrollbarButtonVerticalBackgrounds : scrollbarButtonHorizontalBackgrounds;
	OSSetMessageCallback(grip, OS_MAKE_MESSAGE_CALLBACK(ProcessScrollbarGripMessage, nullptr));

	Control *down = (Control *) OSCreateButton(scrollbar->commands + _osScrollbarButtonDown, OS_BUTTON_STYLE_REPEAT);
	down->backgrounds = scrollbarButtonHorizontalBackgrounds;
	down->context = scrollbar;
	down->icon = orientation ? &smallArrowDownNormal : &smallArrowRightNormal;
	down->iconHasVariants = true;
	down->centerIcons = true;
	down->focusable = false;

	OSAddControl(scrollbar, 0, 0, up, OS_CELL_H_EXPAND);
	OSAddControl(scrollbar, 0, 1, grip, OS_CELL_V_EXPAND | OS_CELL_H_EXPAND);
	OSAddControl(scrollbar, 0, 2, down, OS_CELL_H_EXPAND);
	OSAddControl(scrollbar, 0, 3, nudgeUp, OS_CELL_V_EXPAND | OS_CELL_H_EXPAND);
	OSAddControl(scrollbar, 0, 4, nudgeDown, OS_CELL_V_EXPAND | OS_CELL_H_EXPAND);

	scrollbar->enabled = false;

	return scrollbar;
}

void OSDisableControl(OSObject _control, bool disabled) {
	OSMessage message;
	message.type = OS_MESSAGE_DISABLE;
	message.disable.disabled = disabled;
	OSSendMessage(_control, &message);
}

void OSSetObjectNotificationCallback(OSObject _object, OSNotificationCallback callback) {
	GUIObject *object = (GUIObject *) _object;
	object->notificationCallback = callback;
}

void OSSetCommandGroupNotificationCallback(OSCommand *commands, OSNotificationCallbackFunction callback) {
	uintptr_t index = 0;

	while (commands) {
		OSCommand *command = commands;
		OSSetCommandNotificationCallback(command, OS_MAKE_NOTIFICATION_CALLBACK(callback, (void *) index));
		if (commands->lastInGroup) break;
		commands++;
		index++;
	}
}

void OSSetCommandNotificationCallback(OSCommand *command, OSNotificationCallback callback) {
	command->notificationCallback = callback;

	LinkedItem<Control> *item = (LinkedItem<Control> *) command->controls;

	while (item) {
		Control *control = item->thisItem;
		control->notificationCallback = callback;
		item = item->nextItem;
	}
}

void OSDisableCommand(OSCommand *command, bool disabled) {
	if (command->disabled == disabled) return;
	command->disabled = disabled;

	LinkedItem<Control> *item = (LinkedItem<Control> *) command->controls;

	while (item) {
		Control *control = item->thisItem;
		OSDisableControl(control, disabled);
		item = item->nextItem;
	}
}

bool OSGetCommandCheck(OSCommand *command) {
	return command->checked;
}

void OSCheckCommand(OSCommand *command, bool checked) {
	OSCommandTemplate *_command = command->specification;

	if (_command->radioCheck) {
		OSCommand *commands = command->startOfCommandGroup;

		while (commands) {
			if (command == commands) {
				goto next;
			}

			if (_command->radioBytes != commands->specification->radioBytes ||
					0 != OSCompareBytes(_command->radio, commands->specification->radio, commands->specification->radioBytes)) {
				goto next;
			}

			{
				OSCommand *command = commands;
				command->checked = false;
				LinkedItem<Control> *item = (LinkedItem<Control> *) command->controls;

				while (item) {
					item->thisItem->isChecked = false;
					RepaintControl(item->thisItem);
					item = item->nextItem;
				}
			}

			next:;
			if (commands->lastInGroup) break;
			commands++;
		}
	} 

	command->checked = checked;
	LinkedItem<Control> *item = (LinkedItem<Control> *) command->controls;

	while (item) {
		item->thisItem->isChecked = checked;
		RepaintControl(item->thisItem);
		item = item->nextItem;
	}
}

static inline int DistanceSquared(int x, int y) {
	return x * x + y * y;
}

static bool CompareKeyboardShortcut(OSCommandTemplate *command, OSMessage *message) {
	bool alt = message->keyboard.alt;
	bool shift = message->keyboard.shift;
	bool ctrl = message->keyboard.ctrl;
	unsigned scancode = message->keyboard.scancode;

	if (scancode == OS_SCANCODE_EQUALS && shift) {
		EnterDebugger();
	}

	size_t shortcutBytes = command->shortcutBytes;

	if (!shortcutBytes) {
		// This command does not have a shortcut.
		return false;
	}

	if (shortcutBytes >= 64) {
		// This command's shortcut is too long.
		return false;
	}

	char shortcut[64];
	OSCopyMemory(shortcut, command->shortcut, command->shortcutBytes);
	shortcut[command->shortcutBytes] = 0;

	for (uintptr_t i = 0; i < command->shortcutBytes; i++) {
		shortcut[i] = tolower(shortcut[i]);
	}

	// Look for prefixes.

	if (strstr(shortcut, "ctrl+")) {
		if (!ctrl) {
			return false;
		}
	} else if (ctrl) {
		return false;
	}

	if (strstr(shortcut, "shift+")) {
		if (!shift) {
			return false;
		}
	} else if (shift) {
		return false;
	}

	if (strstr(shortcut, "alt+")) {
		if (!alt) {
			return false;
		}
	} else if (alt) {
		return false;
	}

	// Then compare the actual scancode.

	char *position = shortcut + command->shortcutBytes;

	while (--position != shortcut) {
		if (*position == '+') {
			position++;
			break;
		}
	}

	const char *expected = nullptr;
	const char *alias = nullptr;

	switch (scancode) {
		case OS_SCANCODE_A:
			expected = "a";
			break;
		case OS_SCANCODE_B:
			expected = "b";
			break;
		case OS_SCANCODE_C:
			expected = "c";
			break;
		case OS_SCANCODE_D:
			expected = "d";
			break;
		case OS_SCANCODE_E:
			expected = "e";
			break;
		case OS_SCANCODE_F:
			expected = "f";
			break;
		case OS_SCANCODE_G:
			expected = "g";
			break;
		case OS_SCANCODE_H:
			expected = "h";
			break;
		case OS_SCANCODE_I:
			expected = "i";
			break;
		case OS_SCANCODE_J:
			expected = "j";
			break;
		case OS_SCANCODE_K:
			expected = "k";
			break;
		case OS_SCANCODE_L:
			expected = "l";
			break;
		case OS_SCANCODE_M:
			expected = "m";
			break;
		case OS_SCANCODE_N:
			expected = "n";
			break;
		case OS_SCANCODE_O:
			expected = "o";
			break;
		case OS_SCANCODE_P:
			expected = "p";
			break;
		case OS_SCANCODE_Q:
			expected = "q";
			break;
		case OS_SCANCODE_R:
			expected = "r";
			break;
		case OS_SCANCODE_S:
			expected = "s";
			break;
		case OS_SCANCODE_T:
			expected = "t";
			break;
		case OS_SCANCODE_U:
			expected = "u";
			break;
		case OS_SCANCODE_V:
			expected = "v";
			break;
		case OS_SCANCODE_W:
			expected = "w";
			break;
		case OS_SCANCODE_X:
			expected = "x";
			break;
		case OS_SCANCODE_Y:
			expected = "y";
			break;
		case OS_SCANCODE_Z:
			expected = "z";
			break;
		case OS_SCANCODE_0:
			expected = "0";
			alias = ")";
			break;
		case OS_SCANCODE_1:
			expected = "1";
			alias = "!";
			break;
		case OS_SCANCODE_2:
			expected = "2";
			alias = "@";
			break;
		case OS_SCANCODE_3:
			expected = "3";
			alias = "#";
			break;
		case OS_SCANCODE_4:
			expected = "4";
			alias = "$";
			break;
		case OS_SCANCODE_5:
			expected = "5";
			alias = "%";
			break;
		case OS_SCANCODE_6:
			expected = "6";
			alias = "^";
			break;
		case OS_SCANCODE_7:
			expected = "7";
			alias = "&";
			break;
		case OS_SCANCODE_8:
			expected = "8";
			alias = "*";
			break;
		case OS_SCANCODE_9:
			expected = "9";
			alias = "(";
			break;
		case OS_SCANCODE_CAPS_LOCK:
			break;
		case OS_SCANCODE_SCROLL_LOCK:
			break;
		case OS_SCANCODE_NUM_LOCK:
			break;
		case OS_SCANCODE_LEFT_SHIFT:
			break;
		case OS_SCANCODE_LEFT_CTRL:
			break;
		case OS_SCANCODE_LEFT_ALT:
			break;
		case OS_SCANCODE_LEFT_FLAG:
			break;
		case OS_SCANCODE_RIGHT_SHIFT:
			break;
		case OS_SCANCODE_RIGHT_CTRL:
			break;
		case OS_SCANCODE_RIGHT_ALT:
			break;
		case OS_SCANCODE_PAUSE:
			expected = "pause";
			break;
		case OS_SCANCODE_CONTEXT_MENU:
			break;
		case OS_SCANCODE_BACKSPACE:
			expected = "backspace";
			alias = "bkspc";
			break;
		case OS_SCANCODE_ESCAPE:
			expected = "escape";
			alias = "esc";
			break;
		case OS_SCANCODE_INSERT:
			expected = "insert";
			alias = "ins";
			break;
		case OS_SCANCODE_HOME:
			expected = "home";
			break;
		case OS_SCANCODE_PAGE_UP:
			expected = "page up";
			alias = "pgup";
			break;
		case OS_SCANCODE_DELETE:
			expected = "delete";
			alias = "del";
			break;
		case OS_SCANCODE_END:
			expected = "end";
			break;
		case OS_SCANCODE_PAGE_DOWN:
			expected = "page down";
			alias = "pgdn";
			break;
		case OS_SCANCODE_UP_ARROW:
			expected = "up";
			break;
		case OS_SCANCODE_LEFT_ARROW:
			expected = "left";
			break;
		case OS_SCANCODE_DOWN_ARROW:
			expected = "down";
			break;
		case OS_SCANCODE_RIGHT_ARROW:
			expected = "right";
			break;
		case OS_SCANCODE_SPACE:
			expected = "space";
			break;
		case OS_SCANCODE_TAB:
			expected = "tab";
			break;
		case OS_SCANCODE_ENTER:
			expected = "enter";
			alias = "return";
			break;
		case OS_SCANCODE_SLASH:
			expected = "/";
			alias = "?";
			break;
		case OS_SCANCODE_BACKSLASH:
			expected = "\\";
			alias = "|";
			break;
		case OS_SCANCODE_LEFT_BRACE:
			expected = "[";
			alias = "{";
			break;
		case OS_SCANCODE_RIGHT_BRACE:
			expected = "]";
			alias = "}";
			break;
		case OS_SCANCODE_EQUALS:
			expected = "=";
			alias = "+";
			break;
		case OS_SCANCODE_BACKTICK:
			expected = "`";
			alias = "~";
			break;
		case OS_SCANCODE_HYPHEN:
			expected = "-";
			alias = "_";
			break;
		case OS_SCANCODE_SEMICOLON:
			expected = ";";
			alias = ":";
			break;
		case OS_SCANCODE_QUOTE:
			expected = "'";
			alias = "\"";
			break;
		case OS_SCANCODE_COMMA:
			expected = ",";
			alias = "<";
			break;
		case OS_SCANCODE_PERIOD:
			expected = ".";
			alias = ">";
			break;
		case OS_SCANCODE_NUM_DIVIDE:
			break;
		case OS_SCANCODE_NUM_MULTIPLY:
			break;
		case OS_SCANCODE_NUM_SUBTRACT:
			break;
		case OS_SCANCODE_NUM_ADD:
			break;
		case OS_SCANCODE_NUM_ENTER:
			break;
		case OS_SCANCODE_NUM_POINT:
			break;
		case OS_SCANCODE_NUM_0:
			break;
		case OS_SCANCODE_NUM_1:
			break;
		case OS_SCANCODE_NUM_2:
			break;
		case OS_SCANCODE_NUM_3:
			break;
		case OS_SCANCODE_NUM_4:
			break;
		case OS_SCANCODE_NUM_5:
			break;
		case OS_SCANCODE_NUM_6:
			break;
		case OS_SCANCODE_NUM_7:
			break;
		case OS_SCANCODE_NUM_8:
			break;
		case OS_SCANCODE_NUM_9:
			break;
		case OS_SCANCODE_PRINT_SCREEN_1:
			break;
		case OS_SCANCODE_PRINT_SCREEN_2:
			break;
		case OS_SCANCODE_F1:
			expected = "f1";
			break;
		case OS_SCANCODE_F2:
			expected = "f2";
			break;
		case OS_SCANCODE_F3:
			expected = "f3";
			break;
		case OS_SCANCODE_F4:
			expected = "f4";
			break;
		case OS_SCANCODE_F5:
			expected = "f5";
			break;
		case OS_SCANCODE_F6:
			expected = "f6";
			break;
		case OS_SCANCODE_F7:
			expected = "f7";
			break;
		case OS_SCANCODE_F8:
			expected = "f8";
			break;
		case OS_SCANCODE_F9:
			expected = "f9";
			break;
		case OS_SCANCODE_F10:
			expected = "f10";
			break;
		case OS_SCANCODE_F11:
			expected = "f11";
			break;
		case OS_SCANCODE_F12:
			expected = "f12";
			break;
		case OS_SCANCODE_ACPI_POWER:
			break;
		case OS_SCANCODE_ACPI_SLEEP:
			break;
		case OS_SCANCODE_ACPI_WAKE:
			break;
		case OS_SCANCODE_MM_NEXT:
			break;
		case OS_SCANCODE_MM_PREVIOUS:
			break;
		case OS_SCANCODE_MM_STOP:
			break;
		case OS_SCANCODE_MM_PAUSE:
			break;
		case OS_SCANCODE_MM_MUTE:
			break;
		case OS_SCANCODE_MM_QUIETER:
			break;
		case OS_SCANCODE_MM_LOUDER:
			break;
		case OS_SCANCODE_MM_SELECT:
			break;
		case OS_SCANCODE_MM_EMAIL:
			break;
		case OS_SCANCODE_MM_CALC:
			break;
		case OS_SCANCODE_MM_FILES:
			break;
		case OS_SCANCODE_WWW_SEARCH:
			break;
		case OS_SCANCODE_WWW_HOME:
			break;
		case OS_SCANCODE_WWW_BACK:
			break;
		case OS_SCANCODE_WWW_FORWARD:
			break;
		case OS_SCANCODE_WWW_STOP:
			break;
		case OS_SCANCODE_WWW_REFRESH:
			break;
		case OS_SCANCODE_WWW_STARRED:
			break;
	}

	if (expected && 0 == strcmp(position, expected)) {
		// The strings matched.
		return true;
	}

	if (alias && 0 == strcmp(position, alias)) {
		// The strings matched.
		return true;
	}

	return false;
}

static bool DoKeyboardShortcutsForCommandGroup(OSCommand *commands, OSMessage *message, OSInstance *instance) {
	while (commands) {
		if (CompareKeyboardShortcut(commands->specification, message)) {
			IssueCommand(nullptr, commands, instance);
			return true;
		}

		if (commands->lastInGroup) break;
		else commands++;
	}

	return false;
}

static OSCallbackResponse ProcessWindowMessage(OSObject _object, OSMessage *message) {
	OSCallbackResponse response = OS_CALLBACK_HANDLED;
	Window *window = (Window *) _object;
	window->willUpdateAfterMessageProcessing = true;

	static int lastClickX = 0, lastClickY = 0;
	static bool draggingStarted = false;
	bool updateWindow = false;
	bool rightClick = false;

	if (window->destroyed && message->type != OS_MESSAGE_WINDOW_DESTROYED) {
		return OS_CALLBACK_NOT_HANDLED;
	}

	switch (message->type) {
		case OS_MESSAGE_WINDOW_CREATED: {
			window->created = true;
			updateWindow = true;
		} break;

		case OS_MESSAGE_WINDOW_ACTIVATED: {
			if (!window->activated) {
				for (int i = 0; i < 12; i++) {
					if (i == 7 || (window->flags & OS_CREATE_WINDOW_MENU)) continue;
					OSDisableControl(window->root->objects[i], false);
				}

				if (window->lastFocus) {
					OSMessage message;
					message.type = OS_MESSAGE_CLIPBOARD_UPDATED;
					OSSyscall(OS_SYSCALL_GET_CLIPBOARD_HEADER, 0, (uintptr_t) &message.clipboard, 0, 0);
					OSSendMessage(window->lastFocus, &message);
				}

				OSSetFocusedControl(window->defaultFocus, false);

				window->activated = true;
			}
		} break;

		case OS_MESSAGE_WINDOW_DEACTIVATED: {
			for (int i = 0; i < 12; i++) {
				if (i == 7 || (window->flags & OS_CREATE_WINDOW_MENU)) continue;
				OSDisableControl(window->root->objects[i], true);
			}

			OSRemoveFocusedControl(window, true);

			if (window->flags & OS_CREATE_WINDOW_MENU) {
				message->type = OS_MESSAGE_DESTROY;
				OSSendMessage(window, message);
			}

			if (openMenuCount && openMenus[0].window == window) {
				navigateMenuMode = false;
			}

			window->activated = false;
		} break;

		case OS_MESSAGE_WINDOW_DESTROYED: {
			OSNotification n;
			n.type = OS_NOTIFICATION_WINDOW_CLOSE;
			OSSendNotification(window, window->notificationCallback, &n, window->instance);

			if (window->dialogCommands) OSDestroyCommands(window->dialogCommands);

			if (window->temporaryInstance) {
				OSDestroyInstance(window->instance);
			}

			window->windowItem.RemoveFromList();
			GUIFree(window->baseWindowTitle.buffer);
			GUIFree(window);
			return response;
		} break;

		case OS_MESSAGE_KEY_RELEASED: {
			if (navigateMenuMode) {
				if (message->keyboard.scancode == OS_SCANCODE_ENTER && navigateMenuItem) {
					window->pressed = nullptr;
					navigateMenuItem->pressedByKeyboard = false;
					OSAnimateControl(navigateMenuItem, false);
					IssueCommand(navigateMenuItem);

					OSMessage m;
					m.type = OS_MESSAGE_DESTROY;
					OSSendMessage(openMenus[0].window, &m);
				}
			} else {
				GUIObject *control = window->lastFocus;
				OSCallbackResponse response = OSSendMessage(control, message);

				if (response == OS_CALLBACK_NOT_HANDLED && message->keyboard.scancode == OS_SCANCODE_LEFT_ALT) {
					if (window->flags & OS_CREATE_WINDOW_WITH_MENUBAR) {
						navigateMenuUsedKey = true;
						MenuItem *control = (MenuItem *) ((Grid *) ((Grid *) window->root->objects[7])->objects[0])->objects[0];
						OSCreateMenu((OSMenuTemplate *) control->item.value, control, 
								OS_CREATE_MENU_AT_SOURCE, OS_CREATE_MENU_FROM_MENUBAR,
								window->instance);
					}
				}
			}
		} break;

		case OS_MESSAGE_KEY_PRESSED: {
			if (message->keyboard.scancode == OS_SCANCODE_F2 && message->keyboard.alt) {
				EnterDebugger();
			}

			if (navigateMenuMode) {
				if (navigateMenuItem) {
					RepaintControl(navigateMenuItem);
				}
				
				navigateMenuUsedKey = true;

				if (message->keyboard.scancode == OS_SCANCODE_ESCAPE) {
					if (window->hasMenuParent) {
						OSMessage m;
						m.type = OS_MESSAGE_DESTROY;
						OSSendMessage(window, &m);
					} 

					if (openMenuCount <= 1) {
						navigateMenuMode = false;
					}
				} else if (message->keyboard.scancode == OS_SCANCODE_LEFT_ARROW) {
					if (openMenuCount == 1) {
						if (((GUIObject *) openMenus[0].source->parent)->isMenubar) {
							Grid *parent = (Grid *) openMenus[0].source->parent;
							int i = FindObjectInGrid(parent, openMenus[0].source) - 1;

							if (i == -1) {
								i += parent->columns;
							}

							MenuItem *control = (MenuItem *) parent->objects[i];
							OSCreateMenu((OSMenuTemplate *) control->item.value, control, 
									OS_CREATE_MENU_AT_SOURCE, OS_CREATE_MENU_FROM_MENUBAR,
									window->instance);
						}
					} else {
						OSMessage m;
						m.type = OS_MESSAGE_DESTROY;
						OSSendMessage(window, &m);
					}
				} else if (message->keyboard.scancode == OS_SCANCODE_RIGHT_ARROW) {
					if (navigateMenuItem && navigateMenuItem->item.type == OSMenuItem_SUBMENU) {
						OSCreateMenu((OSMenuTemplate *) navigateMenuItem->item.value, navigateMenuItem, 
								OS_CREATE_MENU_AT_SOURCE, OS_CREATE_SUBMENU, window->instance);
					} else if (openMenuCount == 1 && ((GUIObject *) openMenus[0].source->parent)->isMenubar) {
						Grid *parent = (Grid *) openMenus[0].source->parent;
						int i = FindObjectInGrid(parent, openMenus[0].source) + 1;

						if (i == (int) parent->columns) {
							i -= parent->columns;
						}

						MenuItem *control = (MenuItem *) parent->objects[i];
						OSCreateMenu((OSMenuTemplate *) control->item.value, control, 
								OS_CREATE_MENU_AT_SOURCE, OS_CREATE_MENU_FROM_MENUBAR, window->instance);
					}
				} else if (message->keyboard.scancode == OS_SCANCODE_UP_ARROW) {
					if (!navigateMenuItem) {
						navigateMenuItem = navigateMenuFirstItem;
					}

					Grid *parent = (Grid *) navigateMenuItem->parent;

					do {
						int i = FindObjectInGrid(parent, navigateMenuItem) - 1;

						if (i == -1) {
							i += parent->rows;
						}

						navigateMenuItem = (MenuItem *) parent->objects[i];
					} while (!navigateMenuItem->tabStop);
				} else if (message->keyboard.scancode == OS_SCANCODE_DOWN_ARROW) {
					bool f = false;

					if (!navigateMenuItem) {
						navigateMenuItem = navigateMenuFirstItem;
						f = true;
					}

					Grid *parent = (Grid *) navigateMenuItem->parent;

					do {
						int i = FindObjectInGrid(parent, navigateMenuItem) + (f ? 0 : 1);

						if (i == (int) parent->rows) {
							i -= parent->rows;
						}

						navigateMenuItem = (MenuItem *) parent->objects[i];
					} while (!navigateMenuItem->tabStop);
				} else if (message->keyboard.scancode == OS_SCANCODE_ENTER && navigateMenuItem) {
					window->pressed = navigateMenuItem;
					navigateMenuItem->pressedByKeyboard = true;
					OSAnimateControl(navigateMenuItem, true);
				}

				if (navigateMenuItem) {
					RepaintControl(navigateMenuItem);
				}

				break;
			}
			
			if (message->keyboard.scancode == OS_SCANCODE_F4 && message->keyboard.alt) {
				if (window->flags & OS_CREATE_WINDOW_DIALOG) {
					IssueCommand(nullptr, window->dialogCommands + osDialogStandardCancel, window->instance);
				} else {
					OSMessage m;
					m.type = OS_MESSAGE_DESTROY;
					OSSendMessage(window, &m);
				}
			} else {
				OSCallbackResponse response = OS_CALLBACK_NOT_HANDLED;

				if (window->focus) {
					message->type = OS_MESSAGE_KEY_TYPED;
					response = OSSendMessage(window->focus, message);
				}

				GUIObject *control = window->lastFocus;
				message->keyboard.notHandledBy = nullptr;
				message->type = OS_MESSAGE_KEY_PRESSED;

				while (control && control != window && response == OS_CALLBACK_NOT_HANDLED) {
					response = OSSendMessage(control, message);
					message->keyboard.notHandledBy = control;
					control = (GUIObject *) control->parent;
				}

				if (response == OS_CALLBACK_NOT_HANDLED) {
					if (!DoKeyboardShortcutsForCommandGroup(window->instance->customCommands, message, window->instance)) {
						if (!DoKeyboardShortcutsForCommandGroup(window->instance->builtinCommands, message, window->instance)) {
							if (!DoKeyboardShortcutsForCommandGroup(window->dialogCommands, message, window->instance)) {
							} else response = OS_CALLBACK_HANDLED;
						} else response = OS_CALLBACK_HANDLED;
					} else response = OS_CALLBACK_HANDLED;
				}

				if (response == OS_CALLBACK_NOT_HANDLED) {
					if (message->keyboard.scancode == OS_SCANCODE_TAB 
							&& !message->keyboard.ctrl && !message->keyboard.alt) {
						message->keyboard.notHandledBy = nullptr;
						OSSendMessage(window->root, message);
					} else if (message->keyboard.scancode == OS_SCANCODE_ESCAPE) {
						if (window->flags & OS_CREATE_WINDOW_DIALOG) {
							IssueCommand(nullptr, window->dialogCommands + osDialogStandardCancel, window->instance);
						} else if (window->hasMenuParent) {
							OSMessage m;
							m.type = OS_MESSAGE_DESTROY;
							OSSendMessage(window, &m);
						} else {
							OSRemoveFocusedControl(window, true);
						}
					} else if (message->keyboard.scancode == OS_SCANCODE_ENTER
							&& !message->keyboard.ctrl && !message->keyboard.alt && !message->keyboard.shift
							&& window->buttonFocus) {
						IssueCommand(window->buttonFocus);
					}

					// TODO Access keys.
				}
			}
		} break;

		case OS_MESSAGE_DESTROY: {
			for (uintptr_t i = 0; i < openMenuCount; i++) {
				if (openMenus[i].window == window) {
					if (i + 1 != openMenuCount) OSSendMessage(openMenus[i + 1].window, message);
					
					if (openMenus[i].source) {
						OSAnimateControl(openMenus[i].source, true);

						if (i >= 1) {
							navigateMenuItem = (MenuItem *) openMenus[i].source;
						} else {
							navigateMenuItem = nullptr;
							navigateMenuMode = false;
							navigateMenuUsedKey = false;
						}
					}

					openMenuCount = i;
					break;
				}
			}

			OSSendMessage(window->root, message);

			if (!(window->flags & OS_CREATE_WINDOW_HEADLESS)) {
				OSCloseHandle(window->surface);
				OSCloseHandle(window->window);
			} else {
				// TODO Destroy the window?
			} 

			if (window->instance->window == window) {
				window->instance->window = nullptr;
			}

			window->destroyed = true;
		} break;

		case OS_MESSAGE_CLICK_REPEAT: {
			if (window->pressed) {
				OSSendMessage(window->pressed, message);

				if (window->pressed == window->hover && window->pressed->useClickRepeat) {
					OSMessage clicked;
					clicked.type = OS_MESSAGE_CLICKED;
					OSSendMessage(window->pressed, &clicked);
				}
			} else if (window->hoverGrid) {
				OSSendMessage(window->hoverGrid, message);
			}
		} break;

		case OS_MESSAGE_MOUSE_RIGHT_PRESSED:
			rightClick = true;
			// Fallthrough
		case OS_MESSAGE_MOUSE_LEFT_PRESSED: {
			bool allowFocus = false;

			// If there is a control we're hovering over that isn't disabled,
			// and this either isn't an window activation click or the control is fine with activation clicks:
			if (window->hover && !window->hover->disabled
					&& (!message->mousePressed.activationClick || !window->hover->ignoreActivationClicks)) {
				// Send the raw WM message to the control.
				OSSendMessage(window->hover, message);

				// This control can get the focus from this message.
				allowFocus = true;

				// Press the control.
				window->pressed = window->hover;

				draggingStarted = false;
				lastClickX = message->mousePressed.positionX;
				lastClickY = message->mousePressed.positionY;

				OSMessage m = *message;
				m.type = OS_MESSAGE_START_PRESS;
				OSSendMessage(window->pressed, &m);

				if (!rightClick && window->hover->useClickRepeat) {
					OSMessage clicked;
					clicked.type = OS_MESSAGE_CLICKED;
					OSSendMessage(window->pressed, &clicked);
				}
			}

			// If there isn't a control we're hovering over,
			// but we are hovering over a grid,
			// send it the raw message.
			if (!window->hover && window->hoverGrid) {
				OSSendMessage(window->hoverGrid, message);
			}

			// If a different control had focus, it must lose it.
			if (window->focus != window->hover) {
				OSRemoveFocusedControl(window, false);
			}

			// If this control should be given focus...
			if (allowFocus) {
				Control *control = window->hover;

				// And it is focusable...
				if (control->focusable) {
					OSSetFocusedControl(control, false);
				}
			}

			OSMessage m = *message;
			m.type = OS_MESSAGE_MOUSE_MOVED;
			m.mouseMoved.oldPositionX = message->mousePressed.positionX;
			m.mouseMoved.oldPositionY = message->mousePressed.positionY;
			m.mouseMoved.newPositionX = message->mousePressed.positionX;
			m.mouseMoved.newPositionY = message->mousePressed.positionY;
			m.mouseMoved.newPositionXScreen = message->mousePressed.positionXScreen;
			m.mouseMoved.newPositionYScreen = message->mousePressed.positionYScreen;
			OSSendMessage(window->root, &m);
		} break;

		case OS_MESSAGE_MOUSE_MIDDLE_RELEASED:
		case OS_MESSAGE_MOUSE_RIGHT_RELEASED:
			rightClick = true; // Fallthrough.
		case OS_MESSAGE_MOUSE_LEFT_RELEASED: {
			if (window->pressed) {
				// Send the raw message.
				OSSendMessage(window->pressed, message);

				RepaintControl(window->pressed);

				if (!rightClick) {
					if (window->pressed == window->hover && !window->pressed->useClickRepeat) {
						OSMessage clicked;
						clicked.type = OS_MESSAGE_CLICKED;
						OSSendMessage(window->pressed, &clicked);
					}
				}

				OSMessage message;
				message.type = OS_MESSAGE_END_PRESS;
				OSSendMessage(window->pressed, &message);
				window->pressed = nullptr;
			}

			OSMessage m = *message;
			m.type = OS_MESSAGE_MOUSE_MOVED;
			m.mouseMoved.oldPositionX = message->mousePressed.positionX;
			m.mouseMoved.oldPositionY = message->mousePressed.positionY;
			m.mouseMoved.newPositionX = message->mousePressed.positionX;
			m.mouseMoved.newPositionY = message->mousePressed.positionY;
			m.mouseMoved.newPositionXScreen = message->mousePressed.positionXScreen;
			m.mouseMoved.newPositionYScreen = message->mousePressed.positionYScreen;
			OSSendMessage(window->root, &m);
		} break;

		case OS_MESSAGE_MOUSE_EXIT: {
			if (window->hover) {
				OSMessage message;
				message.type = OS_MESSAGE_END_HOVER;
				OSSendMessage(window->hover, &message);
				window->hover = nullptr;
			}
		} break;

		case OS_MESSAGE_MOUSE_MOVED: {
			if (window->pressed) {
				OSMessage hitTest;
				hitTest.type = OS_MESSAGE_HIT_TEST;
				hitTest.hitTest.positionX = message->mouseMoved.newPositionX;
				hitTest.hitTest.positionY = message->mouseMoved.newPositionY;
				OSCallbackResponse response = OSSendMessage(window->pressed, &hitTest);

				if (response == OS_CALLBACK_HANDLED && !hitTest.hitTest.result && window->hover) {
					OSMessage message;
					message.type = OS_MESSAGE_END_HOVER;
					OSSendMessage(window->hover, &message);
					window->hover = nullptr;
				} else if (response == OS_CALLBACK_HANDLED && hitTest.hitTest.result && !window->hover) {
					OSMessage message;
					message.type = OS_MESSAGE_START_HOVER;
					window->hover = window->pressed;
					OSSendMessage(window->hover, &message);
				}

				if (draggingStarted || DistanceSquared(message->mouseMoved.newPositionX, message->mouseMoved.newPositionY) >= 4) {
					message->mouseDragged.originalPositionX = lastClickX;
					message->mouseDragged.originalPositionY = lastClickY;

					if (!draggingStarted) {
						draggingStarted = true;
						message->type = OS_MESSAGE_START_DRAG;
						OSSendMessage(window->pressed, message);
					}

					message->type = OS_MESSAGE_MOUSE_DRAGGED;
					OSSendMessage(window->pressed, message);
				}
			} else {
				Control *old = window->hover;

				OSMessage hitTest;
				hitTest.type = OS_MESSAGE_HIT_TEST;
				hitTest.hitTest.positionX = message->mouseMoved.newPositionX;
				hitTest.hitTest.positionY = message->mouseMoved.newPositionY;
				OSCallbackResponse response = OSSendMessage(old, &hitTest);

				if (!hitTest.hitTest.result || response == OS_CALLBACK_NOT_HANDLED) {
					window->hover = nullptr;
					OSSendMessage(window->root, message);
				} else {
					OSSendMessage(old, message);
				}

				if (window->hover != old && old) {
					OSMessage message;
					message.type = OS_MESSAGE_END_HOVER;
					OSSendMessage(old, &message);
				}
			}
		} break;

		case OS_MESSAGE_WM_TIMER: {
			LinkedItem<Control> *item = window->timerControls.firstItem;

			while (item) {
				int ticksPerMessage = window->timerHz / item->thisItem->timerHz;
				item->thisItem->timerStep++;

				if (item->thisItem->timerStep >= ticksPerMessage) {
					item->thisItem->timerStep = 0;
					OSSendMessage(item->thisItem, message);
				}

				item = item->nextItem;
			}

			if (window->lastFocus) {
				window->caretBlinkStep++;

				if (window->caretBlinkStep >= window->timerHz / CARET_BLINK_HZ) {
					window->caretBlinkStep = 0;

					OSMessage message;
					message.type = OS_MESSAGE_CARET_BLINK;
					OSSendMessage(window->lastFocus, &message);
				}
			}
		} break;

		case OS_MESSAGE_CLIPBOARD_UPDATED: {
			if (window->lastFocus) {
				OSSendMessage(window->lastFocus, message);
			}
		} break;

		default: {
			response = OS_CALLBACK_NOT_HANDLED;
		} break;
	}

	if (window->destroyed) {
		return response;
	}

	if (!(window->flags & OS_CREATE_WINDOW_HEADLESS)) {
		// TODO Only do this if the list or focus has changed.

		LinkedItem<Control> *item = window->timerControls.firstItem;
		int largestHz = window->focus ? CARET_BLINK_HZ : 0;

		while (item) {
			int thisHz = item->thisItem->timerHz;
			if (thisHz > largestHz) largestHz = thisHz;
			item = item->nextItem;
		}

		if (window->timerHz != largestHz) {
			window->timerHz = largestHz;
			OSSyscall(OS_SYSCALL_NEED_WM_TIMER, window->window, largestHz, 0, 0);
		}

		if (window->descendentInvalidationFlags & DESCENDENT_RELAYOUT) {
			window->descendentInvalidationFlags &= ~DESCENDENT_RELAYOUT;

			OSMessage message;
			message.type = OS_MESSAGE_LAYOUT;
			message.layout.clip = OS_MAKE_RECTANGLE(0, window->width, 0, window->height);
			message.layout.left = 0;
			message.layout.top = 0;
			message.layout.right = window->width;
			message.layout.bottom = window->height;
			message.layout.force = false;
			OSSendMessage(window->root, &message);
		}

		window->cursor = OS_CURSOR_NORMAL;

		if (window->hover) {
			Control *control = window->hover;

			OSMessage m;
			m.type = OS_MESSAGE_GET_CURSOR;

			if (OS_CALLBACK_HANDLED == OSSendMessage(control, &m)) {
				window->cursor = m.getCursor.cursor;
			} else if (!control->disabled || control->keepCustomCursorWhenDisabled) {
				window->cursor = (OSCursorStyle) control->cursor;
			} else {
				window->cursor = OS_CURSOR_NORMAL;
			}
		}

		if (window->cursor != window->cursorOld) {
			OSSyscall(OS_SYSCALL_SET_CURSOR_STYLE, window->window, window->cursor, 0, 0);
			window->cursorOld = window->cursor;
			updateWindow = true;
		}

		if (window->descendentInvalidationFlags & DESCENDENT_REPAINT) {
			window->descendentInvalidationFlags &= ~DESCENDENT_REPAINT;
			
#ifdef MERGE_REPAINTS
			if (!window->repaintItem.list) {
				repaintWindows.InsertEnd(&window->repaintItem);
			}
#else
			OSMessage message;
			message.type = OS_MESSAGE_PAINT;
			message.paint.clip = OS_MAKE_RECTANGLE(0, window->width, 0, window->height);
			message.paint.surface = window->surface;
			message.paint.force = false;
#ifdef VISUALISE_REPAINTS
			OSFillRectangle(window->surface, message.paint.clip, OSColor(255, 0, 255));
#endif
			OSSendMessage(window->root, &message);
			updateWindow = true;
#endif
		}

		if (updateWindow) {
			OSSyscall(OS_SYSCALL_UPDATE_WINDOW, window->window, 0, 0, 0);
		}
	}

	window->willUpdateAfterMessageProcessing = false;

	return response;
}

void OSDestroyCommands(OSCommand *commands) {
	OSMessage m;
	m.type = OS_MESSAGE_DESTROY_COMMANDS;
	m.argument = commands;
	OSPostMessage(&m);
}

OSCommand *OSGetBuiltinCommands(OSObject instance) {
	return ((OSInstance *) instance)->builtinCommands;
}

OSCommand *OSCreateCommands(OSCommandGroup group) {
	OSCommand *commands = (OSCommand *) GUIAllocate(sizeof(OSCommand) * group.commandCount, true);

	for (uintptr_t i = 0; i < group.commandCount; i++) {
		OSCommand *command = commands + i;
		OSCommandTemplate *specification = group.commands[i];
		command->disabled = specification->defaultDisabled;
		command->checked = specification->defaultCheck;
		command->notificationCallback = specification->callback;
		command->specification = specification;
		command->startOfCommandGroup = commands;
		if (i == group.commandCount - 1) command->lastInGroup = true;
	}

	return commands;
}

OSString OSGetInstanceData(OSObject _instance) {
	OSInstance *instance = (OSInstance *) _instance;
	OSString string;
	string.buffer = (char *) instance->data;
	string.bytes = instance->dataBytes;
	return string;
}

void OSMarkInstanceModified(OSObject _instance) {
	OSInstance *instance = (OSInstance *) _instance;
	instance->modified = true;
	OSEnableCommand(instance->builtinCommands + osCommandSaveFile, true);
}

OSObject OSCreateInstance(void *instanceContext, OSMessage *message, OSCommand *customCommands) {
	OSInstance *instance = (OSInstance *) OSHeapAllocate(sizeof(OSInstance), true);
	instance->type = API_OBJECT_INSTANCE;

	instance->argument = instanceContext;
	instance->foreign = false;
	instance->handle = message ? message->createInstance.instanceHandle : OS_INVALID_HANDLE;
	instance->headless = message ? message->createInstance.headless : false;
	instance->builtinCommands = OSCreateCommands(osBuiltinCommands);
	instance->customCommands = customCommands;

	if (instance->handle) {
		OSSyscall(OS_SYSCALL_ATTACH_INSTANCE_TO_PROCESS, instance->handle, (uintptr_t) instance, 0, 0);
	}

	if (message && message->createInstance.dataBytes) {
		instance->data = OSHeapAllocate(message->createInstance.dataBytes, false);
		instance->dataBytes = message->createInstance.dataBytes;
		OSReadConstantBuffer(message->createInstance.dataBuffer, instance->data);
		OSCloseHandle(message->createInstance.dataBuffer);
	}

	return instance;
}

void OSDestroyInstance(OSObject _instance) {
	OSMessage m;
	m.type = OS_MESSAGE_DESTROY_INSTANCE;
	m.argument = _instance;
	OSPostMessage(&m);
}

OSCallbackResponse _InstanceDiscardThenNewFile(OSNotification *notification) {
	if (notification->type != OS_NOTIFICATION_COMMAND) return OS_CALLBACK_NOT_HANDLED;
	OSCloseWindow(((OSInstance *) notification->instance)->fileDialog);
	((OSInstance *) notification->instance)->modified = false;
	return _InstanceNewFile(notification);
}

OSCallbackResponse _InstanceNewFile(OSNotification *notification) {
	if (notification->type != OS_NOTIFICATION_COMMAND) return OS_CALLBACK_NOT_HANDLED;

	OSInstance *instance = (OSInstance *) notification->instance;

	if (instance->modified) {
		instance->fileDialog = OSShowDialogConfirm(OSLiteral("Unsaved Changes"),
				OSLiteral("The file has unsaved changes."),
				OSLiteral("Any unsaved changes will be permanently lost."),
				instance,
				OS_ICON_WARNING, instance->window,
				instance->builtinCommands + _osCommandSaveThenNew, instance->builtinCommands + _osCommandDiscardThenNew);
	} else {
		OSNotification n;
		n.type = OS_NOTIFICATION_NEW_FILE;
		OSSendNotification(notification->generator, instance->notificationCallback, &n, instance);

		CreateString(nullptr, 0, &instance->filename);
		OSDisableCommand(instance->builtinCommands + osCommandBackupFile, true);
		OSDisableCommand(instance->builtinCommands + osCommandSaveFile, true);
		OSSetWindowTitle(instance->window, OSLiteral("Untitled"));
	}

	return OS_CALLBACK_HANDLED;
}

OSCallbackResponse _InstanceDiscardThenOpenFile(OSNotification *notification) {
	if (notification->type != OS_NOTIFICATION_COMMAND) return OS_CALLBACK_NOT_HANDLED;
	OSCloseWindow(((OSInstance *) notification->instance)->fileDialog);
	((OSInstance *) notification->instance)->modified = false;
	return _InstanceOpenFile(notification);
}

void OSOpenFile(OSObject instance, char *path, size_t pathBytes) {
	OSNotification n;
	n.type = OS_NOTIFICATION_FILE_DIALOG;
	n.fileDialog.path = path;
	n.fileDialog.pathBytes = pathBytes;
	n.fileDialog.cancelled = false;
	n.instance = instance;
	_InstanceOpenFile(&n);
}

OSCallbackResponse _InstanceOpenFile(OSNotification *notification) {
	OSInstance *instance = (OSInstance *) notification->instance;

	if (notification->type == OS_NOTIFICATION_FILE_DIALOG) {
		if (notification->fileDialog.cancelled) {
			return OS_CALLBACK_HANDLED;
		}

		OSNotification n = *notification;
		n.type = OS_NOTIFICATION_OPEN_FILE;
		n.fileDialog.error = OS_ERROR_UNKNOWN_OPERATION_FAILURE;

		if (OS_CALLBACK_REJECTED == OSSendNotification(notification->generator, instance->notificationCallback, &n, instance)) {
			OSShowDialogAlert(OSLiteral("Error"),
					OSLiteral("The file could not be opened"),
					OSLiteral("TODO: Error messages."),
					notification->instance,
					OS_ICON_ERROR, instance->window);
		} else {
			CreateString(n.fileDialog.path, n.fileDialog.pathBytes, &instance->filename);
			OSEnableCommand(instance->builtinCommands + osCommandBackupFile, true);
			OSDisableCommand(instance->builtinCommands + osCommandSaveFile, true);
			OSSetWindowTitle(instance->window, n.fileDialog.path, n.fileDialog.pathBytes);
		}

		return OS_CALLBACK_HANDLED;
	} else if (notification->type == OS_NOTIFICATION_COMMAND) {
		if (instance->modified) {
			instance->fileDialog = OSShowDialogConfirm(OSLiteral("Unsaved Changes"),
					OSLiteral("The file has unsaved changes."),
					OSLiteral("Any unsaved changes will be permanently lost."),
					instance,
					OS_ICON_WARNING, instance->window,
					instance->builtinCommands + _osCommandSaveThenOpen, instance->builtinCommands + _osCommandDiscardThenOpen);
		} else {
			OSShowOpenFileDialog(instance, OS_MAKE_NOTIFICATION_CALLBACK(_InstanceOpenFile, nullptr), instance->window); 
		}

		return OS_CALLBACK_HANDLED;
	} else {
		return OS_CALLBACK_NOT_HANDLED;
	}
}

OSCallbackResponse _InstanceSaveFile(OSNotification *notification) {
	OSInstance *instance = (OSInstance *) notification->instance;

	if (notification->type == OS_NOTIFICATION_FILE_DIALOG) {
		if (notification->fileDialog.cancelled) {
			return OS_CALLBACK_HANDLED;
		}

		OSNotification n = *notification;
		n.type = OS_NOTIFICATION_SAVE_FILE;
		n.fileDialog.error = OS_ERROR_UNKNOWN_OPERATION_FAILURE;

		if (OS_CALLBACK_REJECTED == OSSendNotification(notification->generator, instance->notificationCallback, &n, instance)) {
			OSShowDialogAlert(OSLiteral("Error"),
					OSLiteral("The file could not be saved."),
					OSLiteral("TODO: Error messages."),
					notification->instance,
					OS_ICON_ERROR, instance->window);
		} else {
			if ((uintptr_t) notification->context != osCommandSaveCopyFile) {
				instance->modified = false;
				CreateString(n.fileDialog.path, n.fileDialog.pathBytes, &instance->filename);
				OSEnableCommand(instance->builtinCommands + osCommandBackupFile, true);
				OSDisableCommand(instance->builtinCommands + osCommandSaveFile, true);
				OSSetWindowTitle(instance->window, n.fileDialog.path, n.fileDialog.pathBytes);
			}

			if ((uintptr_t) notification->context == _osCommandSaveThenNew) {
				n.type = OS_NOTIFICATION_COMMAND;
				_InstanceNewFile(&n);
			} else if ((uintptr_t) notification->context == _osCommandSaveThenOpen) {
				n.type = OS_NOTIFICATION_COMMAND;
				_InstanceOpenFile(&n);
			}
		}

		return OS_CALLBACK_HANDLED;
	} else if (notification->type == OS_NOTIFICATION_COMMAND) {
		if ((uintptr_t) notification->context == _osCommandSaveThenNew
				|| (uintptr_t) notification->context == _osCommandSaveThenOpen) {
			OSCloseWindow(instance->fileDialog);
		}

		if (!instance->filename.bytes || (uintptr_t) notification->context == osCommandSaveAsFile || (uintptr_t) notification->context == osCommandSaveCopyFile) {
			OSShowSaveFileDialog(instance, OS_MAKE_NOTIFICATION_CALLBACK(_InstanceSaveFile, notification->context), instance->window); 
		} else {
			instance->modified = false;

			OSNotification n = *notification;
			n.type = OS_NOTIFICATION_SAVE_FILE;
			n.fileDialog.path = instance->filename.buffer;
			n.fileDialog.pathBytes = instance->filename.bytes;
			n.fileDialog.error = OS_ERROR_UNKNOWN_OPERATION_FAILURE;

			if (OS_CALLBACK_REJECTED == OSSendNotification(notification->generator, instance->notificationCallback, &n, instance)) {
				OSShowDialogAlert(OSLiteral("Error"),
						OSLiteral("The file could not be saved."),
						OSLiteral("TODO: Error messages."),
						notification->instance,
						OS_ICON_ERROR, instance->window);
			} else {
				CreateString(n.fileDialog.path, n.fileDialog.pathBytes, &instance->filename);
				OSEnableCommand(instance->builtinCommands + osCommandBackupFile, true);
				OSDisableCommand(instance->builtinCommands + osCommandSaveFile, true);
				OSSetWindowTitle(instance->window, n.fileDialog.path, n.fileDialog.pathBytes);
			}
		}

		return OS_CALLBACK_HANDLED;
	} else {
		return OS_CALLBACK_NOT_HANDLED;
	}
}

OSCallbackResponse _InstanceBackupFile(OSNotification *notification) {
	OSInstance *instance = (OSInstance *) notification->instance;

	if (notification->type == OS_NOTIFICATION_COMMAND) {
		OSShowDialogAlert(OSLiteral("Not Implemented"),
				OSLiteral("TODO Implement this."),
				OSLiteral("hello!"),
				instance,
				OS_ICON_ERROR, instance->window);

		return OS_CALLBACK_HANDLED;
	} else return OS_CALLBACK_NOT_HANDLED;
}

static Window *CreateWindow(OSWindowTemplate *specification, Window *menuParent, unsigned x = 0, unsigned y = 0, 
		Window *modalParent = nullptr, OSInstance *instance = nullptr) {
	unsigned flags = specification->flags;

	if (!flags) {
		flags = OS_CREATE_WINDOW_NORMAL;
	}

	if (specification->menubar) {
		flags |= OS_CREATE_WINDOW_WITH_MENUBAR;
	}

	int width = specification->width;
	int height = specification->height;
	int minimumWidth = specification->minimumWidth;
	int minimumHeight = specification->minimumHeight;

	if (!(flags & OS_CREATE_WINDOW_MENU)) {
		width += totalBorderWidth;
		minimumWidth += totalBorderWidth;
		height += totalBorderHeight;
		minimumHeight += totalBorderHeight;
	}

	Window *window = (Window *) GUIAllocate(sizeof(Window), true);
	window->type = API_OBJECT_WINDOW;
	window->windowItem.thisItem = window;
	window->repaintItem.thisItem = window;
	allWindows.InsertEnd(&window->windowItem);
	CreateString(specification->title, specification->titleBytes, &window->baseWindowTitle);

	if (instance) {
		window->instance = instance;
	} else if (menuParent) {
		window->instance = menuParent->instance;
		window->hasMenuParent = true;
	} else if (modalParent) {
		window->instance = modalParent->instance;
		window->modalParent = modalParent;
	} else {
		window->temporaryInstance = true;
		window->instance = (OSInstance *) OSCreateInstance(nullptr, nullptr, nullptr);
	}

	if (!window->instance->window) window->instance->window = window;

	OSHandle modalParentHandle = OS_INVALID_HANDLE;
	bool closeModalParentHandle = false;

	if (!modalParent && !menuParent) {
		modalParentHandle = OSSyscall(OS_SYSCALL_GET_INSTANCE_MODAL_PARENT, window->instance->handle, 0, 0, 0);
		closeModalParentHandle = true;
	} else if (modalParent) {
		modalParentHandle = modalParent->window;
	}

	bool headless = window->instance->headless;

	if (headless) {
		flags |= OS_CREATE_WINDOW_HEADLESS;
	}

	if (!window->instance->builtinCommands) {
		OSCrashProcess(OS_FATAL_ERROR_INVALID_INSTANCE);
	}

	if (!headless) {
		OSRectangle bounds;
		bounds.left = x;
		bounds.right = x + width;
		bounds.top = y;
		bounds.bottom = y + height;

		window->window = modalParentHandle;
		OSSyscall(OS_SYSCALL_CREATE_WINDOW, (uintptr_t) &window->window, (uintptr_t) &bounds, (uintptr_t) window, menuParent ? (uintptr_t) menuParent->window : 0);
		OSSyscall(OS_SYSCALL_GET_WINDOW_BOUNDS, window->window, (uintptr_t) &bounds, 0, 0);

		window->width = bounds.right - bounds.left;
		window->height = bounds.bottom - bounds.top;
	}

	if (closeModalParentHandle && modalParentHandle != OS_INVALID_HANDLE) {
		OSCloseHandle(modalParentHandle);
	}

	window->flags = flags;
	window->cursor = OS_CURSOR_NORMAL;
	window->minimumWidth = minimumWidth;
	window->minimumHeight = minimumHeight;

	OSSetMessageCallback(window, OS_MAKE_MESSAGE_CALLBACK(ProcessWindowMessage, nullptr));

	window->root = (Grid *) OSCreateGrid(3, 4, OS_GRID_STYLE_LAYOUT);
	window->root->noRightToLeftLayout = true;
	window->root->parent = window;

	{
		OSMessage message;
		message.parentUpdated.window = window;
		message.type = OS_MESSAGE_PARENT_UPDATED;
		OSSendMessage(window->root, &message);
	}

	if (flags & OS_CREATE_WINDOW_NORMAL) {
		OSObject titlebar = CreateWindowResizeHandle(windowBorder22, RESIZE_MOVE);
		OSAddControl(window->root, 1, 1, titlebar, OS_CELL_H_PUSH | OS_CELL_H_EXPAND | OS_CELL_V_EXPAND);
		OSSetText(titlebar, specification->title, specification->titleBytes, OS_RESIZE_MODE_IGNORE);

		if (!(flags & OS_CREATE_WINDOW_RESIZABLE)) {
			OSAddControl(window->root, 0, 0, CreateWindowResizeHandle(dialogBorder11, RESIZE_MOVE), 0);
			OSAddControl(window->root, 1, 0, CreateWindowResizeHandle(dialogBorder12, RESIZE_MOVE), OS_CELL_H_PUSH | OS_CELL_H_EXPAND);
			OSAddControl(window->root, 2, 0, CreateWindowResizeHandle(dialogBorder13, RESIZE_MOVE), 0);
			OSAddControl(window->root, 0, 1, CreateWindowResizeHandle(dialogBorder21, RESIZE_MOVE), 0);
			OSAddControl(window->root, 2, 1, CreateWindowResizeHandle(dialogBorder23, RESIZE_MOVE), 0);
			OSAddControl(window->root, 0, 2, CreateWindowResizeHandle(dialogBorder31, RESIZE_NONE), OS_CELL_V_PUSH | OS_CELL_V_EXPAND);
			OSAddControl(window->root, 2, 2, CreateWindowResizeHandle(dialogBorder33, RESIZE_NONE), OS_CELL_V_PUSH | OS_CELL_V_EXPAND);
			OSAddControl(window->root, 0, 3, CreateWindowResizeHandle(dialogBorder41, RESIZE_NONE), 0);
			OSAddControl(window->root, 1, 3, CreateWindowResizeHandle(dialogBorder42, RESIZE_NONE), OS_CELL_H_PUSH | OS_CELL_H_EXPAND);
			OSAddControl(window->root, 2, 3, CreateWindowResizeHandle(dialogBorder43, RESIZE_NONE), 0);
		} else {
			OSAddControl(window->root, 0, 0, CreateWindowResizeHandle(windowBorder11, RESIZE_TOP_LEFT), 0);
			OSAddControl(window->root, 1, 0, CreateWindowResizeHandle(windowBorder12, RESIZE_TOP), OS_CELL_H_PUSH | OS_CELL_H_EXPAND);
			OSAddControl(window->root, 2, 0, CreateWindowResizeHandle(windowBorder13, RESIZE_TOP_RIGHT), 0);
			OSAddControl(window->root, 0, 1, CreateWindowResizeHandle(windowBorder21, RESIZE_LEFT), 0);
			OSAddControl(window->root, 2, 1, CreateWindowResizeHandle(windowBorder23, RESIZE_RIGHT), 0);
			OSAddControl(window->root, 0, 2, CreateWindowResizeHandle(windowBorder31, RESIZE_LEFT), OS_CELL_V_PUSH | OS_CELL_V_EXPAND);
			OSAddControl(window->root, 2, 2, CreateWindowResizeHandle(windowBorder33, RESIZE_RIGHT), OS_CELL_V_PUSH | OS_CELL_V_EXPAND);
			OSAddControl(window->root, 0, 3, CreateWindowResizeHandle(windowBorder41, RESIZE_BOTTOM_LEFT), 0);
			OSAddControl(window->root, 1, 3, CreateWindowResizeHandle(windowBorder42, RESIZE_BOTTOM), OS_CELL_H_PUSH | OS_CELL_H_EXPAND);
			OSAddControl(window->root, 2, 3, CreateWindowResizeHandle(windowBorder43, RESIZE_BOTTOM_RIGHT), 0);
		}

		if (flags & OS_CREATE_WINDOW_WITH_MENUBAR) {
			OSObject grid = OSCreateGrid(1, 2, OS_GRID_STYLE_LAYOUT);
			OSAddGrid(window->root, 1, 2, grid, OS_CELL_FILL);
			OSAddGrid(grid, 0, 0, OSCreateMenu(specification->menubar, nullptr, OS_MAKE_POINT(0, 0), OS_CREATE_MENUBAR, instance), OS_CELL_H_PUSH | OS_CELL_H_EXPAND);
		}
	}

	OSRemoveFocusedControl(window, true);

	return window;
}

void OSSetWindowTitle(OSObject _window, char *text, size_t textBytes) {
	Window *window = (Window *) _window;
	Control *titlebar = (Control *) window->root->objects[4];

	if (!textBytes) {
		OSSetText(titlebar, window->baseWindowTitle.buffer, window->baseWindowTitle.bytes, OS_RESIZE_MODE_IGNORE);
	} else {
		char buffer[4096];
		size_t length = OSFormatString(buffer, 4096, "%s - %s", textBytes, text, window->baseWindowTitle.bytes, window->baseWindowTitle.buffer);
		OSSetText(titlebar, buffer, length, OS_RESIZE_MODE_IGNORE);
	}
}

OSObject OSCreateWindow(OSWindowTemplate *specification, OSObject instance) {
	return CreateWindow(specification, nullptr, 0, 0, nullptr, (OSInstance *) instance);
}

static OSCallbackResponse CommandDialogAlertOK(OSNotification *notification) {
	if (notification->type == OS_NOTIFICATION_COMMAND) {
		OSCloseWindow(notification->context);
		return OS_CALLBACK_HANDLED;
	}

	return OS_CALLBACK_NOT_HANDLED;
}

static OSCallbackResponse CommandDialogAlertCancel(OSNotification *notification) {
	if (notification->type == OS_NOTIFICATION_COMMAND) {
		OSCloseWindow(notification->context);
		return OS_CALLBACK_HANDLED;
	}

	return OS_CALLBACK_NOT_HANDLED;
}

void OSCloseWindow(OSObject window) {
	OSMessage m;
	m.type = OS_MESSAGE_DESTROY;
	m.context = window;
	OSPostMessage(&m);
}

static OSCallbackResponse FileDialogComplete(OSNotification *notification) {
	if (notification->type == OS_NOTIFICATION_RECEIVE_DATA_FROM_CHILD) {
		OSNotification n;
		n.type = OS_NOTIFICATION_FILE_DIALOG;
		n.fileDialog.path = (char *) notification->receiveData.data;
		n.fileDialog.pathBytes = notification->receiveData.dataBytes;
		n.fileDialog.cancelled = notification->receiveData.dataBytes ? false : true;
		OSNotificationCallback *callback = (OSNotificationCallback *) notification->context;
		OSSendNotification(nullptr, *callback, &n, notification->instance);
		OSDestroyInstance(notification->receiveData.instance);
		OSHeapFree(callback);

		return OS_CALLBACK_HANDLED;
	}

	return OS_CALLBACK_NOT_HANDLED;
}

void OSShowOpenFileDialog(OSObject thisInstance, OSNotificationCallback _callback, OSObject modalWindowParent) {
	OSNotificationCallback *callback = (OSNotificationCallback *) OSHeapAllocate(sizeof(OSNotificationCallback), false);
	OSCopyMemory(callback, &_callback, sizeof(OSNotificationCallback));
	OSObject fileManager = OSCreateInstance(nullptr, nullptr, nullptr);
	OSOpenInstance(fileManager, thisInstance, OSLiteral("File Manager"), OS_FLAGS_DEFAULT, OSLiteral("DIALOG_OPEN"), modalWindowParent);
	OSSetObjectNotificationCallback(fileManager, OS_MAKE_NOTIFICATION_CALLBACK(FileDialogComplete, callback));
}

void OSShowSaveFileDialog(OSObject thisInstance, OSNotificationCallback _callback, OSObject modalWindowParent) {
	OSNotificationCallback *callback = (OSNotificationCallback *) OSHeapAllocate(sizeof(OSNotificationCallback), false);
	OSCopyMemory(callback, &_callback, sizeof(OSNotificationCallback));
	OSObject fileManager = OSCreateInstance(nullptr, nullptr, nullptr);
	OSOpenInstance(fileManager, thisInstance, OSLiteral("File Manager"), OS_FLAGS_DEFAULT, OSLiteral("DIALOG_SAVE"), modalWindowParent);
	OSSetObjectNotificationCallback(fileManager, OS_MAKE_NOTIFICATION_CALLBACK(FileDialogComplete, callback));
}

OSCommand *OSGetDialogCommands(OSObject dialog) {
	return ((Window *) dialog)->dialogCommands;
}

static OSObject CreateDialog(char *title, size_t titleBytes,
				char *message, size_t messageBytes,
				uint16_t iconID, OSObject modalParent,
				OSObject *layouts, OSWindowTemplate *_specification, OSObject *instance, size_t layout5Columns = 2) {
	OSWindowTemplate specification = *_specification;
	specification.flags |= OS_CREATE_WINDOW_DIALOG;
	specification.title = title;
	specification.titleBytes = titleBytes;

	OSObject dialog = CreateWindow(&specification, nullptr, 0, 0, (Window *) modalParent, (OSInstance *) *instance);
	*instance = OSGetInstance(dialog);
	((Window *) dialog)->dialogCommands = OSCreateCommands(osDialogCommands);

	OSObject layout1 = OSCreateGrid(1, 2, OS_GRID_STYLE_LAYOUT);
	OSObject layout2 = OSCreateGrid(3, 1, OS_GRID_STYLE_CONTAINER);
	OSObject layout3 = OSCreateGrid(1, 2, OS_GRID_STYLE_CONTAINER_WITHOUT_BORDER);
	OSObject layout4 = OSCreateGrid(1, 1, OS_GRID_STYLE_CONTAINER_ALT);
	OSObject layout5 = OSCreateGrid(layout5Columns, 1, OS_GRID_STYLE_CONTAINER_WITHOUT_BORDER);

	OSSetRootGrid(dialog, layout1);
	OSAddGrid(layout1, 0, 0, layout2, OS_CELL_FILL);
	OSAddGrid(layout2, 2, 0, layout3, OS_CELL_FILL);
	OSAddGrid(layout1, 0, 1, layout4, OS_CELL_H_EXPAND);
	OSAddGrid(layout4, 0, 0, layout5, OS_CELL_H_RIGHT | OS_CELL_H_PUSH);

	Control *label = (Control *) OSCreateLabel(message, messageBytes, true, true);
	label->textSize = 10;
	label->textColor = TEXT_COLOR_HEADING;
	OSAddControl(layout3, 0, 0, label, OS_CELL_H_EXPAND | OS_CELL_H_PUSH | OS_CELL_V_EXPAND);

	OSAddControl(layout2, 0, 0, OSCreateIconDisplay(iconID), OS_CELL_V_TOP);

	layouts[1] = layout1;
	layouts[2] = layout2;
	layouts[3] = layout3;
	layouts[4] = layout4;
	layouts[5] = layout5;

	return dialog;
}

OSObject OSShowDialogTextPrompt(char *title, size_t titleBytes,
		char *message, size_t messageBytes,
		OSObject instance, uint16_t iconID, OSObject modalParent, OSCommand *command, OSObject *textbox) {
	OSObject layouts[6];
	OSObject dialog = CreateDialog(title, titleBytes, message, messageBytes, iconID, modalParent, layouts, osDialogTextPrompt, &instance);
	OSCommand *dialogCommands = OSGetDialogCommands(dialog);

	OSObject okButton = OSCreateButton(command, OS_BUTTON_STYLE_NORMAL);
	OSAddControl(layouts[5], 0, 0, okButton, OS_CELL_H_RIGHT);
	OSSetFocusedControl(okButton, false);

	OSObject cancelButton = OSCreateButton(dialogCommands + osDialogStandardCancel, OS_BUTTON_STYLE_NORMAL);
	OSAddControl(layouts[5], 1, 0, cancelButton, OS_CELL_H_RIGHT);
	OSSetCommandNotificationCallback(dialogCommands + osDialogStandardCancel, OS_MAKE_NOTIFICATION_CALLBACK(CommandDialogAlertCancel, dialog));

	*textbox = OSCreateTextbox(OS_TEXTBOX_STYLE_NORMAL, OS_TEXTBOX_WRAP_MODE_NONE);
	OSAddControl(layouts[3], 0, 1, *textbox, OS_CELL_H_EXPAND | OS_CELL_H_PUSH | OS_CELL_V_EXPAND);
	OSSetFocusedControl(*textbox, false);

	return dialog;
}

OSObject OSShowDialogConfirm(char *title, size_t titleBytes,
				   char *message, size_t messageBytes,
				   char *description, size_t descriptionBytes,
				   OSObject instance, uint16_t iconID, OSObject modalParent, OSCommand *command, OSCommand *altCommand) {
	OSObject layouts[6];
	OSObject dialog = CreateDialog(title, titleBytes, message, messageBytes, iconID, modalParent, layouts, osDialogStandard, &instance, altCommand ? 3 : 2);
	OSCommand *dialogCommands = OSGetDialogCommands(dialog);

	uintptr_t position = 0;

	OSObject okButton = OSCreateButton(command, OS_BUTTON_STYLE_NORMAL);
	OSAddControl(layouts[5], position++, 0, okButton, OS_CELL_H_RIGHT);
	if (!command->specification->dangerous) OSSetFocusedControl(okButton, false);

	if (altCommand) {
		OSObject altButton = OSCreateButton(altCommand, OS_BUTTON_STYLE_NORMAL);
		OSAddControl(layouts[5], position++, 0, altButton, OS_CELL_H_RIGHT);
	}

	OSObject cancelButton = OSCreateButton(dialogCommands + osDialogStandardCancel, OS_BUTTON_STYLE_NORMAL);
	OSAddControl(layouts[5], position++, 0, cancelButton, OS_CELL_H_RIGHT);
	OSSetCommandNotificationCallback(dialogCommands + osDialogStandardCancel, OS_MAKE_NOTIFICATION_CALLBACK(CommandDialogAlertCancel, dialog));
	if (command->specification->dangerous) OSSetFocusedControl(cancelButton, false);

	OSAddControl(layouts[3], 0, 1, OSCreateLabel(description, descriptionBytes, true, true), OS_CELL_H_EXPAND | OS_CELL_H_PUSH | OS_CELL_V_EXPAND);

	return dialog;
}

OSObject OSShowDialogAlert(char *title, size_t titleBytes,
				   char *message, size_t messageBytes,
				   char *description, size_t descriptionBytes,
				   OSObject instance, uint16_t iconID, OSObject modalParent) {
	OSObject layouts[6];
	OSObject dialog = CreateDialog(title, titleBytes, message, messageBytes, iconID, modalParent, layouts, osDialogStandard, &instance);
	OSCommand *dialogCommands = OSGetDialogCommands(dialog);

	OSObject okButton = OSCreateButton(dialogCommands + osDialogStandardOK, OS_BUTTON_STYLE_NORMAL);
	OSAddControl(layouts[5], 0, 0, okButton, OS_CELL_H_RIGHT);
	OSSetCommandNotificationCallback(dialogCommands + osDialogStandardOK, OS_MAKE_NOTIFICATION_CALLBACK(CommandDialogAlertOK, dialog));
	OSSetCommandNotificationCallback(dialogCommands + osDialogStandardCancel, OS_MAKE_NOTIFICATION_CALLBACK(CommandDialogAlertOK, dialog));
	OSSetFocusedControl(okButton, false);

	OSAddControl(layouts[3], 0, 1, OSCreateLabel(description, descriptionBytes, true, true), OS_CELL_H_EXPAND | OS_CELL_H_PUSH | OS_CELL_V_EXPAND);

	return dialog;
}

OSObject OSCreateDialog(OSObject instance, OSObject modalParent, OSWindowTemplate *specification) {
	specification->flags |= OS_CREATE_WINDOW_DIALOG;
	OSObject dialog = CreateWindow(specification, nullptr, 0, 0, (Window *) modalParent, (OSInstance *) instance);
	instance = OSGetInstance(dialog);
	OSCommand *dialogCommands = OSCreateCommands(osDialogCommands);
	((Window *) dialog)->dialogCommands = dialogCommands;
	return dialog;
}

OSObject OSCreateMenu(OSMenuTemplate *menuTemplate, OSObject _source, OSPoint position, unsigned flags, OSObject instance) {
	if (!menuTemplate->itemCount) {
		// Menus cannot be empty.
		menuTemplate = _osEmptyMenu;
	}

	Control *source = (Control *) _source;

	if (source) {
		RepaintControl(source);
	}

	size_t itemCount = menuTemplate->itemCount;
	bool menubar = flags & OS_CREATE_MENUBAR;

	Control *items[itemCount];
	int width = 0, height = 7;

	MenuItem *firstMenuItem = nullptr;

	for (uintptr_t i = 0; i < itemCount; i++) {
		switch (menuTemplate->items[i].type) {
			case OSMenuItem_SEPARATOR: {
				items[i] = (Control *) CreateMenuSeparator();
			} break;

			case OSMenuItem_SUBMENU:
			case OSMenuItem_COMMAND: {
				items[i] = (Control *) CreateMenuItem(menuTemplate->items[i], menubar, (OSInstance *) instance);
				if (!firstMenuItem) firstMenuItem = (MenuItem *) items[i];
			} break;

			default: {} continue;
		}

		if (menubar) {
			if (items[i]->preferredHeight > height) {
				height = items[i]->preferredHeight;
			}

			width += items[i]->preferredWidth;
		} else {
			if (items[i]->preferredWidth > width) {
				width = items[i]->preferredWidth;
			}

			height += items[i]->preferredHeight;
		}
	}

	if (!menubar && !(flags & OS_CREATE_MENU_BLANK)) {
		if (width < 100) width = 100;
		width += 8;
	}

	if (width < menuTemplate->minimumWidth) {
		width = menuTemplate->minimumWidth;
	}

	if (height < menuTemplate->minimumHeight) {
		height = menuTemplate->minimumHeight;
	}

	OSObject grid = nullptr;

	if (!(flags & OS_CREATE_MENU_BLANK)) {
		grid = OSCreateGrid(menubar ? itemCount : 1, !menubar ? itemCount : 1, menubar ? OS_GRID_STYLE_MENUBAR : OS_GRID_STYLE_MENU);
		((Grid *) grid)->tabStop = false;
		((Grid *) grid)->isMenubar = menubar;
	}
	
	OSObject returnValue = grid;

	if (!menubar) {
		OSWindowTemplate specification = {};
		specification.width = width;
		specification.height = height;
		specification.flags = OS_CREATE_WINDOW_MENU;

		Window *window;
		int x = position.x;
		int y = position.y;

		if (x == -1 && y == -1 && source && x != -2) {
			if (flags & OS_CREATE_SUBMENU) {
				x = source->bounds.right;
				y = source->bounds.top;
			} else {
				x = source->bounds.left;
				y = source->bounds.bottom - 2;
			}

			OSRectangle bounds;
			OSSyscall(OS_SYSCALL_GET_WINDOW_BOUNDS, source->window->window, (uintptr_t) &bounds, 0, 0);

			x += bounds.left;
			y += bounds.top;
		}

		if ((x == -1 && y == -1) || x == -2) {
			OSPoint position;
			OSGetMousePosition(nullptr, &position);

			x = position.x;
			y = position.y;
		}

		window = CreateWindow(&specification, source ? source->window : nullptr, x, y);

		for (uintptr_t i = 0; i < openMenuCount; i++) {
			if (openMenus[i].source->window == source->window) {
				OSMessage message;
				message.type = OS_MESSAGE_DESTROY;
				OSSendMessage(openMenus[i].window, &message);
				break;
			}
		}

		openMenus[openMenuCount].source = source;
		openMenus[openMenuCount].window = window;
		openMenus[openMenuCount].fromMenubar = flags & OS_CREATE_MENU_FROM_MENUBAR;
		openMenuCount++;

		if (!(flags & OS_CREATE_MENU_BLANK)) {
			if (navigateMenuMode && navigateMenuUsedKey) {
				navigateMenuItem = firstMenuItem;
			} else {
				navigateMenuMode = true;
				navigateMenuItem = nullptr;
			}
		}

		navigateMenuFirstItem = firstMenuItem;

		if (grid) {
			OSSetRootGrid(window, grid);
		}

		returnValue = window;
	}

	for (uintptr_t i = 0; i < itemCount; i++) {
		OSAddControl(grid, menubar ? i : 0, !menubar ? i : 0, items[i], OS_CELL_H_EXPAND | OS_CELL_V_EXPAND);
	}

#if 0
	if (!menubar) {
		OSSetFocusedControl(items[0], true);
	}
#endif

	return returnValue;
}

void OSGetMousePosition(OSObject relativeWindow, OSPoint *position) {
	OSSyscall(OS_SYSCALL_GET_CURSOR_POSITION, (uintptr_t) position, 0, 0, 0);

	if (relativeWindow) {
		Window *window = (Window *) relativeWindow;
		if (window->flags & OS_CREATE_WINDOW_HEADLESS) return;

		OSRectangle bounds;
		OSSyscall(OS_SYSCALL_GET_WINDOW_BOUNDS, window->window, (uintptr_t) &bounds, 0, 0);

		position->x -= bounds.left;
		position->y -= bounds.top;
	}
}

void OSSetProperty(OSObject object, uintptr_t index, void *value) {
	OSMessage message;
	message.type = OS_MESSAGE_SET_PROPERTY;
	message.setProperty.index = index;
	message.setProperty.value = value;
	OSSendMessage(object, &message);
}

void OSInitialiseGUI() {
	OSLinearBuffer buffer;
	OSGetLinearBuffer(OS_SURFACE_UI_SHEET, &buffer);

	uint32_t *skin = (uint32_t *) OSMapObject(buffer.handle, 0, buffer.width * buffer.height * 4, OS_FLAGS_DEFAULT);

	if (osSystemConstants[OS_SYSTEM_CONSTANT_NO_FANCY_GRAPHICS]) {
		STANDARD_BACKGROUND_COLOR = OS_BOX_COLOR_GRAY;

		LIST_VIEW_COLUMN_TEXT_COLOR = OS_BOX_COLOR_BLACK;
		LIST_VIEW_PRIMARY_TEXT_COLOR = OS_BOX_COLOR_BLACK;
		LIST_VIEW_SECONDARY_TEXT_COLOR = OS_BOX_COLOR_BLACK;
		LIST_VIEW_BACKGROUND_COLOR = OS_BOX_COLOR_WHITE;

		TEXT_COLOR_DEFAULT = OS_BOX_COLOR_BLACK;
		// TEXT_COLOR_DISABLED = skin[6 * 3 + 25 * buffer.width];
		// TEXT_COLOR_DISABLED_SHADOW = skin[7 * 3 + 25 * buffer.width];
		TEXT_COLOR_HEADING = OS_BOX_COLOR_BLACK;
		TEXT_COLOR_TITLEBAR = OS_BOX_COLOR_WHITE;
		TEXT_COLOR_TOOLBAR = OS_BOX_COLOR_BLACK;

		TEXTBOX_SELECTED_COLOR_1 = 0x8080FF;
		TEXTBOX_SELECTED_COLOR_2 = OS_BOX_COLOR_GRAY;

		DISABLE_TEXT_SHADOWS = 0;
		TEXT_SHADOWS_OFFSET = 1;
	} else {
		STANDARD_BACKGROUND_COLOR = skin[0 * 3 + 25 * buffer.width];

		LIST_VIEW_COLUMN_TEXT_COLOR = skin[1 * 3 + 25 * buffer.width];
		LIST_VIEW_PRIMARY_TEXT_COLOR = skin[2 * 3 + 25 * buffer.width];
		LIST_VIEW_SECONDARY_TEXT_COLOR = skin[3 * 3 + 25 * buffer.width];
		LIST_VIEW_BACKGROUND_COLOR = skin[4 * 3 + 25 * buffer.width];

		TEXT_COLOR_DEFAULT = skin[5 * 3 + 25 * buffer.width];
		// TEXT_COLOR_DISABLED = skin[6 * 3 + 25 * buffer.width];
		// TEXT_COLOR_DISABLED_SHADOW = skin[7 * 3 + 25 * buffer.width];
		TEXT_COLOR_HEADING = skin[8 * 3 + 25 * buffer.width];
		TEXT_COLOR_TITLEBAR = skin[0 * 3 + 28 * buffer.width];
		TEXT_COLOR_TOOLBAR = skin[1 * 3 + 28 * buffer.width];

		TEXTBOX_SELECTED_COLOR_1 = skin[2 * 3 + 28 * buffer.width];
		TEXTBOX_SELECTED_COLOR_2 = skin[3 * 3 + 28 * buffer.width];

		DISABLE_TEXT_SHADOWS = skin[4 * 3 + 28 * buffer.width];
		TEXT_SHADOWS_OFFSET = 0;
	}

	OSUnmapObject(skin);
	OSCloseHandle(buffer.handle);
}

void RefreshAllWindows() {
	LinkedItem<Window> *item = allWindows.firstItem;

	while (item) {
		Window *window = item->thisItem;

		OSMessage layout;
		layout.type = OS_MESSAGE_LAYOUT;
		layout.layout.left = 0;
		layout.layout.top = 0;
		layout.layout.right = window->width;
		layout.layout.bottom = window->height;
		layout.layout.force = true;
		layout.layout.clip = OS_MAKE_RECTANGLE(0, window->width, 0, window->height);
		OSSendMessage(window->root, &layout);

		item = item->nextItem;
	}
}

#include "list_view.cpp"
