// UI images:

static UIImage activeWindowBorder11	= {{1, 1 + 6, 144, 144 + 6}, 	{1, 1, 144, 144}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};
static UIImage activeWindowBorder12	= {{8, 8 + 1, 144, 144 + 6}, 	{8, 9, 144, 144}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};
static UIImage activeWindowBorder13	= {{10, 10 + 6, 144, 144 + 6}, 	{10, 10, 144, 144}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};
static UIImage activeWindowBorder21	= {{1, 1 + 6, 151, 151 + 24}, 	{1, 1, 151, 151}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};
static UIImage activeWindowBorder22	= {{8, 8 + 1, 151, 151 + 24}, 	{8, 9, 151, 151}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_FLAT, OS_BOX_COLOR_BLUE};
static UIImage activeWindowBorder23	= {{10, 10 + 6, 151, 151 + 24},	{10, 10, 151, 151}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};
static UIImage activeWindowBorder31	= {{1, 1 + 6, 176, 176 + 1}, 	{1, 1, 176, 177}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};
static UIImage activeWindowBorder33	= {{10, 10 + 6, 176, 176 + 1}, 	{10, 10, 176, 177}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};
static UIImage activeWindowBorder41	= {{1, 1 + 6, 178, 178 + 6}, 	{1, 1, 178, 178}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};
static UIImage activeWindowBorder42	= {{8, 8 + 1, 178, 178 + 6}, 	{8, 9, 178, 178}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};
static UIImage activeWindowBorder43	= {{10, 10 + 6, 178, 178 + 6}, 	{10, 10, 178, 178}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};

static UIImage inactiveWindowBorder11	= {{16 + 1, 16 + 1 + 6, 144, 144 + 6}, 	{16 + 1, 16 + 1, 144, 144}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};
static UIImage inactiveWindowBorder12	= {{16 + 8, 16 + 8 + 1, 144, 144 + 6}, 	{16 + 8, 16 + 9, 144, 144}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};
static UIImage inactiveWindowBorder13	= {{16 + 10, 16 + 10 + 6, 144, 144 + 6},{16 + 10, 16 + 10, 144, 144}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};
static UIImage inactiveWindowBorder21	= {{16 + 1, 16 + 1 + 6, 151, 151 + 24}, {16 + 1, 16 + 1, 151, 151}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};
static UIImage inactiveWindowBorder22	= {{16 + 8, 16 + 8 + 1, 151, 151 + 24}, {16 + 8, 16 + 9, 151, 151}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_FLAT, OS_BOX_COLOR_DARK_GRAY};
static UIImage inactiveWindowBorder23	= {{16 + 10, 16 + 10 + 6, 151, 151 + 24},{16 + 10, 16 + 10, 151, 151}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};
static UIImage inactiveWindowBorder31	= {{16 + 1, 16 + 1 + 6, 176, 176 + 1}, 	{16 + 1, 16 + 1, 176, 177}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};
static UIImage inactiveWindowBorder33	= {{16 + 10, 16 + 10 + 6, 176, 176 + 1}, {16 + 10, 16 + 10, 176, 177}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};
static UIImage inactiveWindowBorder41	= {{16 + 1, 16 + 1 + 6, 178, 178 + 6}, 	{16 + 1, 16 + 1, 178, 178}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};
static UIImage inactiveWindowBorder42	= {{16 + 8, 16 + 8 + 1, 178, 178 + 6}, 	{16 + 8, 16 + 9, 178, 178}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};
static UIImage inactiveWindowBorder43	= {{16 + 10, 16 + 10 + 6, 178, 178 + 6}, {16 + 10, 16 + 10, 178, 178}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};

static UIImage activeDialogBorder11	= {{1, 1 + 3, 144, 144 + 3}, 	{1, 1, 144, 144}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};
static UIImage activeDialogBorder12	= {{8, 8 + 1, 144, 144 + 3}, 	{8, 9, 144, 144}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};
static UIImage activeDialogBorder13	= {{13, 13 + 3, 144, 144 + 3}, 	{13, 13, 144, 144}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};
static UIImage activeDialogBorder21	= {{1, 1 + 3, 151, 151 + 24}, 	{1, 1, 151, 151}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};
static UIImage activeDialogBorder23	= {{13, 13 + 3, 151, 151 + 24},	{13, 13, 151, 151}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};
static UIImage activeDialogBorder31	= {{1, 1 + 3, 185, 185 + 1}, 	{1, 1, 185, 186}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};
static UIImage activeDialogBorder33	= {{1, 1 + 3, 187, 187 + 1}, 	{1, 1, 187, 188}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};
static UIImage activeDialogBorder41	= {{1, 1 + 3, 181, 181 + 3}, 	{1, 1, 181, 181}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};
static UIImage activeDialogBorder42	= {{5, 5 + 1, 185, 185 + 3}, 	{5, 6, 185, 185}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};
static UIImage activeDialogBorder43	= {{13, 13 + 3, 181, 181 + 3}, 	{13, 13, 181, 181}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};

static UIImage inactiveDialogBorder11	= {{16 + 1, 16 + 1 + 3, 144, 144 + 3}, 	{16 + 1, 16 + 1, 144, 144}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};
static UIImage inactiveDialogBorder12	= {{16 + 8, 16 + 8 + 1, 144, 144 + 3}, 	{16 + 8, 16 + 9, 144, 144}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};
static UIImage inactiveDialogBorder13	= {{16 + 10 + 3, 16 + 10 + 3 + 3, 144, 144 + 3},{16 + 10 + 3, 16 + 10 + 3, 144, 144}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};
static UIImage inactiveDialogBorder21	= {{16 + 1, 16 + 1 + 3, 151, 151 + 24}, {16 + 1, 16 + 1, 151, 151}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};
static UIImage inactiveDialogBorder23	= {{16 + 10 + 3, 16 + 10 + 3 + 3, 151, 151 + 24},{16 + 10 + 3, 16 + 10 + 3, 151, 151}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};
static UIImage inactiveDialogBorder31	= {{1, 1 + 3, 189, 189 + 1}, 	{1, 1, 189, 190}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};
static UIImage inactiveDialogBorder33	= {{1, 1 + 3, 191, 191 + 1}, {1, 1, 191, 192}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};
static UIImage inactiveDialogBorder41	= {{16 + 1, 16 + 1 + 3, 181, 181 + 3}, 	{16 + 1, 16 + 1, 181, 181}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};
static UIImage inactiveDialogBorder42	= {{5, 5 + 1, 189, 189 + 3}, 	{5, 5 + 1, 189, 189}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};
static UIImage inactiveDialogBorder43	= {{16 + 10 + 3, 16 + 10 + 3 + 3, 181, 181 + 3}, {16 + 10 + 3, 16 + 10 + 3, 181, 181}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};

static UIImage progressBarBackground 	= {{34, 62, 0, 16}, {36, 60, 8, 14}, OS_DRAW_MODE_STRECH, OS_BOX_STYLE_INWARDS, OS_BOX_COLOR_GRAY};
static UIImage progressBarDisabled 	= {{29 + 34, 29 + 62, 0, 16}, {29 + 36, 29 + 60, 8, 14}, OS_DRAW_MODE_STRECH, OS_BOX_STYLE_INWARDS, OS_BOX_COLOR_GRAY};
static UIImage progressBarFilled 	= {{1 + 29 + 29 + 34, 29 + 29 + 62 - 1, 1, 15}, {29 + 29 + 36, 29 + 29 + 60, 8, 14}, OS_DRAW_MODE_STRECH, OS_BOX_STYLE_FLAT, OS_BOX_COLOR_BLUE};
static UIImage progressBarFilledBlue 	= {{29 + 1 + 29 + 29 + 34, 29 + 29 + 29 + 62 - 1, 1, 15}, {29 + 29 + 29 + 36, 29 + 29 + 29 + 60, 8, 14}, OS_DRAW_MODE_STRECH, OS_BOX_STYLE_FLAT, OS_BOX_COLOR_BLUE};
static UIImage progressBarMarquee 	= {{95, 185, 17, 30}, {95, 185, 24, 29}, OS_DRAW_MODE_STRECH, OS_BOX_STYLE_FLAT, OS_BOX_COLOR_BLUE};

static UIImage buttonNormal		= {{0 * 9 + 0, 0 * 9 + 8, 88, 109}, {0 * 9 + 3, 0 * 9 + 5, 91, 106}, OS_DRAW_MODE_STRECH, OS_BOX_STYLE_OUTWARDS, OS_BOX_COLOR_GRAY };
static UIImage buttonDisabled		= {{1 * 9 + 0, 1 * 9 + 8, 88, 109}, {1 * 9 + 3, 1 * 9 + 5, 91, 106}, OS_DRAW_MODE_STRECH, OS_BOX_STYLE_OUTWARDS, OS_BOX_COLOR_GRAY };
static UIImage buttonHover		= {{2 * 9 + 0, 2 * 9 + 8, 88, 109}, {2 * 9 + 3, 2 * 9 + 5, 91, 106}, OS_DRAW_MODE_STRECH, OS_BOX_STYLE_OUTWARDS, OS_BOX_COLOR_GRAY };
static UIImage buttonPressed		= {{3 * 9 + 0, 3 * 9 + 8, 88, 109}, {3 * 9 + 3, 3 * 9 + 5, 91, 106}, OS_DRAW_MODE_STRECH, OS_BOX_STYLE_PUSHED, OS_BOX_COLOR_GRAY };
static UIImage buttonFocused		= {{4 * 9 + 0, 4 * 9 + 8, 88, 109}, {4 * 9 + 3, 4 * 9 + 5, 91, 106}, OS_DRAW_MODE_STRECH, OS_BOX_STYLE_OUTWARDS | OS_BOX_STYLE_DOTTED, OS_BOX_COLOR_GRAY };
static UIImage buttonDangerousHover	= {{5 * 9 + 0, 5 * 9 + 8, 88, 109}, {5 * 9 + 3, 5 * 9 + 5, 91, 106}, OS_DRAW_MODE_STRECH, OS_BOX_STYLE_OUTWARDS, OS_BOX_COLOR_GRAY };
static UIImage buttonDangerousPressed	= {{6 * 9 + 0, 6 * 9 + 8, 88, 109}, {6 * 9 + 3, 6 * 9 + 5, 91, 106}, OS_DRAW_MODE_STRECH, OS_BOX_STYLE_PUSHED, OS_BOX_COLOR_GRAY };
static UIImage buttonDangerousFocused	= {{7 * 9 + 0, 7 * 9 + 8, 88, 109}, {7 * 9 + 3, 7 * 9 + 5, 91, 106}, OS_DRAW_MODE_STRECH, OS_BOX_STYLE_OUTWARDS | OS_BOX_STYLE_DOTTED, OS_BOX_COLOR_GRAY };
static UIImage buttonDefault		= {{8 * 9 + 0, 8 * 9 + 8, 88, 109}, {8 * 9 + 3, 8 * 9 + 5, 91, 106}, OS_DRAW_MODE_STRECH, OS_BOX_STYLE_SELECTED, OS_BOX_COLOR_GRAY };
static UIImage buttonDangerousDefault	= {{9 * 9 + 0, 9 * 9 + 8, 88, 109}, {9 * 9 + 3, 9 * 9 + 5, 91, 106}, OS_DRAW_MODE_STRECH, OS_BOX_STYLE_SELECTED, OS_BOX_COLOR_GRAY };

static UIImage checkboxHover		= {{48, 61, 242, 255}, {48, 49, 242, 243}, OS_DRAW_MODE_REPEAT_FIRST};
static UIImage radioboxHover		= {{48, 61, 242 - 13, 255 - 13}, {48, 49, 242 - 13, 243 - 13}, OS_DRAW_MODE_REPEAT_FIRST};

static UIImage textboxNormal		= {{52, 61, 166, 189}, {55, 58, 169, 186}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_INWARDS, OS_BOX_COLOR_WHITE};
static UIImage textboxFocus		= {{11 + 52, 11 + 61, 166, 189}, {11 + 55, 11 + 58, 169, 186}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_INWARDS, OS_BOX_COLOR_WHITE};
static UIImage textboxHover		= {{-11 + 52, -11 + 61, 166, 189}, {-11 + 55, -11 + 58, 169, 186}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_INWARDS, OS_BOX_COLOR_WHITE};
static UIImage textboxDisabled		= {{22 + 52, 22 + 61, 166, 189}, {22 + 55, 22 + 58, 169, 186}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_INWARDS, OS_BOX_COLOR_GRAY};
static UIImage textboxCommand		= {{33 + 52, 33 + 61, 166, 189}, {33 + 55, 33 + 58, 169, 186}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_INWARDS, OS_BOX_COLOR_GRAY};
static UIImage textboxCommandHover	= {{44 + 52, 44 + 61, 166, 189}, {44 + 55, 44 + 58, 169, 186}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_INWARDS, OS_BOX_COLOR_GRAY};
static UIImage textboxNoBorder		= {{164, 173, 202, 225}, {165, 172, 203, 224}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_FLAT, OS_BOX_COLOR_WHITE};

static UIImage gridBox 			= {{1, 7, 17, 23}, {3, 4, 19, 20}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_NEUTRAL, OS_BOX_COLOR_GRAY};
static UIImage menuBox 			= {{1, 31, 1, 14}, {28, 30, 4, 6}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_OUTWARDS, OS_BOX_COLOR_GRAY};
static UIImage menuBoxBlank		= {{22, 27, 16, 24}, {24, 25, 19, 20}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_OUTWARDS, OS_BOX_COLOR_GRAY};
static UIImage menubarBackground	= {{34, 40, 124, 145}, {35, 38, 128, 144}, OS_DRAW_MODE_STRECH, OS_BOX_STYLE_FLAT, OS_BOX_COLOR_GRAY};
static UIImage dialogAltAreaBox		= {{18, 19, 17, 22}, {18, 19, 21, 22}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_FLAT, OS_BOX_COLOR_GRAY};

static UIImage menuIconCheck 		= {{0, 16, 32, 32 + 16}, {0, 0, 32, 32}, OS_DRAW_MODE_REPEAT_FIRST};
static UIImage menuIconRadio 		= {{0, 16, 17 + 32, 17 + 32 + 16}, {0, 0, 17 + 32, 17 + 32}, OS_DRAW_MODE_REPEAT_FIRST};
static UIImage menuIconSub 		= {{0, 16, 17 + 17 + 32, 17 + 17 + 32 + 16}, {0, 0, 17 + 17 + 32, 17 + 17 + 32}, OS_DRAW_MODE_REPEAT_FIRST};

static UIImage menuItemHover		= {{42, 50, 142, 159}, {45, 46, 151, 157}, OS_DRAW_MODE_STRECH, OS_BOX_STYLE_FLAT, OS_BOX_COLOR_BLUE};
static UIImage menuItemDragged		= {{18 + 42, 18 + 50, 142, 159}, {18 + 45, 18 + 46, 151, 157}, OS_DRAW_MODE_STRECH, OS_BOX_STYLE_FLAT, OS_BOX_COLOR_BLUE};

static UIImage toolbarBackground	= {{0, 0 + 60, 195, 195 + 31}, {0 + 1, 0 + 59, 195 + 1, 195 + 29}, OS_DRAW_MODE_STRECH, OS_BOX_STYLE_FLAT, OS_BOX_COLOR_GRAY};
static UIImage toolbarBackgroundAlt	= {{98 + 0, 98 + 0 + 60, 195, 195 + 31}, {98 + 0 + 1, 98 + 0 + 59, 195 + 1, 195 + 29}, OS_DRAW_MODE_STRECH, OS_BOX_STYLE_FLAT, OS_BOX_COLOR_GRAY};

static UIImage toolbarHover		= {{73, 84, 195, 226}, {78, 79, 203, 204}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_OUTWARDS, OS_BOX_COLOR_GRAY};
static UIImage toolbarPressed		= {{73 - 12, 84 - 12, 195, 226}, {78 - 12, 79 - 12, 203, 204}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_INWARDS, OS_BOX_COLOR_GRAY};
static UIImage toolbarNormal		= {{73 + 12, 84 + 12, 195, 226}, {78 + 12, 79 + 12, 203, 204}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_OUTWARDS, OS_BOX_COLOR_GRAY};

static UIImage scrollbarTrackHorizontalEnabled  = {{121, 122, 62, 79}, {121, 122, 62, 62}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_FLAT, OS_BOX_COLOR_GRAY};
static UIImage scrollbarTrackHorizontalPressed  = {{117, 118, 62, 79}, {117, 118, 62, 62}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_FLAT, OS_BOX_COLOR_GRAY};
static UIImage scrollbarTrackHorizontalDisabled = {{119, 120, 62, 79}, {119, 120, 62, 62}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_FLAT, OS_BOX_COLOR_WHITE};
static UIImage scrollbarTrackVerticalEnabled    = {{174, 191, 82, 83}, {174, 174, 82, 83}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_FLAT, OS_BOX_COLOR_GRAY};
static UIImage scrollbarTrackVerticalPressed    = {{174, 191, 84, 85}, {174, 174, 84, 85}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_FLAT, OS_BOX_COLOR_GRAY};
static UIImage scrollbarTrackVerticalDisabled   = {{174, 191, 80, 81}, {174, 174, 80, 81}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_FLAT, OS_BOX_COLOR_WHITE};
static UIImage scrollbarButtonHorizontalNormal  = {{159, 166, 62, 79}, {162, 163, 62, 62}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_OUTWARDS, OS_BOX_COLOR_GRAY};
static UIImage scrollbarButtonHorizontalHover   = {{167, 174, 62, 79}, {170, 171, 62, 62}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_OUTWARDS, OS_BOX_COLOR_GRAY};
static UIImage scrollbarButtonHorizontalPressed = {{175, 182, 62, 79}, {178, 179, 62, 62}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_OUTWARDS, OS_BOX_COLOR_GRAY};
static UIImage scrollbarButtonVerticalNormal    = {{141, 158, 62, 69}, {141, 141, 65, 66}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_OUTWARDS, OS_BOX_COLOR_GRAY};
static UIImage scrollbarButtonVerticalHover     = {{141, 158, 70, 77}, {141, 141, 73, 74}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_OUTWARDS, OS_BOX_COLOR_GRAY};
static UIImage scrollbarButtonVerticalPressed   = {{141, 158, 78, 85}, {141, 141, 81, 82}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_OUTWARDS, OS_BOX_COLOR_GRAY};
static UIImage scrollbarButtonDisabled          = {{183, 190, 62, 79}, {186, 187, 62, 62}, OS_DRAW_MODE_REPEAT_FIRST, OS_BOX_STYLE_OUTWARDS, OS_BOX_COLOR_GRAY};
static UIImage scrollbarNotchesHorizontal       = {{159, 164, 80, 88}, {159, 159, 80, 80}, OS_DRAW_MODE_REPEAT_FIRST};
static UIImage scrollbarNotchesVertical         = {{165, 173, 80, 85}, {165, 165, 80, 80}, OS_DRAW_MODE_REPEAT_FIRST};

static UIImage smallArrowUpNormal      = {{206, 217, 25, 34}, {206, 206, 25, 25}, OS_DRAW_MODE_REPEAT_FIRST};
static UIImage smallArrowDownNormal    = {{206, 217, 35, 44}, {206, 206, 35, 35}, OS_DRAW_MODE_REPEAT_FIRST};
static UIImage smallArrowLeftNormal    = {{204, 213, 14, 25}, {204, 204, 14, 14}, OS_DRAW_MODE_REPEAT_FIRST};
static UIImage smallArrowRightNormal   = {{204, 213, 3, 14}, {204, 204, 3, 3}, OS_DRAW_MODE_REPEAT_FIRST};

static UIImage lineHorizontal		= {{40, 52, 115, 116}, {41, 41, 115, 115}, OS_DRAW_MODE_REPEAT_FIRST};
static UIImage lineVertical		= {{35, 36, 110, 122}, {35, 35, 111, 111}, OS_DRAW_MODE_REPEAT_FIRST};

static UIImage sliderBox = {{98, 126, 132, 160}, {112, 113, 145, 146}, OS_DRAW_MODE_REPEAT_FIRST};
static UIImage tickHorizontal = {{208, 213, 151, 153}, {209, 211, 151, 153}, OS_DRAW_MODE_REPEAT_FIRST};
static UIImage tickVertical = {{209, 211, 155, 160}, {209, 211, 156, 158}, OS_DRAW_MODE_REPEAT_FIRST};

static UIImage sliderDownHover = {{206, 219, 171, 191}, {206, 206, 171, 171}, OS_DRAW_MODE_REPEAT_FIRST};
static UIImage sliderDownNormal = {{12 + 206, 12 + 219, 171, 191}, {12 + 206, 12 + 206, 171, 171}, OS_DRAW_MODE_REPEAT_FIRST};
static UIImage sliderDownFocused = {{24 + 206, 24 + 219, 171, 191}, {24 + 206, 24 + 206, 171, 171}, OS_DRAW_MODE_REPEAT_FIRST};
static UIImage sliderDownPressed = {{36 + 206, 36 + 219, 171, 191}, {36 + 206, 36 + 206, 171, 171}, OS_DRAW_MODE_REPEAT_FIRST};
static UIImage sliderDownDisabled = {{110, 123, 161, 181}, {110, 110, 161, 161}, OS_DRAW_MODE_REPEAT_FIRST};

static UIImage sliderUpHover = {{206, 219, 19 + 171, 19 + 191}, {206, 206, 19 + 171, 19 + 171}, OS_DRAW_MODE_REPEAT_FIRST};
static UIImage sliderUpNormal = {{12 + 206, 12 + 219, 19 + 171, 19 + 191}, {12 + 206, 12 + 206, 19 + 171, 19 + 171}, OS_DRAW_MODE_REPEAT_FIRST};
static UIImage sliderUpFocused = {{24 + 206, 24 + 219, 19 + 171, 19 + 191}, {24 + 206, 24 + 206, 19 + 171, 19 + 171}, OS_DRAW_MODE_REPEAT_FIRST};
static UIImage sliderUpPressed = {{36 + 206, 36 + 219, 19 + 171, 19 + 191}, {36 + 206, 36 + 206, 19 + 171, 19 + 171}, OS_DRAW_MODE_REPEAT_FIRST};
static UIImage sliderUpDisabled = {{72, 85, 140, 160}, {72, 72, 140, 140}, OS_DRAW_MODE_REPEAT_FIRST};

static UIImage sliderVerticalHover = {{206, 219, 19 + 19 + 171, 19 + 19 + 191}, {206, 206, 19 + 19 + 171, 19 + 19 + 171}, OS_DRAW_MODE_REPEAT_FIRST};
static UIImage sliderVerticalNormal = {{12 + 206, 12 + 219, 19 + 19 + 171, 19 + 19 + 191}, {12 + 206, 12 + 206, 19 + 19 + 171, 19 + 19 + 171}, OS_DRAW_MODE_REPEAT_FIRST};
static UIImage sliderVerticalFocused = {{24 + 206, 24 + 219, 19 + 19 + 171, 19 + 19 + 191}, {24 + 206, 24 + 206, 19 + 19 + 171, 19 + 19 + 171}, OS_DRAW_MODE_REPEAT_FIRST};
static UIImage sliderVerticalPressed = {{36 + 206, 36 + 219, 19 + 19 + 171, 19 + 19 + 191}, {36 + 206, 36 + 206, 19 + 19 + 171, 19 + 19 + 171}, OS_DRAW_MODE_REPEAT_FIRST};
static UIImage sliderVerticalDisabled = {{12 + 72, 12 + 85, 140, 160}, {12 + 72, 12 + 72, 140, 140}, OS_DRAW_MODE_REPEAT_FIRST};

static UIImage sliderRightHover = {{147, 167, 145, 158}, {147, 147, 145, 145}, OS_DRAW_MODE_REPEAT_FIRST};
static UIImage sliderRightNormal = {{147, 167, 12 + 145, 12 + 158}, {147, 147, 12 + 145, 12 + 145}, OS_DRAW_MODE_REPEAT_FIRST};
static UIImage sliderRightFocused = {{147, 167, 24 + 145, 24 + 158}, {147, 147, 24 + 145, 24 + 145}, OS_DRAW_MODE_REPEAT_FIRST};
static UIImage sliderRightPressed = {{147, 167, 36 + 145, 36 + 158}, {147, 147, 36 + 145, 36 + 145}, OS_DRAW_MODE_REPEAT_FIRST};
static UIImage sliderRightDisabled = {{127, 147, 12 + 145, 12 + 158}, {127, 127, 12 + 145, 12 + 145}, OS_DRAW_MODE_REPEAT_FIRST};

static UIImage sliderLeftHover = {{20 + 147, 20 + 167, 145, 158}, {20 + 147, 20 + 147, 145, 145}, OS_DRAW_MODE_REPEAT_FIRST};
static UIImage sliderLeftNormal = {{20 + 147, 20 + 167, 12 + 145, 12 + 158}, {20 + 147, 20 + 147, 12 + 145, 12 + 145}, OS_DRAW_MODE_REPEAT_FIRST};
static UIImage sliderLeftFocused = {{20 + 147, 20 + 167, 24 + 145, 24 + 158}, {20 + 147, 20 + 147, 24 + 145, 24 + 145}, OS_DRAW_MODE_REPEAT_FIRST};
static UIImage sliderLeftPressed = {{20 + 147, 20 + 167, 36 + 145, 36 + 158}, {20 + 147, 20 + 147, 36 + 145, 36 + 145}, OS_DRAW_MODE_REPEAT_FIRST};
static UIImage sliderLeftDisabled = {{127, 147, 145, 158}, {127, 127, 145, 145}, OS_DRAW_MODE_REPEAT_FIRST};

static UIImage sliderHorizontalHover = {{40 + 147, 40 + 167, 145, 158}, {40 + 147, 40 + 147, 145, 145}, OS_DRAW_MODE_REPEAT_FIRST};
static UIImage sliderHorizontalNormal = {{40 + 147, 40 + 167, 12 + 145, 12 + 158}, {40 + 147, 40 + 147, 12 + 145, 12 + 145}, OS_DRAW_MODE_REPEAT_FIRST};
static UIImage sliderHorizontalFocused = {{40 + 147, 40 + 167, 24 + 145, 24 + 158}, {40 + 147, 40 + 147, 24 + 145, 24 + 145}, OS_DRAW_MODE_REPEAT_FIRST};
static UIImage sliderHorizontalPressed = {{40 + 147, 40 + 167, 36 + 145, 36 + 158}, {40 + 147, 40 + 147, 36 + 145, 36 + 145}, OS_DRAW_MODE_REPEAT_FIRST};
static UIImage sliderHorizontalDisabled = {{127, 148, 24 + 145, 24 + 158}, {127, 127, 24 + 145, 24 + 145}, OS_DRAW_MODE_REPEAT_FIRST};

static UIImage tabBand = {{174, 179, 35, 38}, {175, 176, 36, 37}, OS_DRAW_MODE_REPEAT_FIRST};
static UIImage tabContent = {{149, 179, 40, 56}, {150, 177, 41, 54}, OS_DRAW_MODE_STRECH};
static UIImage tabOtherNormal = {{180, 183, 34, 56}, {181, 182, 44, 45}, OS_DRAW_MODE_REPEAT_FIRST};
static UIImage tabOtherHover = {{184, 187, 34, 56}, {185, 186, 44, 45}, OS_DRAW_MODE_REPEAT_FIRST};
static UIImage tabActiveNormal = {{188, 195, 34, 56}, {191, 192, 44, 45}, OS_DRAW_MODE_REPEAT_FIRST};
static UIImage tabPressed = {{145, 148, 34, 56}, {146, 147, 44, 45}, OS_DRAW_MODE_REPEAT_FIRST};
static UIImage tabNew = {{195, 205, 46, 56}, {196, 196, 47, 47}, OS_DRAW_MODE_REPEAT_FIRST};

// UI image groups:

static UIImage *lineHorizontalBackgrounds[] = { &lineHorizontal, &lineHorizontal, &lineHorizontal, &lineHorizontal, };
static UIImage *lineVerticalBackgrounds[] = { &lineVertical, &lineVertical, &lineVertical, &lineVertical, };

static struct UIImage *sliderDown[] = {
	&sliderDownNormal,
	&sliderDownDisabled,
	&sliderDownHover,
	&sliderDownPressed,
	&sliderDownFocused,
};

static struct UIImage *sliderUp[] = {
	&sliderUpNormal,
	&sliderUpDisabled,
	&sliderUpHover,
	&sliderUpPressed,
	&sliderUpFocused,
};

static struct UIImage *sliderVertical[] = {
	&sliderVerticalNormal,
	&sliderVerticalDisabled,
	&sliderVerticalHover,
	&sliderVerticalPressed,
	&sliderVerticalFocused,
};

static struct UIImage *sliderRight[] = {
	&sliderRightNormal,
	&sliderRightDisabled,
	&sliderRightHover,
	&sliderRightPressed,
	&sliderRightFocused,
};

static struct UIImage *sliderLeft[] = {
	&sliderLeftNormal,
	&sliderLeftDisabled,
	&sliderLeftHover,
	&sliderLeftPressed,
	&sliderLeftFocused,
};

static struct UIImage *sliderHorizontal[] = {
	&sliderHorizontalNormal,
	&sliderHorizontalDisabled,
	&sliderHorizontalHover,
	&sliderHorizontalPressed,
	&sliderHorizontalFocused,
};

static struct UIImage *scrollbarTrackVerticalBackgrounds[] = { 
	&scrollbarTrackVerticalEnabled, 
	&scrollbarTrackVerticalDisabled, 
	&scrollbarTrackVerticalEnabled, 
	&scrollbarTrackVerticalPressed, 
	&scrollbarTrackVerticalEnabled, 
};
static struct UIImage *scrollbarTrackHorizontalBackgrounds[] = { 
	&scrollbarTrackHorizontalEnabled, 
	&scrollbarTrackHorizontalDisabled, 
	&scrollbarTrackHorizontalEnabled, 
	&scrollbarTrackHorizontalPressed, 
	&scrollbarTrackHorizontalEnabled, 
};

static UIImage *scrollbarButtonHorizontalBackgrounds[] = {
	&scrollbarButtonHorizontalNormal,
	&scrollbarButtonDisabled,
	&scrollbarButtonHorizontalHover,
	&scrollbarButtonHorizontalPressed,
	&scrollbarButtonVerticalNormal,
};

static UIImage *scrollbarButtonVerticalBackgrounds[] = {
	&scrollbarButtonVerticalNormal,
	&scrollbarButtonDisabled,
	&scrollbarButtonVerticalHover,
	&scrollbarButtonVerticalPressed,
	&scrollbarButtonVerticalNormal,
};

static UIImage *menuItemBackgrounds[] = {
	nullptr,
	nullptr,
	&menuItemHover,
	&menuItemDragged,
	&menuItemHover,
};

struct UIImage *toolbarItemBackgrounds[] = {
	nullptr,
	nullptr,
	&toolbarHover,
	&toolbarPressed,
	&toolbarNormal,
	nullptr,
	&toolbarHover,
	&toolbarPressed,
};

static UIImage *textboxBackgrounds[] = {
	&textboxNormal,
	&textboxDisabled,
	&textboxHover,
	&textboxFocus,
	&textboxFocus,
};

static UIImage *textboxNoBorderBackgrounds[] = {
	&textboxNoBorder,
	&textboxNoBorder,
	&textboxNoBorder,
	&textboxNoBorder,
	&textboxNoBorder,
};

static UIImage *textboxCommandBackgrounds[] = {
	&textboxCommand,
	&textboxDisabled,
	&textboxCommandHover,
	&textboxFocus,
	&textboxFocus,
};

static UIImage *buttonBackgrounds[] = {
	&buttonNormal,
	&buttonDisabled,
	&buttonHover,
	&buttonPressed,
	&buttonFocused,
};

static UIImage *buttonDefaultBackgrounds[] = {
	&buttonNormal,
	&buttonDisabled,
	&buttonHover,
	&buttonPressed,
	&buttonDefault,
};

static UIImage *buttonDangerousBackgrounds[] = {
	&buttonNormal,
	&buttonDisabled,
	&buttonDangerousHover,
	&buttonDangerousPressed,
	&buttonDangerousFocused,
};

static UIImage *buttonDangerousDefaultBackgrounds[] = {
	&buttonNormal,
	&buttonDisabled,
	&buttonDangerousHover,
	&buttonDangerousPressed,
	&buttonDangerousDefault,
};

struct UIImage *tabBandBackgrounds[] = {
	&tabBand,
	&tabBand,
	&tabBand,
	&tabBand,
	&tabBand,
};

static UIImage *windowBorder11[] = {&activeWindowBorder11, &inactiveWindowBorder11, &activeWindowBorder11, &activeWindowBorder11, &activeWindowBorder11};
static UIImage *windowBorder12[] = {&activeWindowBorder12, &inactiveWindowBorder12, &activeWindowBorder12, &activeWindowBorder12, &activeWindowBorder12};
static UIImage *windowBorder13[] = {&activeWindowBorder13, &inactiveWindowBorder13, &activeWindowBorder13, &activeWindowBorder13, &activeWindowBorder13};
static UIImage *windowBorder21[] = {&activeWindowBorder21, &inactiveWindowBorder21, &activeWindowBorder21, &activeWindowBorder21, &activeWindowBorder21};
static UIImage *windowBorder22[] = {&activeWindowBorder22, &inactiveWindowBorder22, &activeWindowBorder22, &activeWindowBorder22, &activeWindowBorder22};
static UIImage *windowBorder23[] = {&activeWindowBorder23, &inactiveWindowBorder23, &activeWindowBorder23, &activeWindowBorder23, &activeWindowBorder23};
static UIImage *windowBorder31[] = {&activeWindowBorder31, &inactiveWindowBorder31, &activeWindowBorder31, &activeWindowBorder31, &activeWindowBorder31};
static UIImage *windowBorder33[] = {&activeWindowBorder33, &inactiveWindowBorder33, &activeWindowBorder33, &activeWindowBorder33, &activeWindowBorder33};
static UIImage *windowBorder41[] = {&activeWindowBorder41, &inactiveWindowBorder41, &activeWindowBorder41, &activeWindowBorder41, &activeWindowBorder41};
static UIImage *windowBorder42[] = {&activeWindowBorder42, &inactiveWindowBorder42, &activeWindowBorder42, &activeWindowBorder42, &activeWindowBorder42};
static UIImage *windowBorder43[] = {&activeWindowBorder43, &inactiveWindowBorder43, &activeWindowBorder43, &activeWindowBorder43, &activeWindowBorder43};
                                                                                                                                
static UIImage *dialogBorder11[] = {&activeDialogBorder11, &inactiveDialogBorder11, &activeDialogBorder11, &activeDialogBorder11, &activeDialogBorder11};
static UIImage *dialogBorder12[] = {&activeDialogBorder12, &inactiveDialogBorder12, &activeDialogBorder12, &activeDialogBorder12, &activeDialogBorder12};
static UIImage *dialogBorder13[] = {&activeDialogBorder13, &inactiveDialogBorder13, &activeDialogBorder13, &activeDialogBorder13, &activeDialogBorder13};
static UIImage *dialogBorder21[] = {&activeDialogBorder21, &inactiveDialogBorder21, &activeDialogBorder21, &activeDialogBorder21, &activeDialogBorder21};
static UIImage *dialogBorder23[] = {&activeDialogBorder23, &inactiveDialogBorder23, &activeDialogBorder23, &activeDialogBorder23, &activeDialogBorder23};
static UIImage *dialogBorder31[] = {&activeDialogBorder31, &inactiveDialogBorder31, &activeDialogBorder31, &activeDialogBorder31, &activeDialogBorder31};
static UIImage *dialogBorder33[] = {&activeDialogBorder33, &inactiveDialogBorder33, &activeDialogBorder33, &activeDialogBorder33, &activeDialogBorder33};
static UIImage *dialogBorder41[] = {&activeDialogBorder41, &inactiveDialogBorder41, &activeDialogBorder41, &activeDialogBorder41, &activeDialogBorder41};
static UIImage *dialogBorder42[] = {&activeDialogBorder42, &inactiveDialogBorder42, &activeDialogBorder42, &activeDialogBorder42, &activeDialogBorder42};
static UIImage *dialogBorder43[] = {&activeDialogBorder43, &inactiveDialogBorder43, &activeDialogBorder43, &activeDialogBorder43, &activeDialogBorder43};

// Icons:

#define ICON16(x, y) {{x, x + 16, y, y + 16}, {x, x, y, y}}
#define ICON24(x, y) {{x, x + 24, y, y + 24}, {x, x, y, y}}
#define ICON32(x, y) {{x, x + 32, y, y + 32}, {x, x, y, y}}

static UIImage icons16[] = {
	{{}, {}},
	ICON16(237, 117),
	ICON16(220, 117),
	{{}, {}},
	ICON16(512 + 320, 208),
	ICON16(512 + 320, 160),
	ICON16(512 + 64, 192),
	ICON16(512 + 320, 288),
	ICON16(512 + 192, 16),
	ICON16(512 + 0, 432),
	ICON16(512 + 64, 288),
};

static UIImage icons32[] = {
	{{}, {}},
	{{}, {}},
	{{}, {}},
	ICON32(220, 135),
	{{}, {}},
	{{}, {}},
	{{}, {}},
	{{}, {}},
	{{}, {}},
	{{}, {}},
	{{}, {}},
	ICON32(512 + 0, 480),
	ICON32(512 + 32, 480),
	ICON32(512 + 64, 480),
	ICON32(512 + 96, 480),
};

