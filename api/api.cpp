#define OS_API
#define OS_CRT
#include "os.h"

#define Defer(x) OSDefer(x)

#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_STATIC
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "stb_image.h"

#if 0
#define STB_SPRINTF_IMPLEMENTATION
#define STB_SPRINTF_STATIC
#include "stb_sprintf.h"
#endif

#ifndef CF
#define CF(x) OS ## x
#endif

enum APIObjectType {
	API_OBJECT_WINDOW,
	API_OBJECT_GRID,
	API_OBJECT_CONTROL,
	API_OBJECT_INSTANCE,
};

struct APIObject {
	APIObjectType type;
	OSMessageCallback callback;
	APIObject *parent;
};

OSMutex osMessageMutex;

APIObject _osSystemMessages;
OSObject osSystemMessages;

uint64_t osSystemConstants[256];

static bool sendIdleMessages, draggingWindowResizeHandle;
static uint64_t lastIdleTimeStamp;

// Only repaint when the message queue is empty.
// #define MERGE_REPAINTS

#include "utf8.h"
#include "heap.cpp"
#include "linked_list.cpp"
#include "font.cpp"
#include "gui.cpp"
#include "common.cpp"
#include "syscall.cpp"
#include "crt.c"

extern "C" void ProgramEntry();

extern "C" void OSInitialiseAPI() {
	// TODO Seed random number generator.

	osSystemMessages = &_osSystemMessages;

	OSSyscall(OS_SYSCALL_GET_SYSTEM_CONSTANTS, (uintptr_t) osSystemConstants, 0, 0, 0);

	OSInitialiseGUI();
	InitialiseCStandardLibrary();

	OSMessage m = {};
	m.type = OS_MESSAGE_PROCESS_STARTED;
	OSPostMessage(&m);
}

extern "C" void _start() {
	// Call global constructors.
	void _init();
	_init();

	OSInitialiseAPI();
	ProgramEntry();
	OSTerminateThread(OS_CURRENT_THREAD);
}

OSCallbackResponse OSSendMessage(OSObject target, OSMessage *message) {
	APIObject *object = (APIObject *) target;
	if (!object) return OS_CALLBACK_NOT_HANDLED;
	if (!object->callback.function) return OS_CALLBACK_NOT_HANDLED;
	message->context = object->callback.context;
	return object->callback.function(target, message);
}

OSCallbackResponse OSForwardMessage(OSObject target, OSMessageCallback callback, OSMessage *message) {
	if (!callback.function) {
		return OS_CALLBACK_NOT_HANDLED;
	}

	message->context = callback.context;
	return callback.function(target, message);
}

void OSSendIdleMessages(bool enabled) {
	sendIdleMessages = enabled;
	lastIdleTimeStamp = OSProcessorReadTimeStamp();
}

void OSProcessMessages() {
	while (true) {
		OSMessage message;

		if (OSGetMessage(&message) == OS_SUCCESS) {
			goto gotMessage;
		} else {
			if (sendIdleMessages && !draggingWindowResizeHandle) {
				uint64_t timeStamp = OSProcessorReadTimeStamp();
				message.type = OS_MESSAGE_IDLE;
				message.idle.microsecondsSinceLastIdleMessage = 
					(timeStamp - lastIdleTimeStamp) / osSystemConstants[OS_SYSTEM_CONSTANT_TIME_STAMP_UNITS_PER_MICROSECOND];
				lastIdleTimeStamp = timeStamp;
				OSSendMessage(osSystemMessages, &message);
			}

			LinkedItem<Window> *item = repaintWindows.firstItem;

			while (item) {
				Window *window = item->thisItem;

				OSMessage message;
				message.type = OS_MESSAGE_PAINT;
				message.paint.clip = OS_MAKE_RECTANGLE(0, window->width, 0, window->height);
				message.paint.surface = window->surface;
				message.paint.force = false;
				OSSendMessage(window->root, &message);
				OSSyscall(OS_SYSCALL_UPDATE_WINDOW, window->window, 0, 0, 0);

				item = item->nextItem;
				window->repaintItem.RemoveFromList();
			}

			OSWaitMessage(OS_WAIT_NO_TIMEOUT);
		}

		if (OSGetMessage(&message) == OS_SUCCESS) {
			gotMessage:;
			OSAcquireMutex(&osMessageMutex);

			if (message.type == OS_MESSAGE_SYSTEM_CONSTANT_UPDATED) {
				osSystemConstants[message.systemConstantUpdated.index] = message.systemConstantUpdated.newValue;
				RefreshAllWindows();
			} else if (message.type == OS_MESSAGE_ISSUE_COMMAND) {
				// This is received from a [send data to parent] call.
				OSNotification notification;
				notification.type = OS_NOTIFICATION_RECEIVE_DATA_FROM_CHILD;
				notification.receiveData.dataBytes = message.issueCommand.nameBytes;
				notification.receiveData.data = OSHeapAllocate(message.issueCommand.nameBytes, false);
				if (notification.receiveData.dataBytes) OSReadConstantBuffer(message.issueCommand.nameBuffer, notification.receiveData.data);
				OSCloseHandle(message.issueCommand.nameBuffer);
				OSInstance *instance = (OSInstance *) message.context;
				notification.receiveData.instance = instance;
				OSSendNotification(nullptr, instance->notificationCallback, &notification, instance->parent);
				OSHeapFree(notification.receiveData.data);
			} else if (message.type == OS_MESSAGE_ISSUE_REQUEST) {
				OSInstance *instance = (OSInstance *) message.context;
				OSHandle handle = message.issueRequest.requestBuffer;
				size_t s = message.issueRequest.requestBytes;
				char *data = (char *) OSHeapAllocate(s, false);
				OSReadConstantBuffer(handle, data);
				message.type = OS_MESSAGE_PROCESS_REQUEST;
				message.processRequest.request = data;
				message.processRequest.requestBytes = s;
				message.processRequest.instance = instance;
				message.processRequest.response = nullptr;
				message.processRequest.responseBytes = 0;
				OSSendMessage(osSystemMessages, &message);
				// OSPrint("Response: %s\n", message.processRequest.responseBytes, message.processRequest.response);
				OSSyscall(OS_SYSCALL_SET_REQUEST_RESPONSE, instance->handle, (uintptr_t) message.processRequest.response, message.processRequest.responseBytes, 0);
				OSHeapFree(data);
				OSCloseHandle(handle);
			} else if (message.type == OS_MESSAGE_DESTROY_INSTANCE) {
				OSInstance *instance = (OSInstance *) message.argument;
				// OSPrint("closing instance handle\n");
				if (instance->handle) OSCloseHandle(instance->handle);
				if (!instance->foreign) OSDestroyCommands(instance->builtinCommands);
				if (instance->data) OSHeapFree(instance->data);
				OSHeapFree(instance);
			} else if (message.type == OS_MESSAGE_DESTROY_COMMANDS) {
				GUIFree(message.argument);
			} else if (message.context) {
				OSSendMessage(message.context, &message);
			} else {
				OSSendMessage(osSystemMessages, &message);
			}

			OSReleaseMutex(&osMessageMutex);
		}
	}
}

OSMessageCallback OSSetMessageCallback(OSObject generator, OSMessageCallback callback) {
	OSMessageCallback old;
	APIObject *object = (APIObject *) generator;
	old = object->callback;
	object->callback = callback;

	return old;
}

OSCallbackResponse OSSendNotification(OSObject generator, OSNotificationCallback callback, OSNotification *notification, OSObject instance) {
	if (!callback.function) {
		return OS_CALLBACK_NOT_HANDLED;
	}

	notification->context = callback.context;
	notification->generator = generator;
	notification->instance = instance;
	notification->instanceContext = ((OSInstance *) instance)->argument;

	return callback.function(notification);
}
