// TODO Separate this into another library from the API.

int _rename(const char *oldPath, const char *newPath) {
	size_t oldPathBytes = OSCStringLength(oldPath);
	const char *newFileName = newPath + OSCStringLength(newPath);

	while (true) {
		newFileName--;
		if (*newFileName == '/') {
			newFileName++;
			break;
		}
	}

	size_t newFileNameBytes = OSCStringLength(newFileName);
	size_t newPathBytes = OSCStringLength(newPath) - newFileNameBytes;

	OSNodeInformation oldNode, newDirectory;

	if (OSOpenNode((char *) oldPath, oldPathBytes, OS_OPEN_NODE_FAIL_IF_NOT_FOUND, &oldNode) != OS_SUCCESS) {
		return -1;
	}

	if (OSOpenNode((char *) newPath, newPathBytes, OS_OPEN_NODE_FAIL_IF_NOT_FOUND | OS_OPEN_NODE_DIRECTORY, &newDirectory) != OS_SUCCESS) {
		OSCloseHandle(oldNode.handle);
		return -1;
	}

	if (OSMoveNode(oldNode.handle, newDirectory.handle, (char *) newFileName, newFileNameBytes) != OS_SUCCESS) {
		OSCloseHandle(oldNode.handle);
		OSCloseHandle(newDirectory.handle);
		return -1;
	}

	OSCloseHandle(oldNode.handle);
	OSCloseHandle(newDirectory.handle);
	return 0;
}

#define __NEED_struct_iovec
#define __NEED_sigset_t
#define __NEED_struct_timespec
#define __NEED_time_t
#define _POSIX_SOURCE
#define _GNU_SOURCE
#include "../ports/musl/obj/include/bits/syscall.h"
#include "../ports/musl/obj/include/bits/alltypes.h"

#include <signal.h>
#include <sys/sysinfo.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <bits/ioctl.h>
#include <fcntl.h>
#include <dirent.h>
#include <unistd.h>
#include <poll.h>
#include <sys/utsname.h>
#include <errno.h>

char currentWorkingDirectory[4096];

struct sigaltstack signalHandlerStack;

extern "C" int __libc_start_main(int (*main)(int,char **,char **), int argc, char **argv);

// TODO Proper values?
static char *argv[] = {
	(char *) "/__prefix/ProgramName.esx",
	(char *) "LANG=en_US.UTF-8",
	(char *) "PWD=/__prefix",
	(char *) "HOME=/__user_storage",
	(char *) "PATH=/__posix_bin",
	(char *) nullptr,
};

// HACK HACK HACK HACK
#define GET_POSIX_FILE(fildes) ((PosixFile *) ((intptr_t) fildes | 0x100000000000))

struct PosixFile {
	OSHandle handle;
	uint64_t offset;
	bool seekToEndBeforeWrite;
	char path[4096];
	long flags;
	OSDirectoryChild *children;
	size_t childCount;
};

static void InitialiseCStandardLibrary() {
	// OSPrint("starting musl libc\n");
	strcpy(currentWorkingDirectory, "");
	__libc_start_main(nullptr, 1, argv);
}

static char *FixFilename(char *filename) {
	// TODO Thread safety.
	static char fixedFilename[4096];

	char *result;

	if (0 == memcmp(filename, OSLiteral("/__prefix/"))) {
		result = filename + OSCStringLength("/__prefix/");
	} else if (0 == memcmp(filename, OSLiteral("/__user_storage/"))) {
		result = filename + OSCStringLength("/__user_storage/");
	} else if (0 == memcmp(filename, OSLiteral("/usr/local/lib/tcc/"))) {
		result = filename + OSCStringLength("/usr/local/lib/tcc/");
	} else if (0 == memcmp(filename, OSLiteral("/usr/include"))) {
		strcpy(fixedFilename, "/Programs/C Standard Library/Headers");
		strcat(fixedFilename, filename + OSCStringLength("/usr/include"));
		result = (fixedFilename);
	} else if (0 == memcmp(filename, OSLiteral("/usr/lib64"))) {
		strcpy(fixedFilename, "/Programs/C Standard Library");
		strcat(fixedFilename, filename + OSCStringLength("/usr/lib64"));
		result = (fixedFilename);
	} else if (0 == strcmp(filename, "/__prefix")) {
		result = (char *) "";
	} else if (0 == strcmp(filename, "/__user_storage")) {
		result = (char *) "";
	} else if (0 == memcmp(filename, OSLiteral("./"))) {
		strcpy(fixedFilename, currentWorkingDirectory);
		int x = currentWorkingDirectory[strlen(currentWorkingDirectory) - 1] == '/' ? 1 : 0;
		strcat(fixedFilename, filename + OSCStringLength(".") + x);
		result = (fixedFilename);
	} else if (0 == strcmp(filename, ".")) {
		result = (currentWorkingDirectory);
	} else if (filename[0] != '/') {
		strcpy(fixedFilename, currentWorkingDirectory);
		int x = currentWorkingDirectory[strlen(currentWorkingDirectory) - 1] == '/' ? 1 : 0;
		if (!x && currentWorkingDirectory[0]) strcat(fixedFilename, "/");
		strcat(fixedFilename, filename);
		result = (fixedFilename);
	} else {
		result = filename;
	}

	// OSPrint("~~~~~~~~FIX %z -> %z\n", filename, result);

	return result;
}

void PrintOutput(char *string, size_t length) {
#if 0
	OSPrint("Print output (%d):\n", length);

	for (uintptr_t i = 0; i < length; i++) {
		OSPrint("\t%d: %X, %c\n", i, string[i], string[i]);
	}
#else
#if 1
	OSPrintDirect(string, length);
#else
	OSMessage m;
	m.type = OS_MESSAGE_PRINT_OUTPUT;
	m.printOutput.string = string;
	m.printOutput.length = length;
	OSSendMessage(osSystemMessages, &m);
#endif
#endif
}

long _OSMakeLinuxSystemCall(long n, long a1, long a2, long a3, long a4, long a5, long a6) {
	switch (n) {
		case SYS_mmap: {
			if (a1 == 0 && a2 && a3 == 3 && a4 == 34 && a5 == -1 && !a6) {
				return (long) OSHeapAllocate(a2, true);
			} else {
				OSPrint("Unsupported mmap [%x, %x, %x, %x, %x, %x]\n", n, a1, a2, a3, a4, a5, a6);
				OSCrashProcess(OS_FATAL_ERROR_UNKNOWN_SYSCALL);
			}
		} break;

		case SYS_munmap: {
			// Warning: we free the whole block regardless of a2
			OSHeapFree((void *) a1);
			return 0;
		} break;

		case SYS_brk: {
			return -1;
		} break;

		case SYS_mremap: {
			void *old = (void *) a1;
			size_t oldSize = a2;
			size_t newSize = a3;

			void *replacement = OSHeapAllocate(newSize, true);
			OSCopyMemory(replacement, old, oldSize);
			OSHeapFree(old);

			return (long) replacement;
		} break;

		case SYS_clock_gettime: {
			struct timespec *tp = (struct timespec *) a2;
			uint64_t microseconds = OSProcessorReadTimeStamp() / osSystemConstants[OS_SYSTEM_CONSTANT_TIME_STAMP_UNITS_PER_MICROSECOND];
			tp->tv_sec = microseconds / 1000;
			tp->tv_nsec = (microseconds * 1000) % 1000000000;
			return 0;
		} break;

		case SYS_arch_prctl: {
			if (a2 == 0x1002) {
				OSSetThreadLocalStorageAddress((void *) a1);
				return 0;
			} else {
				OSPrint("Unsupported arch_prctl [%x, %x, %x, %x, %x, %x]\n", n, a1, a2, a3, a4, a5, a6);
				OSCrashProcess(OS_FATAL_ERROR_UNKNOWN_SYSCALL);
			}
		} break;

		case SYS_set_tid_address: {
			// Warning: we're also supposed to set some kernel variable??
			return OSGetThreadID(OS_CURRENT_THREAD);
		} break;

		case SYS_ioctl: {
			if ((a1 == 0 || a1 == 1 || a1 == 2) && a2 == 21523) {
				// TIOCGWINSZ - get terminal size
				struct winsize *size = (struct winsize *) a3;

				OSMessage m;
				m.type = OS_MESSAGE_GET_TERMINAL_DIMENSIONS;
				OSSendMessage(osSystemMessages, &m);

				size->ws_row = m.getTerminalDimensions.rows;
				size->ws_col = m.getTerminalDimensions.columns;
				size->ws_xpixel = 800;
				size->ws_ypixel = 800;
				return 0;
			}
		} break;

		case SYS_read: {
			if (!a3) return 0;

			if (a1 == 0) {
				// OSPrint("read stdin, max %d\n", a3);
				OSMessage m;
				m.type = OS_MESSAGE_GET_STDIN_BUFFER;
				OSSendMessage(osSystemMessages, &m);
				memcpy((void *) a2, m.getStdinBuffer.string, m.getStdinBuffer.length);
				// OSPrint("\tread %d\n", m.getStdinBuffer.length);
				// for (uintptr_t i = 0; i < m.getStdinBuffer.length; i++) OSPrint("\t%X\n", m.getStdinBuffer.string[i]);
				return m.getStdinBuffer.length;
			} else if (a1 > 2) {
				PosixFile *file = GET_POSIX_FILE(a1);
				uint64_t read = 0;

				// OSPrint("read, %x %d %d %x\n", a1, file->offset, a3, a2);

				intptr_t x = OSReadFileSync(file->handle, file->offset, a3, (void *) a2); 
				if (x >= 0) read += x;
				file->offset += a3;

				return read;
			} else {
				OSPrint("Unsupported read %d [%x, %x, %x, %x, %x, %x]\n", n, a1, a2, a3, a4, a5, a6);
				OSCrashProcess(OS_FATAL_ERROR_UNKNOWN_SYSCALL);
			}
		} break;

		case SYS_access: {
			char *filename = FixFilename((char *) a1);
			OSNodeInformation node;
			OSError error = OSOpenNode(filename, strlen(filename), OS_OPEN_NODE_FAIL_IF_NOT_FOUND, &node);
			if (error == OS_SUCCESS) OSCloseHandle(node.handle);
			return error == OS_SUCCESS ? 0 : -1;
		} break;

		case SYS_write: {
			if (!a3) return 0;

			if (a1 == 1 || a1 == 2) {
				PrintOutput((char *) a2, a3);
				return a3;
			} else if (a1 > 2) {
				PosixFile *file = GET_POSIX_FILE(a1);
				uint64_t written = 0;

				if (file->seekToEndBeforeWrite) {
					OSNodeInformation node;
					node.handle = file->handle;
					OSRefreshNodeInformation(&node);
					file->offset = node.fileSize;
				}

				// OSPrint("write, %x %d %d %x\n", a1, file->offset, a3, a2);

				intptr_t x = OSWriteFileSync(file->handle, file->offset, a3, (void *) a2); 
				if (x >= 0) written += x;
				file->offset += a3;

				return written;
			} else {
				OSPrint("Unsupported write %d [%x, %x, %x, %x, %x, %x]\n", n, a1, a2, a3, a4, a5, a6);
				OSCrashProcess(OS_FATAL_ERROR_UNKNOWN_SYSCALL);
			}
		} break;

		case SYS_writev: {
			if (a1 == 1 || a1 == 2) {
				struct iovec *buffers = (struct iovec *) a2;
				size_t s = 0;

				for (intptr_t i = 0; i < a3; i++) {
					if (!buffers[i].iov_len) continue;
					PrintOutput((char *) buffers[i].iov_base, buffers[i].iov_len);
					s += buffers[i].iov_len;
				}

				return s;
			} else if (a1 > 2) {
				PosixFile *file = GET_POSIX_FILE(a1);
				uint64_t written = 0;

				if (file->seekToEndBeforeWrite) {
					OSNodeInformation node;
					node.handle = file->handle;
					OSRefreshNodeInformation(&node);
					file->offset = node.fileSize;
				}

				struct iovec *buffers = (struct iovec *) a2;

				for (intptr_t i = 0; i < a3; i++) {
					if (!buffers[i].iov_len) continue;
					// OSPrint("write(v), %x %d %d %x\n", a1, file->offset, buffers[i].iov_len, buffers[i].iov_base);
					intptr_t x = OSWriteFileSync(file->handle, file->offset, buffers[i].iov_len, buffers[i].iov_base); 
					if (x < 0) break;
					written += x;
					file->offset += buffers[i].iov_len;

#if 0
					for (uintptr_t j = 0; j < buffers[i].iov_len; j++) {
						OSPrint("%X, ", ((uint8_t *) buffers[i].iov_base)[j]);
					}

					OSPrint("\n");
#endif
				}

				return written;
			} else {
				OSPrint("Unsupported writev %d [%x, %x, %x, %x, %x, %x]\n", n, a1, a2, a3, a4, a5, a6);
				OSCrashProcess(OS_FATAL_ERROR_UNKNOWN_SYSCALL);
			}
		} break;

		case SYS_sigaltstack: {
			if (a1 && !a2) {
				struct sigaltstack *ss = (struct sigaltstack *) a1;
				signalHandlerStack = *ss;
				return 0;
			} else {
				OSPrint("Unsupported sigaltstack [%x, %x, %x, %x, %x, %x]\n", n, a1, a2, a3, a4, a5, a6);
				OSCrashProcess(OS_FATAL_ERROR_UNKNOWN_SYSCALL);
			}
		} break;

		case SYS_getcwd: {
			if (strlen(currentWorkingDirectory) + 32 > (size_t) a2) {
				return NULL;
			} else {
				if (currentWorkingDirectory[0] == '/') {
					strcpy((char *) a1, "");
				} else {
					strcpy((char *) a1, "/__prefix/");
				}

				strcat((char *) a1, currentWorkingDirectory);
				return a1;
			}
		} break;

		case SYS_chdir: {
			strcpy(currentWorkingDirectory, FixFilename((char *) a1));
			// OSPrint("set cwd to %z\n", currentWorkingDirectory);
		} break;

		case SYS_prlimit64: {
			if (!a1 && a2 == RLIMIT_DATA && !a3 && a4) {
				struct rlimit *limit = (struct rlimit *) a4;
				limit->rlim_cur = limit->rlim_max = RLIM_INFINITY;
				return 0;
			} else {
				OSPrint("Unsupported prlimit64 [%x, %x, %x, %x, %x, %x]\n", n, a1, a2, a3, a4, a5, a6);
				OSCrashProcess(OS_FATAL_ERROR_UNKNOWN_SYSCALL);
			}
		} break;

		case SYS_getuid: {
			return 10;
		} break;

		case SYS_sysinfo: {
			struct sysinfo *info = (struct sysinfo *) a1;

			OSSystemInformation systemInformation;
			OSGetSystemInformation(&systemInformation);

			// Warning: this is mostly made up
			info->uptime = 1;
			info->loads[0] = 1;
			info->loads[1] = 1;
			info->loads[2] = 1;
			info->totalram = systemInformation.totalMemory;
			info->freeram = systemInformation.freeMemory;
			info->sharedram = 0;
			info->bufferram = 0;
			info->totalswap = 0;
			info->freeswap = 0;
			info->procs = systemInformation.processCount;
			info->totalhigh = info->totalram;
			info->freehigh = info->freeram;
			info->mem_unit = 1;

			return 0;
		} break;

		case SYS_fstat: {
			PosixFile *file = GET_POSIX_FILE(a1);
			struct stat *buffer = (struct stat *) a2;
			OSNodeInformation node;
			node.handle = file->handle;
			OSRefreshNodeInformation(&node);

			buffer->st_mode = node.type == OS_NODE_DIRECTORY ? S_IFDIR : S_IFREG;
			buffer->st_dev = 0;
			buffer->st_ino = 0;
			buffer->st_nlink = 1;
			buffer->st_mode |= S_IRWXU | S_IRWXG | S_IRWXO;
			buffer->st_uid = 10;
			buffer->st_gid = 10;
			buffer->st_rdev = 0;
			buffer->st_size = node.fileSize;
			buffer->st_atime = 0;
			buffer->st_mtime = 0;
			buffer->st_ctime = 0;
			buffer->st_blksize = 4096;
			buffer->st_blocks = (node.fileSize + 4095) / 4096;

			return 0;
		} break;

		case SYS_lstat:
		case SYS_stat: {
			char *filename = FixFilename((char *) a1);
			struct stat *buffer = (struct stat *) a2;

			OSZeroMemory(buffer, sizeof(struct stat));

			OSError error;
			OSNodeInformation node;

			// OSPrint("attempting to stat %z (%z)\n", filename, (char *) a1);

			error = OSOpenNode(filename, strlen(filename), OS_OPEN_NODE_FAIL_IF_NOT_FOUND, &node);

			if (error == OS_ERROR_INCORRECT_NODE_TYPE) {
				error = OSOpenNode(filename, strlen(filename), OS_OPEN_NODE_FAIL_IF_NOT_FOUND | OS_OPEN_NODE_DIRECTORY, &node);
				buffer->st_mode = S_IFDIR;
			} else {
				buffer->st_mode = node.type == OS_NODE_DIRECTORY ? S_IFDIR : S_IFREG;
			}

			if (error == OS_SUCCESS) {
				OSCloseHandle(node.handle);

				buffer->st_dev = 0;
				buffer->st_ino = 0;
				buffer->st_nlink = 1;
				buffer->st_mode |= S_IRWXU | S_IRWXG | S_IRWXO;
				buffer->st_uid = 10;
				buffer->st_gid = 10;
				buffer->st_rdev = 0;
				buffer->st_size = node.fileSize;
				buffer->st_atime = 0;
				buffer->st_mtime = 0;
				buffer->st_ctime = 0;
				buffer->st_blksize = 4096;
				buffer->st_blocks = (node.fileSize + 4095) / 4096;

				// OSPrint("stat %z\n", a1);

				return 0;
			} else {
				// OSPrint("couldn't stat %z (%z)\n", a1, filename);
				return -1;
			}
		} break;

		case SYS_rt_sigaction:
		case SYS_rt_sigprocmask: {
			// OSPrint("Warning, ignoring signal syscall %d [%x, %x, %x, %x, %x, %x]\n", n, a1, a2, a3, a4, a5, a6);
			return 0;
		} break;

		case SYS_socket: {
			return -1;
		} break;

		case SYS_chmod: {
			return 0;
		} break;

		case SYS_open: {
			char *filename = FixFilename((char *) a1);
			unsigned flags = 0;

			if ((a2 & O_ACCMODE) == O_RDONLY) flags |= OS_OPEN_NODE_READ_ACCESS;
			if ((a2 & O_ACCMODE) == O_RDWR) flags |= OS_OPEN_NODE_READ_ACCESS | OS_OPEN_NODE_WRITE_ACCESS | OS_OPEN_NODE_RESIZE_ACCESS;
			if ((a2 & O_ACCMODE) == O_WRONLY) flags |= OS_OPEN_NODE_WRITE_ACCESS | OS_OPEN_NODE_RESIZE_ACCESS;

			if ((a2 & O_CREAT) && (a2 & O_EXCL)) flags |= OS_OPEN_NODE_FAIL_IF_FOUND;
			if (!(a2 & O_CREAT)) flags |= OS_OPEN_NODE_FAIL_IF_NOT_FOUND;

			if (a2 & O_DIRECTORY) flags |= OS_OPEN_NODE_DIRECTORY;
			if (a2 & O_TRUNC) flags |= OS_OPEN_NODE_RESIZE_ACCESS;

			// OSPrint("attempting to open %z (%z) with flags %x\n", filename, (char *) a1, flags);
			OSNodeInformation node;
			OSError error = OSOpenNode(filename, OSCStringLength(filename), flags, &node);
			// OSPrint("\terror = %x\n", error);

			if (error != OS_SUCCESS) {
				// OSPrint("couldn't open %z [%x; %z; %d]\n", (char *) a1, flags, filename, error);
				return -1;
			} else {
				PosixFile *file = (PosixFile *) OSHeapAllocate(sizeof(PosixFile), true);

				// HACK HACK HACK HACK
				int fildes = (int) ((intptr_t) file & ~0x100000000000);

				if ((void *) ((intptr_t) fildes | 0x100000000000) != file) {
					// *sigh*
					OSHeapFree(file);
					OSCloseHandle(node.handle);
					OSPrint("\tcan't represent as posix file\n");
					return -1;
				}

				file->handle = node.handle;
				file->seekToEndBeforeWrite = a2 & O_APPEND;
				file->offset = 0;
				file->flags = a2;
				strcpy(file->path, filename);
				file->childCount = node.directoryChildren;

				if (a2 & O_TRUNC) {
					OSResizeFile(file->handle, 0); 
				}

				// OSPrint("\tsuccess!\n");
				// OSPrint("opened %z for %x\n", (char *) a1, fildes);

				return fildes;
			}
		} break;

		case SYS_fchdir: {
			strcpy(currentWorkingDirectory, GET_POSIX_FILE(a1)->path);
			// OSPrint("set cwd to %z\n", currentWorkingDirectory);
			return 0;
		} break;

		case SYS_close: {
			PosixFile *file = GET_POSIX_FILE(a1);
			OSCloseHandle(file->handle);
			OSHeapFree(file->children);
			OSHeapFree(file);
			return 0;
		} break;

		case SYS_fcntl: {
			PosixFile *file = GET_POSIX_FILE(a1);

			if (a2 == F_GETFD) {
				return file->flags;
			} else if (a2 == F_SETFD && a3 == FD_CLOEXEC) {
				// ignore.
				return 0;
			} else {
				OSPrint("Unsupported fcntl [%x, %x, %x, %x, %x, %x]\n", n, a1, a2, a3, a4, a5, a6);
				OSCrashProcess(OS_FATAL_ERROR_UNKNOWN_SYSCALL);
			}
		} break;

		case SYS_readv: {
			if (a1 > 2) {
				PosixFile *file = GET_POSIX_FILE(a1);
				struct iovec *buffers = (struct iovec *) a2;
				uint64_t read = 0;

				for (intptr_t i = 0; i < a3; i++) {
					if (!buffers[i].iov_len) continue;
					// OSPrint("read(v), %x %d %d %x\n", a1, file->offset, buffers[i].iov_len, buffers[i].iov_base);
					intptr_t x = OSReadFileSync(file->handle, file->offset, buffers[i].iov_len, buffers[i].iov_base); 
					if (x < 0) break;
					read += x;
					file->offset += buffers[i].iov_len;
				}

				return read;
			} else {
				OSPrint("Unsupported readv [%x, %x, %x, %x, %x, %x]\n", n, a1, a2, a3, a4, a5, a6);
				OSCrashProcess(OS_FATAL_ERROR_UNKNOWN_SYSCALL);
			}
		} break;

		case SYS_madvise: {
			// Ignore.
			// It's only "advice" after all, right?
			return 0;
		} break;

		case SYS_getdents64: {
			PosixFile *file = GET_POSIX_FILE(a1);

			if (!file->children) {
				file->children = (OSDirectoryChild *) OSHeapAllocate(sizeof(OSDirectoryChild) * (file->childCount + 32), true);

				if (OS_SUCCESS != OSEnumerateDirectoryChildren(file->handle, file->children, file->childCount + 32)) {
					return -1;
				}
			}

			struct dirent *buffer = (struct dirent *) a2;
			size_t spaceLeft = a3;
			
			while (true) {
				OSDirectoryChild *child = file->children + file->offset;

				if (!child->information.present) {
					return a3 - spaceLeft;
				}

				size_t s = sizeof(struct dirent) + child->nameLengthBytes;

				// OSPrint("read dirent %s\n", child->nameLengthBytes, child->name);

				if (spaceLeft <= s) {
					return a3 - spaceLeft;
				}

				spaceLeft -= s;
				file->offset++;

				buffer->d_ino = 0;
				buffer->d_off = buffer->d_reclen = s;
				buffer->d_type = child->information.type == OS_NODE_DIRECTORY ? DT_DIR : DT_REG;
				memcpy(buffer->d_name, child->name, child->nameLengthBytes);

				buffer = (struct dirent *) ((char *) buffer + s);
			}
		} break;

		case SYS_ftruncate: {
			PosixFile *file = GET_POSIX_FILE(a1);

			if (OS_SUCCESS == OSResizeFile(file->handle, a2)) return 0;
			else return -1;
		} break;

		case SYS_fsync: {
			return 0;
		} break;

		case SYS_lseek: {
			PosixFile *file = GET_POSIX_FILE(a1);

			if (a3 == SEEK_SET) {
				file->offset = a2;
			} else if (a3 == SEEK_CUR) {
				file->offset += a2;
			} else if (a3 == SEEK_END) {
				OSNodeInformation node;
				node.handle = file->handle;
				OSRefreshNodeInformation(&node);
				file->offset = node.fileSize + a2;
			}

			return file->offset;
		} break;

		case SYS_uname: {
			struct utsname *name = (struct utsname *) a1;
			strcpy(name->sysname, "essence");
			strcpy(name->nodename, "node");
			strcpy(name->release, "indev");
			strcpy(name->machine, "machine");
			return 0;
		} break;

		case SYS_getpid: {
			return OSGetProcessID(OS_CURRENT_PROCESS);
		} break;

		case SYS_poll: {
			struct pollfd *fds = (struct pollfd *) a1;

			if (a2 == 1 && fds[0].fd == 0 /*stdin*/ && fds[0].events == POLL_IN) {
				// OSPrint("poll stdin with timeout %d\n", a3);

				// Wait until data on stdin is available.
				OSMessage m;
				m.type = OS_MESSAGE_WAIT_STDIN;
				m.waitStdin.timeoutMs = a3 < 0 ? OS_WAIT_NO_TIMEOUT : a3;
				OSSendMessage(osSystemMessages, &m);

				if (m.waitStdin.timeoutMs) {
					return 0;
				} else {
					fds[0].revents = POLL_IN;
					return 1;
				}
			} else {
				// TODO Support polling on other file descriptors.
				return -1;
			}
		} break;

		case SYS_exit_group: {
			OSCrashProcess(a1 + 1000);
		} break;

		case SYS_nanosleep: {
			const struct timespec *sleep = (const struct timespec *) a1;
			uint64_t milliseconds = sleep->tv_sec * 1000 + sleep->tv_nsec / 1000000;
			if (!milliseconds) milliseconds = 1;
			OSSleep(milliseconds);
			return 0;
		} break;

		case SYS_readlink: {
			// We don't have symbolic links (yet)
			return -EINVAL;
		} break;

		case SYS_unlink: {
			// We don't have symbolic links (yet)
			return -EACCES;
		} break;

		case SYS_rename: {
			return _rename((char *) a1, (char *) a2);
		} break;

		default: {
			OSPrint("Unknown linux syscall %d [%x, %x, %x, %x, %x, %x]\n", n, a1, a2, a3, a4, a5, a6);
			OSCrashProcess(OS_FATAL_ERROR_UNKNOWN_SYSCALL);
		} break;
	}

	return -1;
}

extern "C" long OSMakeLinuxSystemCall(long n, long a1, long a2, long a3, long a4, long a5, long a6) {
	// OSPrint("-> linux syscall %d [%x, %x, %x, %x, %x, %x]\n", n, a1, a2, a3, a4, a5, a6);
	long result = _OSMakeLinuxSystemCall(n, a1, a2, a3, a4, a5, a6);
	// OSPrint("<- result %x\n", result);
	return result;
}
