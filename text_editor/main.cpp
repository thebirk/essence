#include "../api/os.h"

#define OS_MANIFEST_DEFINITIONS
#include "../bin/Programs/Text Editor/manifest.h"

#define WINDOW_NOTIFICATION (-1)
#define TEXTBOX_NOTIFICATION (-2)
#define INSTANCE_NOTIFICATION (-3)
#define FIND_TEXTBOX_NOTIFICATION (-4)
#define REPLACE_TEXTBOX_NOTIFICATION (-5)

struct Instance {
	OSObject window, textbox, 
		 findDialog, replaceDialog,
		 findTextbox, replaceTextbox;
	OSString findString, replaceString;
	OSCommand *commands;
};

OSCallbackResponse ProcessNotification(OSNotification *notification) {
	bool isCommand = notification->type == OS_NOTIFICATION_COMMAND;
	Instance *instance = (Instance *) notification->instanceContext;

	switch ((intptr_t) notification->context) {
		case commandFind: {
			if (isCommand) {
				if (instance->findDialog) {
					OSSetFocusedWindow(instance->findDialog);
					return OS_CALLBACK_HANDLED;
				}

				if (instance->replaceDialog) {
					OSCloseWindow(instance->replaceDialog);
					instance->replaceDialog = nullptr;
				}

				OSStartGUIAllocationBlock(16384);

				instance->findDialog = OSCreateDialog(notification->instance, nullptr, dialogFind);

				OSObject defaultButton;
				
				OSGenerateContentsOf_dialogFind(instance->findDialog);

				OSSetFocusedControl(defaultButton, false);
				OSSetFocusedControl(instance->findTextbox, false);

				OSSetObjectNotificationCallback(instance->findTextbox, OS_MAKE_NOTIFICATION_CALLBACK(ProcessNotification, (intptr_t) FIND_TEXTBOX_NOTIFICATION));

				OSSetCommandNotificationCallback(OSGetDialogCommands(instance->findDialog) + osDialogStandardCancel, 
						OS_MAKE_NOTIFICATION_CALLBACK(ProcessNotification, (intptr_t) commandCloseDialog));

				OSSetText(instance->findTextbox, instance->findString.buffer, instance->findString.bytes, OS_RESIZE_MODE_IGNORE);

				OSEndGUIAllocationBlock();

				return OS_CALLBACK_HANDLED;
			}
		} break;

		case commandReplace: {
			if (isCommand) {
				if (instance->replaceDialog) {
					OSSetFocusedWindow(instance->replaceDialog);
					return OS_CALLBACK_HANDLED;
				}

				if (instance->findDialog) {
					OSCloseWindow(instance->findDialog);
					instance->findDialog = nullptr;
				}

				OSStartGUIAllocationBlock(16384);

				instance->replaceDialog = OSCreateDialog(notification->instance, nullptr, dialogReplace);

				OSObject defaultButton;

				OSGenerateContentsOf_dialogReplace(instance->replaceDialog);

				OSSetFocusedControl(defaultButton, false);
				OSSetFocusedControl(instance->findTextbox, false);

				OSSetObjectNotificationCallback(instance->findTextbox, OS_MAKE_NOTIFICATION_CALLBACK(ProcessNotification, (intptr_t) FIND_TEXTBOX_NOTIFICATION));
				OSSetObjectNotificationCallback(instance->replaceTextbox, OS_MAKE_NOTIFICATION_CALLBACK(ProcessNotification, (intptr_t) REPLACE_TEXTBOX_NOTIFICATION));

				OSSetCommandNotificationCallback(OSGetDialogCommands(instance->replaceDialog) + osDialogStandardCancel, 
						OS_MAKE_NOTIFICATION_CALLBACK(ProcessNotification, (intptr_t) commandCloseDialog));

				OSSetText(instance->findTextbox, instance->findString.buffer, instance->findString.bytes, OS_RESIZE_MODE_IGNORE);
				OSSetText(instance->replaceTextbox, instance->replaceString.buffer, instance->replaceString.bytes, OS_RESIZE_MODE_IGNORE);

				OSEndGUIAllocationBlock();

				return OS_CALLBACK_HANDLED;
			}
		} break;

		case commandCloseDialog: {
			if (isCommand) {
				if (instance->replaceDialog) {
					OSCloseWindow(instance->replaceDialog);
					instance->replaceDialog = nullptr;
				}

				if (instance->findDialog) {
					OSCloseWindow(instance->findDialog);
					instance->findDialog = nullptr;
				}

				return OS_CALLBACK_HANDLED;
			}
		} break;

		case commandReplaceAll:
		case commandReplaceNext:
		case commandFindPrevious:
		case commandFindNext: {
			if (isCommand) {
				OSString text;
				OSGetText(instance->textbox, &text);

				bool firstPass = true;
				bool matchCase = OSGetCommandCheck(instance->commands + commandMatchCase);
				bool foundMatch = false;
				bool backwards = (intptr_t) notification->context == commandFindPrevious;
				bool replace = (intptr_t) notification->context == commandReplaceNext || (intptr_t) notification->context == commandReplaceAll;
				bool stopAfterMatch = (intptr_t) notification->context != commandReplaceAll;

				OSString query = instance->findString;
				OSString replacement = instance->replaceString;

				uintptr_t byte, byte2, start;
				OSTextboxGetSelection(instance->textbox, &byte, &byte2); 
				if (!backwards && byte2 > byte) byte = byte2;
				else if (backwards && byte > byte2) byte = byte2;
				if (backwards) byte--;
				start = byte;
				
				while (true) {
					if (!backwards) {
						if (firstPass && byte + query.bytes > text.bytes) {
							firstPass = false;
							byte = 0;
						} 

						if (!firstPass && (byte > start || byte + query.bytes > text.bytes)) {
							break;
						}
					} else {
						if (firstPass && byte > text.bytes) {
							firstPass = false;
							byte = text.bytes - query.bytes;
						} 

						if (!firstPass && (byte <= start || byte > text.bytes)) {
							break;
						}
					}

					bool fail = false;

					for (uintptr_t j = 0; j < query.bytes; j++) {
						uint8_t a = query.buffer[j];
						uint8_t b = text.buffer[j + byte];

						if (!matchCase) {
							if (a >= 'a' && a <= 'z') a += 'A' - 'a';
							if (b >= 'a' && b <= 'z') b += 'A' - 'a';
						} 

						if (a != b) {
							fail = true;
							break;
						}
					}

					if (!fail) {
						foundMatch = true;
						OSTextboxSetSelection(instance->textbox, byte, byte + query.bytes); 

						if (replace) {
							OSTextboxRemove(instance->textbox);
							OSTextboxInsert(instance->textbox, replacement.buffer, replacement.bytes);
						}

						if (stopAfterMatch) {
							break;
						}
					}

					if (!backwards) byte++; else byte--;
				}

				if (!foundMatch && stopAfterMatch) {
					OSShowDialogAlert(OSLiteral("Find"),
							OSLiteral("The find query could not be found in the current document."),
							OSLiteral("Make sure that the query is correctly spelt."),
							notification->instance,
							OS_ICON_WARNING, instance->window);
				}

				return OS_CALLBACK_HANDLED;
			}
		} break;

		case FIND_TEXTBOX_NOTIFICATION: {
			if (notification->type == OS_NOTIFICATION_MODIFIED) {
				OSString text;
				OSGetText(instance->findTextbox, &text);
				OSHeapFree(instance->findString.buffer);
				instance->findString.buffer = (char *) OSHeapAllocate(text.bytes, false);
				OSCopyMemory(instance->findString.buffer, text.buffer, text.bytes);
				instance->findString.bytes = text.bytes;

				OSEnableCommand(instance->commands + commandFindNext, instance->findString.bytes);
				OSEnableCommand(instance->commands + commandFindPrevious, instance->findString.bytes);
				OSEnableCommand(instance->commands + commandReplaceNext, instance->findString.bytes);
				OSEnableCommand(instance->commands + commandReplaceAll, instance->findString.bytes);
			}
		} break;

		case REPLACE_TEXTBOX_NOTIFICATION: {
			if (notification->type == OS_NOTIFICATION_MODIFIED) {
				OSString text;
				OSGetText(instance->replaceTextbox, &text);
				OSHeapFree(instance->replaceString.buffer);
				instance->replaceString.buffer = (char *) OSHeapAllocate(text.bytes, false);
				OSCopyMemory(instance->replaceString.buffer, text.buffer, text.bytes);
				instance->replaceString.bytes = text.bytes;
				OSEnableCommand(instance->commands + commandReplaceNext, instance->findString.bytes);
				OSEnableCommand(instance->commands + commandReplaceAll, instance->findString.bytes);
			}
		} break;

		case INSTANCE_NOTIFICATION: {
			if (notification->type == OS_NOTIFICATION_NEW_FILE) {
				OSSetText(instance->textbox, OSLiteral(""), OS_RESIZE_MODE_IGNORE);
				OSTextboxSetSelection(instance->textbox, 0, 0);
			} else if (notification->type == OS_NOTIFICATION_OPEN_FILE) {
				size_t fileSize;
				char *text = (char *) OSReadEntireFile(notification->fileDialog.path, notification->fileDialog.pathBytes, &fileSize); 

				if (text) {
					OSSetText(instance->textbox, text, fileSize, OS_RESIZE_MODE_IGNORE);
					OSTextboxSetSelection(instance->textbox, 0, 0);
					OSHeapFree(text);
				} else {
					return OS_CALLBACK_REJECTED;
				}
			} else if (notification->type == OS_NOTIFICATION_SAVE_FILE) {
				OSString text;
				OSGetText(instance->textbox, &text);

				OSNodeInformation node;
				OSError error = OSOpenNode((char *) notification->fileDialog.path, notification->fileDialog.pathBytes, 
						OS_OPEN_NODE_WRITE_EXCLUSIVE | OS_OPEN_NODE_RESIZE_EXCLUSIVE | OS_OPEN_NODE_CREATE_DIRECTORIES, &node);

				if (error == OS_SUCCESS) {
					error = text.bytes == OSWriteFileSync(node.handle, 0, text.bytes, text.buffer) ? OS_SUCCESS : OS_ERROR_UNKNOWN_OPERATION_FAILURE; 
					OSCloseHandle(node.handle);
				}

				if (error != OS_SUCCESS) {
					notification->fileDialog.error = error;
					return OS_CALLBACK_REJECTED;
				}
			} else {
				return OS_CALLBACK_NOT_HANDLED;
			}

			return OS_CALLBACK_HANDLED;
		} break;

		case WINDOW_NOTIFICATION: {
			if (notification->type == OS_NOTIFICATION_WINDOW_CLOSE) {
				OSDestroyCommands(instance->commands);
				OSDestroyInstance(notification->instance);
				OSHeapFree(instance->findString.buffer);
				OSHeapFree(instance->replaceString.buffer);
				OSHeapFree(instance);
				return OS_CALLBACK_HANDLED;
			}
		} break;

		case TEXTBOX_NOTIFICATION: {
			if (notification->type == OS_NOTIFICATION_MODIFIED) {
				OSMarkInstanceModified(notification->instance);
				return OS_CALLBACK_HANDLED;
			}
		} break;
	}

	return OS_CALLBACK_NOT_HANDLED;
}

OSCallbackResponse ProcessSystemMessage(OSObject _object, OSMessage *message) {
	(void) _object;

	if (message->type == OS_MESSAGE_CREATE_INSTANCE) {
		OSStartGUIAllocationBlock(16384);

		Instance *instance = (Instance *) OSHeapAllocate(sizeof(Instance), true);
		instance->commands = OSCreateCommands(osDefaultCommandGroup);
		OSSetCommandGroupNotificationCallback(instance->commands, ProcessNotification);
		OSObject instanceObject = OSCreateInstance(instance, message, instance->commands);
		OSString arguments = OSGetInstanceData(instanceObject);

		OSObject window = OSCreateWindow(mainWindow, instanceObject);
		OSObject grid = OSCreateGrid(1, 1, OS_GRID_STYLE_LAYOUT);
		OSObject textbox = OSCreateTextbox((OSTextboxStyle) (OS_TEXTBOX_STYLE_MULTILINE | OS_TEXTBOX_STYLE_NO_BORDER), OS_TEXTBOX_WRAP_MODE_NONE);
		OSAddControl(grid, 0, 0, textbox, OS_CELL_FILL);

		instance->window = window;
		instance->textbox = textbox;

		OSSetObjectNotificationCallback(window, OS_MAKE_NOTIFICATION_CALLBACK(ProcessNotification, (intptr_t) WINDOW_NOTIFICATION));
		OSSetObjectNotificationCallback(textbox, OS_MAKE_NOTIFICATION_CALLBACK(ProcessNotification, (intptr_t) TEXTBOX_NOTIFICATION));
		OSSetObjectNotificationCallback(instanceObject, OS_MAKE_NOTIFICATION_CALLBACK(ProcessNotification, (intptr_t) INSTANCE_NOTIFICATION));

		OSSetRootGrid(window, grid);
		OSSetFocusedControl(textbox, true);
		OSSetWindowTitle(window, OSLiteral("Untitled"));

		OSEndGUIAllocationBlock();

		if (arguments.bytes) {
			OSOpenFile(instanceObject, arguments.buffer, arguments.bytes);
		}

		return OS_CALLBACK_HANDLED;
	}

	return OS_CALLBACK_NOT_HANDLED;
}
