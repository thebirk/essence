#include "../api/os.h"

#define OS_MANIFEST_DEFINITIONS
#include "../bin/Programs/Calculator/manifest.h"

struct Instance {
	OSObject textbox;
	OSCommand *commands;
};

struct Token {
	enum { ADD, SUBTRACT, MULTIPLY, DIVIDE, LEFT_BRACKET, RIGHT_BRACKET,
		PERCENTAGE_SIGN, NUMBER, EOL, ERROR, } type;

	union {
		double number;
	};
};

struct EvaluateResult {
	bool error;
	double value;
};

char buffer[1024];

OSCallbackResponse Insert(OSNotification *notification) {
	if (notification->type != OS_NOTIFICATION_COMMAND) return OS_CALLBACK_NOT_HANDLED;
	char c = (char) (uintptr_t) notification->context;
	Instance *instance = (Instance *) notification->instanceContext;
	OSTextboxInsert(instance->textbox, &c, 1);
	return OS_CALLBACK_HANDLED;
}

Token NextToken(char *&string, size_t &stringBytes) {
	while (stringBytes && (string[0] == ' ' || string[0] == '\t')) {
		string++;
		stringBytes--;
	}

	if (!stringBytes) {
		return {Token::EOL};
	}

	switch (string[0]) {
		case '+': { string++; stringBytes--; return {Token::ADD}; } break;
		case '-': { string++; stringBytes--; return {Token::SUBTRACT}; } break;
		case '*': { string++; stringBytes--; return {Token::MULTIPLY}; } break;
		case '/': { string++; stringBytes--; return {Token::DIVIDE}; } break;
		case '(': { string++; stringBytes--; return {Token::LEFT_BRACKET}; } break;
		case ')': { string++; stringBytes--; return {Token::RIGHT_BRACKET}; } break;
		case '%': { string++; stringBytes--; return {Token::PERCENTAGE_SIGN}; } break;

		default: {
			if ((string[0] >= '0' && string[0] <= '9') || string[0] == '.') {
				Token token = {Token::NUMBER};
				uint64_t n = 0, c = 1;
				bool f = false;
				// TODO Error or discard too many digits for integer and fractional parts respectively.

				while (stringBytes && ((string[0] >= '0' && string[0] <= '9') || (!f && string[0] == '.'))) {
					if (string[0] == '.') {
						f = true;
						goto next;
					}

					if (f) c *= 10;
					n *= 10;
					n += string[0] - '0';

					next:;
					string++;
					stringBytes--;
				}

				token.number = (double) n / (double) c;
				return token;
			} else {
				return {Token::ERROR};
			}
		} break;
	}
}

EvaluateResult Evaluate(char *&string, size_t &stringBytes, int precedence = 0) {
#define NEXT_TOKEN() NextToken(string, stringBytes)
#define EVALUATE(p) Evaluate(string, stringBytes, p)

	Token left = NEXT_TOKEN(), right;
	double number = 0;

	char *string2;
	size_t stringBytes2;

	switch (left.type) {
		case Token::NUMBER: {
			number = left.number;
		} break;

		case Token::LEFT_BRACKET: {
			EvaluateResult e = EVALUATE(0);
			if (e.error) goto error;
			number = e.value;

			Token rightBracket = NEXT_TOKEN();

			if (rightBracket.type != Token::RIGHT_BRACKET && rightBracket.type != Token::EOL) {
				goto error;
			}
		} break;

		case Token::SUBTRACT: {
			EvaluateResult e = EVALUATE(1000);
			if (e.error) goto error;
			number = -e.value;
		} break;

		default: {
			goto error;
		} break;
	}

	string2 = string;
	stringBytes2 = stringBytes;

	right = NEXT_TOKEN();

	while (right.type != Token::EOL && right.type != Token::ERROR) {
		switch (right.type) {
			case Token::ADD: {
				if (precedence < 3) {
					EvaluateResult e = EVALUATE(3);
					if (e.error) goto error;
					number += e.value;
				} else {
					string = string2;
					stringBytes = stringBytes2;
					goto done;
				}
			} break;

			case Token::SUBTRACT: {
				if (precedence < 3) {
					EvaluateResult e = EVALUATE(3);
					if (e.error) goto error;
					number -= e.value;
				} else {
					string = string2;
					stringBytes = stringBytes2;
					goto done;
				}
			} break;

			case Token::MULTIPLY: {
				if (precedence < 4) {
					EvaluateResult e = EVALUATE(4);
					if (e.error) goto error;
					number *= e.value;
				} else {
					string = string2;
					stringBytes = stringBytes2;
					goto done;
				}
			} break;

			case Token::DIVIDE: {
				if (precedence < 4) {
					EvaluateResult e = EVALUATE(4);
					if (e.error) goto error;
					// if (e.value == 0) goto error;
					// TODO Temporary.
					if (e.value == 0) OSCrashProcess(OS_FATAL_ERROR_PROCESSOR_EXCEPTION);
					number /= e.value;
				} else {
					string = string2;
					stringBytes = stringBytes2;
					goto done;
				}
			} break;

			case Token::PERCENTAGE_SIGN: {
				number /= 100.0;
			} break;

			default: {
				string = string2;
				stringBytes = stringBytes2;
				goto done;
			} break;
		}

		string2 = string;
		stringBytes2 = stringBytes;
		right = NEXT_TOKEN();
	}

	done:;

	if (right.type == Token::ERROR) {
		goto error;
	}

	return {false, number};

	error:;
	return {true};

#undef NEXT_TOKEN
#undef EVALUATE
}

OSCallbackResponse Evaluate(OSNotification *notification) {
	Instance *instance = (Instance *) notification->instanceContext;

	OSString expression;
	OSGetText(instance->textbox, &expression);

	EvaluateResult e = Evaluate(expression.buffer, expression.bytes);

	size_t length;

	if (e.error) {
		length = OSFormatString(buffer, 1024, "error");
	} else {
		length = OSFormatString(buffer, 1024, "%F", e.value);
	}

	OSSetText(instance->textbox, buffer, length, OS_RESIZE_MODE_IGNORE);

	return OS_CALLBACK_HANDLED;
}

OSObject CreateKeypadButton(OSCommand *command) {
	OSObject button = OSCreateButton(command, OS_BUTTON_STYLE_NORMAL);
	OSSetProperty(button, OS_GUI_OBJECT_PROPERTY_SUGGESTED_WIDTH, 0);
	OSSetProperty(button, OS_GUI_OBJECT_PROPERTY_SUGGESTED_HEIGHT, 0);
	return button;
}

OSCallbackResponse DestroyInstance(OSNotification *notification) {
	if (notification->type != OS_NOTIFICATION_WINDOW_CLOSE) return OS_CALLBACK_NOT_HANDLED;
	Instance *instance = (Instance *) notification->context;
	OSDestroyCommands(instance->commands);
	OSDestroyInstance(notification->instance);
	OSHeapFree(instance);
	return OS_CALLBACK_HANDLED;
}

OSCallbackResponse ProcessSystemMessage(OSObject _object, OSMessage *message) {
	(void) _object;

	if (message->type == OS_MESSAGE_CREATE_INSTANCE) {
		Instance *instance = (Instance *) OSHeapAllocate(sizeof(Instance), true);
		instance->commands = OSCreateCommands(osDefaultCommandGroup);
		OSObject instanceObject = OSCreateInstance(instance, message, instance->commands);

		OSStartGUIAllocationBlock(16384);

		OSObject window = OSCreateWindow(mainWindow, instanceObject);
		OSSetObjectNotificationCallback(window, OS_MAKE_NOTIFICATION_CALLBACK(DestroyInstance, instance));

		OSObject grid = OSCreateGrid(1, 2, OS_GRID_STYLE_CONTAINER);
		OSObject keypad = OSCreateGrid(5, 4, OS_GRID_STYLE_GROUP_BOX);
		instance->textbox = OSCreateTextbox(OS_TEXTBOX_STYLE_LARGE, OS_TEXTBOX_WRAP_MODE_NONE);

		OSSetRootGrid(window, grid);
		OSAddControl(grid, 0, 0, instance->textbox, OS_CELL_H_PUSH | OS_CELL_H_EXPAND);
		OSAddGrid(grid, 0, 1, keypad, OS_CELL_FILL);

		OSAddControl(keypad, 0, 0, CreateKeypadButton(instance->commands + insert7), 			OS_CELL_FILL);
		OSAddControl(keypad, 0, 1, CreateKeypadButton(instance->commands + insert4), 			OS_CELL_FILL);
		OSAddControl(keypad, 0, 2, CreateKeypadButton(instance->commands + insert1), 			OS_CELL_FILL);
		OSAddControl(keypad, 0, 3, CreateKeypadButton(instance->commands + insert0), 			OS_CELL_FILL);
		OSAddControl(keypad, 1, 0, CreateKeypadButton(instance->commands + insert8), 			OS_CELL_FILL);
		OSAddControl(keypad, 1, 1, CreateKeypadButton(instance->commands + insert5), 			OS_CELL_FILL);
		OSAddControl(keypad, 1, 2, CreateKeypadButton(instance->commands + insert2), 			OS_CELL_FILL);
		OSAddControl(keypad, 1, 3, CreateKeypadButton(instance->commands + insertFractionalSeparator), 	OS_CELL_FILL);
		OSAddControl(keypad, 2, 0, CreateKeypadButton(instance->commands + insert9), 			OS_CELL_FILL);
		OSAddControl(keypad, 2, 1, CreateKeypadButton(instance->commands + insert6), 			OS_CELL_FILL);
		OSAddControl(keypad, 2, 2, CreateKeypadButton(instance->commands + insert3), 			OS_CELL_FILL);
		OSAddControl(keypad, 2, 3, CreateKeypadButton(instance->commands + insertPercentageSign),	OS_CELL_FILL);
		OSAddControl(keypad, 3, 0, CreateKeypadButton(instance->commands + insertDivide), 		OS_CELL_FILL);
		OSAddControl(keypad, 3, 1, CreateKeypadButton(instance->commands + insertMultiply), 		OS_CELL_FILL);
		OSAddControl(keypad, 3, 2, CreateKeypadButton(instance->commands + insertSubtract), 		OS_CELL_FILL);
		OSAddControl(keypad, 3, 3, CreateKeypadButton(instance->commands + insertAdd), 			OS_CELL_FILL);
		OSAddControl(keypad, 4, 0, CreateKeypadButton(instance->commands + insertLeftBracket), 		OS_CELL_FILL);
		OSAddControl(keypad, 4, 1, CreateKeypadButton(instance->commands + insertRightBracket), 	OS_CELL_FILL);
		OSAddControl(keypad, 4, 3, CreateKeypadButton(instance->commands + evaluate), 			OS_CELL_FILL);

		OSEndGUIAllocationBlock();

		return OS_CALLBACK_HANDLED;
	} else if (message->type == OS_MESSAGE_PROCESS_REQUEST) {
		size_t arguments = 0;

		for (uintptr_t i = 0; i < message->processRequest.requestBytes; i++) {
			if (message->processRequest.request[i] == '\f') {
				arguments++;
			}
		}

		const char *e = "EVALUATE\f";

		if (arguments == 2) {
			if (OSCStringLength(e) < message->processRequest.requestBytes 
					&& 0 == OSCompareBytes(e, message->processRequest.request, OSCStringLength(e))) {
				while (*message->processRequest.request != '\f') {
					message->processRequest.request++;
					message->processRequest.requestBytes--;
				}

				message->processRequest.request++;
				message->processRequest.requestBytes -= 2;

				EvaluateResult e = Evaluate(message->processRequest.request, message->processRequest.requestBytes);

				size_t length;

				if (e.error) {
					length = OSFormatString(buffer, 1024, "error");
				} else {
					length = OSFormatString(buffer, 1024, "%F", e.value);
				}

				message->processRequest.response = buffer;
				message->processRequest.responseBytes = length;
			}
		}
	}

	return OS_CALLBACK_NOT_HANDLED;
}

void ProgramEntry() {
	OSSetMessageCallback(osSystemMessages, OS_MAKE_MESSAGE_CALLBACK(ProcessSystemMessage, nullptr));
	OSProcessMessages();
}
