// TODO Faster CacheFindBlock.
// TODO Get asynchronous file IO working again (properly this time).
// TODO Caching filesystem metadata, e.g. directory contents.
// 	- Rewrite EsFS.
// TODO Prioritise removal of blocks with the lowest accessIndex.
// TODO Proper errors.

#ifndef IMPLEMENTATION

// Once the number of modified pages in the cache exceeds this value,
// all writes to the cache will be immediately flushed.
#define CACHE_MODIFIED_THRESHOLD (pmm.startPageCount / 16)

// Each block is MM_FILE_CHUNK_BYTES.
#define CACHE_BLOCK_COUNT (1024)

typedef volatile struct _CacheBlock /*Why do I have to put this here??*/ {
	Node *node; // Set to nullptr to indicate a free block.
	uint64_t offset;
	uint64_t accessIndex;
	uint8_t locked : 1, // To lock a block, you must have the node's semaphore.
		modified : 1;
	uint8_t modifiedPages[MM_FILE_CHUNK_PAGES >> 3];
} CacheBlock;

struct CacheManager {
	uint8_t *base;
	CacheBlock *blocks;
	size_t blockCount; // Each block has a size of MM_FILE_CHUNK_BYTES.
	Mutex mutex;
	uint64_t currentAccessIndex;

	uintptr_t modifiedPagesCount;
	Event modifiedPagesEvent;

	Thread *writeModifiedThread, *removeStandbyThread;
};

extern CacheManager cache;

void CacheRemoveBlocks(Node *node, uintptr_t after, bool writeModified);
SharedMemoryRegion *CacheGetSharedMemoryRegion(uintptr_t offsetIntoCache, uintptr_t *offsetIntoSharedMemoryRegion);
void CacheInitialise();
void CacheAccess(Node *node, IORequest *request);
void CacheFlushBlock(CacheBlock *block);

#else

CacheManager cache;

void CacheRemoveBlocks(Node *node, uintptr_t after, bool writeModified) {
	// NOTE The node's semaphore must be taken.

	cache.mutex.Acquire();
	Defer(cache.mutex.Release());

	for (uintptr_t i = 0; i < cache.blockCount; i++) {
		CacheBlock *block = cache.blocks + i;

		if (block->node != node) continue;
		if (block->offset < after) continue;

		if (block->locked) {
			KernelPanic("CacheRemoveBlocks - Block %d containing node %x was locked.\n", i, node);
		}

		if (block->modified && writeModified) {
			Output(OUTCM, "Flushing modified block %d to remove it.\n", i);

			block->locked = true;
			cache.mutex.Release();

			CacheFlushBlock(block);

			cache.mutex.Acquire();
			block->locked = false;
		}

		Output(OUTCM, "Removing block %d.\n", i);
		block->node = nullptr;
	}
}

SharedMemoryRegion *CacheGetSharedMemoryRegion(uintptr_t offsetIntoCache, uintptr_t *offsetIntoSharedMemoryRegion) {
	cache.mutex.Acquire();
	Defer(cache.mutex.Release());

	if (offsetIntoCache >= MM_FILE_CHUNK_BYTES * cache.blockCount) {
		KernelPanic("CacheGetSharedMemoryRegion - Offset %d outside cache.\n", offsetIntoCache);
	}

	uintptr_t blockIndex = offsetIntoCache / MM_FILE_CHUNK_BYTES;
	CacheBlock *block = cache.blocks + blockIndex;

	if (!block->locked) {
		KernelPanic("CacheGetSharedMemoryRegion - Block %d not locked.\n", blockIndex);
	}

	*offsetIntoSharedMemoryRegion = block->offset + (offsetIntoCache % MM_FILE_CHUNK_BYTES);
	return &block->node->region;
}

CacheBlock *CacheFindBlock(Node *node, IORequest *request, uintptr_t requestOffset) {
	// NOTE The node's semaphore must be taken.

	cache.mutex.Acquire();

	CacheBlock *sourceBlock = nullptr, 
		   *unusedBlock = nullptr,
		   *lowestBlock = nullptr;
	uintptr_t lowestAccessIndex = cache.currentAccessIndex;

	for (uintptr_t i = 0; i < cache.blockCount; i++) {
		CacheBlock *block = cache.blocks + i;

		if (block->node == node && block->offset == requestOffset) {
			// This block is the one we are looking for.
			sourceBlock = block;
			break;
		}

		if (!block->node && !unusedBlock) {
			// This block is unused.
			unusedBlock = block;
		} else if (block->accessIndex < lowestAccessIndex && !block->locked) {
			// This block is the least-recently accessed.
			lowestAccessIndex = block->accessIndex;
			lowestBlock = block;
		}
	}

	if (!sourceBlock) {
		// The block wasn't in the cache.

		if (unusedBlock) sourceBlock = unusedBlock;
		else if (lowestBlock) sourceBlock = lowestBlock;

		if (!sourceBlock) {
			// We are out of cache blocks.
			// This is extremely unlikely to occur.
			cache.mutex.Release();
			request->Cancel(OS_ERROR_OUT_OF_CACHE_RESOURCES);
			return nullptr;
		}

		// Make this block ours.

		sourceBlock->node = node;
		sourceBlock->offset = requestOffset;

		uintptr_t blockIndex = sourceBlock - cache.blocks;

		{
			kernelVMM.virtualAddressSpace->lock.Acquire();
			Defer(kernelVMM.virtualAddressSpace->lock.Release());
			kernelVMM.virtualAddressSpace->Remove((uintptr_t) cache.base + blockIndex * MM_FILE_CHUNK_BYTES, MM_FILE_CHUNK_PAGES);
		}
	}

	sourceBlock->accessIndex = cache.currentAccessIndex++;
	sourceBlock->locked = true;

	cache.mutex.Release();

	return sourceBlock;
}

void CacheFlushBlock(CacheBlock *block) {
	if (!block->modified) return;

	cache.mutex.Acquire();

	uintptr_t count = block->node->data.file.fileSize - block->offset;
	if (count > MM_FILE_CHUNK_BYTES) count = MM_FILE_CHUNK_BYTES;

	IORequest *request = (IORequest *) ioRequestPool.Add();
	request->handles = 1;
	request->type = IO_REQUEST_WRITE;
	request->node = block->node;
	request->offset = block->offset;
	request->count = count;
	request->buffer = cache.base + MM_FILE_CHUNK_BYTES * (block - cache.blocks);
	request->flags = IO_REQUEST_BUFFER_ALREADY_IN_KERNEL_ADDRESS_SPACE 
		| IO_REQUEST_BYPASS_CACHE 
		| IO_REQUEST_SEMAPHORE_ALREADY_TAKEN; 

	cache.mutex.Release();

	Output(OUTCM, "Writing block %d, size %d\n", block - cache.blocks, count);

	request->Start();
	request->complete.Wait(OS_WAIT_NO_TIMEOUT);

	cache.mutex.Acquire();

	size_t bytesWritten = request->count;
	OSError error = request->error;

	CloseHandleToObject(request, KERNEL_OBJECT_IO_REQUEST);

	if (bytesWritten != count || error != OS_SUCCESS) {
		Output(OUTCM, "\t(IO error)\n");
		cache.mutex.Release();
		return;
	}

	block->modified = false;

	for (uintptr_t j = 0; j < MM_FILE_CHUNK_PAGES; j++) {
		if (block->modifiedPages[j >> 3] & (1 << (j & 7))) {
			block->modifiedPages[j >> 3] &= ~(1 << (j & 7));
			cache.modifiedPagesCount--;
		}
	}

	cache.mutex.Release();
}

void CacheAccess(Node *node, IORequest *request) {
	uintptr_t requestOffset = request->offset;
	uintptr_t requestCount = request->count;

	Output(OUTCM, "Cache access: node %x, offset %x, count %x\n", node, requestOffset, requestCount);

	uintptr_t blockOffset = requestOffset % MM_FILE_CHUNK_BYTES;
	requestOffset -= blockOffset;
	uint8_t *buffer = (uint8_t *) request->buffer;

	Output(OUTCM, "\tOffset into block %x, aligned request offset %x\n", blockOffset, requestOffset);

	while (requestCount) {
		uintptr_t maximumCount = MM_FILE_CHUNK_BYTES - blockOffset;
		uintptr_t count = requestCount > maximumCount ? maximumCount : requestCount;
		Output(OUTCM, "\tTrying block with offset %x for count of %x\n", requestOffset, count);
		CacheBlock *block = CacheFindBlock(node, request, requestOffset);
		if (!block) return;

		uintptr_t blockIndex = block - cache.blocks;
		uint8_t *blockBase = cache.base + blockIndex * MM_FILE_CHUNK_BYTES;

		{
			// Ensure that the block is loaded into memory.

			FaultInformation fault = {};
			fault.wantWriteAccess = request->type == IO_REQUEST_WRITE;
			kernelVMM.lock.Acquire();
			bool result = kernelVMM.HandlePageFault((uintptr_t) blockBase, 0, &fault);
			kernelVMM.lock.Release();
			if (result) result = fault.Handle();

			if (!result) {
				request->Cancel(OS_ERROR_UNKNOWN_OPERATION_FAILURE);
			}
		}

		// Copy the data.

		Output(OUTCM, "\tUsing block at index %d.\n", blockIndex);

		if (request->type == IO_REQUEST_READ) {
			CopyMemory(buffer, blockBase + blockOffset, count);
		} else if (request->type == IO_REQUEST_WRITE) {
			CopyMemory(blockBase + blockOffset, buffer, count);
		}

		// Update the block.

		cache.mutex.Acquire();

		if (request->type == IO_REQUEST_WRITE) {
			for (uintptr_t i = blockOffset; i < blockOffset + count; i += PAGE_SIZE) {
				uintptr_t page = i >> PAGE_BITS;
				Output(OUTCM, "\tModified page %x of block %d.\n", page, blockIndex);
				block->modifiedPages[page >> 3] |= 1 << (page & 7);
				block->modified = true;
				cache.modifiedPagesCount++;
				cache.modifiedPagesEvent.Set(false, true);
			}
		}

		cache.mutex.Release();

		// If there are a lot of modified pages in the cache, write some out before continuing.

		if (cache.modifiedPagesCount > CACHE_MODIFIED_THRESHOLD) {
			CacheFlushBlock(block);
		}

		// Go to the next part of the request.

		cache.mutex.Acquire();
		block->locked = false;
		cache.mutex.Release();

		blockOffset = 0;
		requestOffset += MM_FILE_CHUNK_BYTES;
		requestCount -= count;
		buffer += count;
	}
}

void CacheWriteModifiedThread() {
	while (true) {
		cache.modifiedPagesEvent.Wait(OS_WAIT_NO_TIMEOUT);

		cache.mutex.Acquire();

		goAgain:;
		bool foundModifiedPages = false;

		for (uintptr_t i = 0; i < cache.blockCount; i++) {
			CacheBlock *block = cache.blocks + i;
			if (!block->node || block->locked || !block->modified) continue;

			foundModifiedPages = true;

			// First, get a pointer to the node we're about to flush a chunk of.
			Node *node = block->node;
			uint64_t offset = block->offset;

			// Make sure that we have a handle to the node.
			// This will acquire the nodeHashTableMutex with the cache's mutex acquired.
			// FYI, the nodeHashTableMutex may be acquired when acquiring some memory mutexes.
			vfs.RegisterNodeHandleForCacheThreads(node);

			// Now, with the cache mutex NOT acquired, 
			// take the node's semaphore.
			// The node will still be valid as we have a handle to it.
			cache.mutex.Release();
			node->semaphore.Take();
			cache.mutex.Acquire();

			// Now that the cache mutex has been reacquired, 
			// we need to check that we're still dealing with the same node chunk.
			// If we are, then we can mark the block as locked,
			// now that we have taken the node's semaphore.
			bool sameChunk = block->node == node && block->offset == offset;
			if (sameChunk) block->locked = true;
			cache.mutex.Release();

			if (sameChunk) {
				// Flush the block, and then mark it as non-locked.
				CacheFlushBlock(block);
				block->locked = false;
			}

			// Return the node's semaphore, and close the handle to the node.
			// These are both done without the cache's mutex,
			// since closing a node handle can cause the cache the be accessed.
			node->semaphore.Return();
			vfs.CloseNode(node, 0);

			// Acquire the cache's mutex again, so that we can continue.
			cache.mutex.Acquire();
		}

		if (foundModifiedPages) goto goAgain;

		cache.mutex.Release();
	}
}

void CacheRemoveStandbyThread() {
	// TODO Test.

	while (true) {
		pmm.signalMemoryLowThread.Wait(OS_WAIT_NO_TIMEOUT);

		cache.mutex.Acquire();

		for (uintptr_t i = 0; i < cache.blockCount; i++) {
			if (pmm.pagesAllocated * 100 / pmm.startPageCount < MEMORY_PERCENTAGE_LOW) break;

			CacheBlock *block = cache.blocks + i;
			if (!block->node || block->locked || block->modified) continue;

			// See CacheWriteModifiedThread for an explanation of how this all works.

			Node *node = block->node;
			uint64_t offset = block->offset;
			vfs.RegisterNodeHandleForCacheThreads(node);

			cache.mutex.Release();
			node->semaphore.Take();
			cache.mutex.Acquire();

			bool sameChunk = block->node == node && block->offset == offset && !block->modified;
			if (sameChunk) block->locked = true;
			cache.mutex.Release();

			if (sameChunk) {
				uintptr_t cacheBlockStart = offset, cacheBlockEnd = offset + MM_FILE_CHUNK_BYTES;

				SharedMemoryRegion *sharedRegion = &node->region;

				sharedRegion->mutex.Acquire();

				// Remove all mappings of pages in this block.

				for (uintptr_t i = 0; i < sharedRegion->mappingsCount; i++) {
					VMMRegionReference reference = sharedRegion->mappings[i];

					VMM *vmm = reference.vmm; 
					vmm->lock.Acquire(); // TODO Deadlock with the sharedRegion mutex.
					vmm->virtualAddressSpace->lock.Acquire();
					pmm.lock.Acquire();

					VMMRegion *region = vmm->regions + reference.index;

					uintptr_t vmmRegionStart = region->offset, vmmRegionEnd = region->offset + region->pageCount * PAGE_SIZE;

					if (vmmRegionEnd > cacheBlockStart 
							&& vmmRegionStart < cacheBlockEnd) {
						uintptr_t intersectionStart = vmmRegionStart > cacheBlockStart ? vmmRegionStart : cacheBlockStart;
						uintptr_t intersectionEnd = vmmRegionEnd < cacheBlockEnd ? vmmRegionEnd : cacheBlockEnd;

						vmm->virtualAddressSpace->Remove(region->baseAddress + intersectionStart, (intersectionEnd - intersectionStart) >> PAGE_BITS);
					}

					pmm.lock.Release();
					vmm->virtualAddressSpace->lock.Release();
					vmm->lock.Release();
				}

				// Free the physical memory.

				for (uintptr_t i = 0; i < MM_FILE_CHUNK_PAGES; i++) {
					uintptr_t *entry = nullptr;
					uintptr_t base = offset + (i << PAGE_BITS);
					uintptr_t group = base / BIG_SHARED_MEMORY;

					if (sharedRegion->big && ((uintptr_t *) (sharedRegion->data))[group]) {
						entry = &(((uintptr_t **) (sharedRegion->data))[group][(base % BIG_SHARED_MEMORY) >> PAGE_BITS]);
					} else {
						entry = &(((uintptr_t *) (sharedRegion->data))[base >> PAGE_BITS]);
					}

					if (entry && (*entry & SHARED_ADDRESS_PRESENT)) {
						pmm.lock.Acquire();
						pmm.FreePage(*entry & ~(PAGE_SIZE - 1));
						*entry = 0;
						pmm.lock.Release();
					}
				}

				sharedRegion->mutex.Release();
			}

			node->semaphore.Return();
			vfs.CloseNode(node, 0);

			cache.mutex.Acquire();
		}

		cache.mutex.Release();
	}
}

void CacheInitialise() {
	cache.blockCount = CACHE_BLOCK_COUNT;

	cache.blocks = (CacheBlock *) OSHeapAllocate(sizeof(CacheBlock) * cache.blockCount, true);
	cache.base = (uint8_t *) kernelVMM.Allocate("FileCache", MM_FILE_CHUNK_BYTES * cache.blockCount, VMM_MAP_CHUNKS, VMM_REGION_FILE_CACHE);
	cache.modifiedPagesEvent.autoReset = true;

	cache.writeModifiedThread = scheduler.SpawnThread((uintptr_t) CacheWriteModifiedThread, 0);

	// TODO Enable once tested.
	// cache.removeStandbyThread = scheduler.SpawnThread((uintptr_t) CacheRemoveStandbyThread, 0);

	Output(OUTCM, "Initialised cache with %d blocks, each %d bytes.\n", cache.blockCount, MM_FILE_CHUNK_BYTES);
}

#endif
